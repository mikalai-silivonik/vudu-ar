

;(function() {
	'use strict'

	/**
		The ARController is the main object for doing AR marker detection with JSARToolKit.

		To use an ARController, you need to tell it the dimensions to use for the AR processing canvas and
		pass it an ARCameraParam to define the camera parameters to use when processing images. 
		The ARCameraParam defines the lens distortion and aspect ratio of the camera used. 
		See https://www.artoolworks.com/support/library/Calibrating_your_camera for more information about AR camera parameteters and how to make and use them.

		If you pass an image as the first argument, the ARController uses that as the image to process,
		using the dimensions of the image as AR processing canvas width and height. If the first argument
		to ARController is an image, the second argument is used as the camera param.

		The camera parameters argument can be either an ARCameraParam or an URL to a camera definition file.
		If the camera argument is an URL, it is loaded into a new ARCameraParam, and the ARController dispatches
		a 'load' event and calls the onload method if it is defined.

	 	@exports ARController
	 	@constructor

		@param {number} width The width of the images to process.
		@param {number} height The height of the images to process.
		@param {ARCameraParam | string} camera The ARCameraParam to use for image processing. If this is a string, the ARController treats it as an URL and tries to load it as a ARCameraParam definition file, calling ARController#onload on success. 
	*/
	var ARController = function(width, height, camera) {
		var id;
		var w = width, h = height;

		this.orientation = 'landscape';

		this.listeners = {};

		if (typeof width !== 'number') {
			var image = width;
			camera = height;
			w = image.videoWidth || image.width;
			h = image.videoHeight || image.height;
			this.image = image;
		}

		this.defaultMarkerWidth = 1;
		this.patternMarkers = {};
		this.barcodeMarkers = {};
		this.transform_mat = new Float32Array(16);

		this.canvas = document.createElement('canvas');
		this.canvas.width = w;
		this.canvas.height = h;
		this.ctx = this.canvas.getContext('2d');

		this.videoWidth = w;
		this.videoHeight = h;

		if (typeof camera === 'string') {

			var self = this;
			this.cameraParam = new ARCameraParam(camera, function() {
				self._initialize();
			}, function(err) {
				console.error("ARController: Failed to load ARCameraParam", err);
			});

		} else {

			this.cameraParam = camera;
			this._initialize();

		}
	};

	/**
		Destroys the ARController instance and frees all associated resources.
		After calling dispose, the ARController can't be used any longer. Make a new one if you need one.

		Calling this avoids leaking Emscripten memory, which may be important if you're using multiple ARControllers.
	*/
	ARController.prototype.dispose = function() {
		artoolkit.teardown(this.id);

		for (var t in this) {
			this[t] = null;
		}
	};

	/**
		Detects markers in the given image. The process method dispatches marker detection events during its run.

		The marker detection process proceeds by first dispatching a markerNum event that tells you how many
		markers were found in the image. Next, a getMarker event is dispatched for each found marker square.
		Finally, getMultiMarker is dispatched for every found multimarker, followed by getMultiMarkerSub events
		dispatched for each of the markers in the multimarker.
			
			arController.addEventListener('markerNum', function(ev) {
				console.log("Detected " + ev.data + " markers.")
			});
			arController.addEventListener('getMarker', function(ev) {
				console.log("Detected marker with ids:", ev.data.marker.id, ev.data.marker.idPatt, ev.data.marker.idMatrix);
				console.log("Marker data", ev.data.marker);
				console.log("Marker transform matrix:", [].join.call(ev.data.matrix, ', '));
			});
			arController.addEventListener('getMultiMarker', function(ev) {
				console.log("Detected multimarker with id:", ev.data.multiMarkerId);
			});
			arController.addEventListener('getMultiMarkerSub', function(ev) {
				console.log("Submarker for " + ev.data.multiMarkerId, ev.data.markerIndex, ev.data.marker);
			});
			
			arController.process(image);	


		If no image is given, defaults to this.image.

		If the debugSetup has been called, draws debug markers on the debug canvas.

		@param {ImageElement | VideoElement} image The image to process [optional]. 
	*/
	ARController.prototype.process = function(image) {
		this.detectMarker(image);

		var markerNum = this.getMarkerNum();
		var k,o;
		for (k in this.patternMarkers) {
			o = this.patternMarkers[k]
			o.inPrevious = o.inCurrent;
			o.inCurrent = false;
		}
		for (k in this.barcodeMarkers) {
			o = this.barcodeMarkers[k]
			o.inPrevious = o.inCurrent;
			o.inCurrent = false;
		}

		for (var i=0; i<markerNum; i++) {
			var markerInfo = this.getMarker(i);

			var markerType = artoolkit.UNKNOWN_MARKER;
			var visible = this.trackPatternMarkerId(-1);

			if (markerInfo.idPatt > -1 && (markerInfo.id === markerInfo.idPatt || markerInfo.idMatrix === -1)) {
				visible = this.trackPatternMarkerId(markerInfo.idPatt);
				markerType = artoolkit.PATTERN_MARKER;

				if (markerInfo.dir !== markerInfo.dirPatt) {
					this.setMarkerInfoDir(i, markerInfo.dirPatt);
				}

			} else if (markerInfo.idMatrix > -1) {
				visible = this.trackBarcodeMarkerId(markerInfo.idMatrix);
				markerType = artoolkit.BARCODE_MARKER;

				if (markerInfo.dir !== markerInfo.dirMatrix) {
					this.setMarkerInfoDir(i, markerInfo.dirMatrix);
				}
			}

			if (markerType !== artoolkit.UNKNOWN_MARKER && visible.inPrevious) {
				this.getTransMatSquareCont(i, visible.markerWidth, visible.matrix, visible.matrix);
			} else {
				this.getTransMatSquare(i, visible.markerWidth, visible.matrix);
			}
// this.getTransMatSquare(i, visible.markerWidth, visible.matrix);

			visible.inCurrent = true;
			this.transMatToGLMat(visible.matrix, this.transform_mat);
			this.dispatchEvent({
				name: 'getMarker',
				target: this,
				data: {
					index: i,
					type: markerType,
					marker: markerInfo,
					matrix: this.transform_mat
				}
			});
		}

		var multiMarkerCount = this.getMultiMarkerCount();
		for (var i=0; i<multiMarkerCount; i++) {
			var subMarkerCount = this.getMultiMarkerPatternCount(i);
			var visible = false;

			artoolkit.getTransMatMultiSquareRobust(this.id, i);
			this.transMatToGLMat(this.marker_transform_mat, this.transform_mat);
			for (var j=0; j<subMarkerCount; j++) {
				var multiEachMarkerInfo = this.getMultiEachMarker(i, j);
				if (multiEachMarkerInfo.visible >= 0) {
					visible = true;
					this.dispatchEvent({
						name: 'getMultiMarker',
						target: this,
						data: {
							multiMarkerId: i,
							matrix: this.transform_mat
						}
					});
					break;
				}
			}
			if (visible) {
				for (var j=0; j<subMarkerCount; j++) {
					var multiEachMarkerInfo = this.getMultiEachMarker(i, j);
					this.transMatToGLMat(this.marker_transform_mat, this.transform_mat);
					this.dispatchEvent({
						name: 'getMultiMarkerSub',
						target: this,
						data: {
							multiMarkerId: i,
							markerIndex: j,
							marker: multiEachMarkerInfo,
							matrix: this.transform_mat
						}
					});
				}
			}
		}
		if (this._bwpointer) {
			this.debugDraw();
		}
	};

	/**
		Adds the given pattern marker ID to the index of tracked IDs.
		Sets the markerWidth for the pattern marker to markerWidth.

		Used by process() to implement continuous tracking, 
		keeping track of the marker's transformation matrix
		and customizable marker widths.

		@param {number} id ID of the pattern marker to track.
		@param {number} markerWidth The width of the marker to track.
		@return {Object} The marker tracking object.
	*/
	ARController.prototype.trackPatternMarkerId = function(id, markerWidth) {
		var obj = this.patternMarkers[id];
		if (!obj) {
			this.patternMarkers[id] = obj = {
				inPrevious: false,
				inCurrent: false,
				matrix: new Float32Array(12),
				markerWidth: markerWidth || this.defaultMarkerWidth
			};
		}
		if (markerWidth) {
			obj.markerWidth = markerWidth;
		}
		return obj;
	};

	/**
		Adds the given barcode marker ID to the index of tracked IDs.
		Sets the markerWidth for the pattern marker to markerWidth.

		Used by process() to implement continuous tracking, 
		keeping track of the marker's transformation matrix
		and customizable marker widths.

		@param {number} id ID of the barcode marker to track.
		@param {number} markerWidth The width of the marker to track.
		@return {Object} The marker tracking object.
	*/
	ARController.prototype.trackBarcodeMarkerId = function(id, markerWidth) {
		var obj = this.barcodeMarkers[id];
		if (!obj) {
			this.barcodeMarkers[id] = obj = {
				inPrevious: false,
				inCurrent: false,
				matrix: new Float32Array(12),
				markerWidth: markerWidth || this.defaultMarkerWidth
			};
		}
		if (markerWidth) {
			obj.markerWidth = markerWidth;
		}
		return obj;
	};

	/**
		Returns the number of multimarkers registered on this ARController.

		@return {number} Number of multimarkers registered.
	*/
	ARController.prototype.getMultiMarkerCount = function() {
		return artoolkit.getMultiMarkerCount(this.id);
	};

	/**
		Returns the number of markers in the multimarker registered for the given multiMarkerId.

		@param {number} multiMarkerId The id number of the multimarker to access. Given by loadMultiMarker.
		@return {number} Number of markers in the multimarker. Negative value indicates failure to find the multimarker.
	*/
	ARController.prototype.getMultiMarkerPatternCount = function(multiMarkerId) {
		return artoolkit.getMultiMarkerNum(this.id, multiMarkerId);
	};

	/**
		Add an event listener on this ARController for the named event, calling the callback function
		whenever that event is dispatched.

		Possible events are: 
		  * getMarker - dispatched whenever process() finds a square marker
		  * getMultiMarker - dispatched whenever process() finds a visible registered multimarker
		  * getMultiMarkerSub - dispatched by process() for each marker in a visible multimarker
		  * load - dispatched when the ARController is ready to use (useful if passing in a camera URL in the constructor)

		@param {string} name Name of the event to listen to.
		@param {function} callback Callback function to call when an event with the given name is dispatched.
	*/
	ARController.prototype.addEventListener = function(name, callback) {
       if (!this.listeners[name]) {
			this.listeners[name] = [];
		}
		this.listeners[name].push(callback);
	};

	/**
		Remove an event listener from the named event.

		@param {string} name Name of the event to stop listening to.
		@param {function} callback Callback function to remove from the listeners of the named event.
	*/
	ARController.prototype.removeEventListener = function(name, callback) {
		if (this.listeners[name]) {
			var index = this.listeners[name].indexOf(callback);
			if (index > -1) {
				this.listeners[name].splice(index, 1);
			}
		}
	};

	/**
		Dispatches the given event to all registered listeners on event.name.

		@param {Object} event Event to dispatch.
	*/
	ARController.prototype.dispatchEvent = function(event) {
		var listeners = this.listeners[event.name];
		if (listeners) {
			for (var i=0; i<listeners.length; i++) {
				listeners[i].call(this, event);
			}
		}
	};

	/**
		Sets up a debug canvas for the AR detection. Draws a red marker on top of each detected square in the image.

		The debug canvas is added to document.body.
	*/
	ARController.prototype.debugSetup = function() {
		document.body.appendChild(this.canvas)
		this.setDebugMode(1);
		this._bwpointer = this.getProcessingImage();
	};

	/**
		Loads a pattern marker from the given URL and calls the onSuccess callback with the UID of the marker.

		arController.loadMarker(markerURL, onSuccess, onError);

		@param {string} markerURL - The URL of the marker pattern file to load.
		@param {function} onSuccess - The success callback. Called with the id of the loaded marker on a successful load.
		@param {function} onError - The error callback. Called with the encountered error if the load fails.
	*/
	ARController.prototype.loadMarker = function(markerURL, onSuccess, onError) {
		return artoolkit.addMarker(this.id, markerURL, onSuccess, onError);
	};

	/**
		Loads a multimarker from the given URL and calls the onSuccess callback with the UID of the marker.

		arController.loadMultiMarker(markerURL, onSuccess, onError);

		@param {string} markerURL - The URL of the multimarker pattern file to load.
		@param {function} onSuccess - The success callback. Called with the id and the number of sub-markers of the loaded marker on a successful load.
		@param {function} onError - The error callback. Called with the encountered error if the load fails.
	*/
	ARController.prototype.loadMultiMarker = function(markerURL, onSuccess, onError) {
		return artoolkit.addMultiMarker(this.id, markerURL, onSuccess, onError);
	};
	
	/**
	 * Populates the provided float array with the current transformation for the specified marker. After 
	 * a call to detectMarker, all marker information will be current. Marker transformations can then be 
	 * checked.
	 * @param {number} markerUID	The unique identifier (UID) of the marker to query
	 * @param {number} markerWidth	The width of the marker
	 * @param {Float64Array} dst	The float array to populate with the 3x4 marker transformation matrix
	 * @return	{Float64Array} The dst array.
	 */
	ARController.prototype.getTransMatSquare = function(markerIndex, markerWidth, dst) {
		artoolkit.getTransMatSquare(this.id, markerIndex, markerWidth);
		dst.set(this.marker_transform_mat);
		return dst;
	};

	/**
	 * Populates the provided float array with the current transformation for the specified marker, using 
	 * previousMarkerTransform as the previously detected transformation. After 
	 * a call to detectMarker, all marker information will be current. Marker transformations can then be 
	 * checked.
	 * @param {number} markerUID	The unique identifier (UID) of the marker to query
	 * @param {number} markerWidth	The width of the marker
	 * @param {Float64Array} previousMarkerTransform	The float array to use as the previous 3x4 marker transformation matrix
	 * @param {Float64Array} dst	The float array to populate with the 3x4 marker transformation matrix
	 * @return	{Float64Array} The dst array.
	 */
	ARController.prototype.getTransMatSquareCont = function(markerIndex, markerWidth, previousMarkerTransform, dst) {
		this.marker_transform_mat.set(previousMarkerTransform)
		artoolkit.getTransMatSquareCont(this.id, markerIndex, markerWidth);
		dst.set(this.marker_transform_mat);
		return dst;
	};

	/**
	 * Populates the provided float array with the current transformation for the specified multimarker. After 
	 * a call to detectMarker, all marker information will be current. Marker transformations can then be 
	 * checked.
	 *
	 * @param {number} markerUID	The unique identifier (UID) of the marker to query
	 * @param {number} markerWidth	The width of the marker
	 * @param {Float64Array} dst	The float array to populate with the 3x4 marker transformation matrix
	 * @return	{Float64Array} The dst array.
	 */
	ARController.prototype.getTransMatMultiSquare = function(multiMarkerId, dst) {
		artoolkit.getTransMatMultiSquare(this.id, multiMarkerId);
		dst.set(this.marker_transform_mat);
		return dst;
	};

	/**
	 * Populates the provided float array with the current robust transformation for the specified multimarker. After 
	 * a call to detectMarker, all marker information will be current. Marker transformations can then be 
	 * checked.
	 * @param {number} markerUID	The unique identifier (UID) of the marker to query
	 * @param {number} markerWidth	The width of the marker
	 * @param {Float64Array} dst	The float array to populate with the 3x4 marker transformation matrix
	 * @return	{Float64Array} The dst array.
	 */
	ARController.prototype.getTransMatMultiSquareRobust = function(multiMarkerId, dst) {
		artoolkit.getTransMatMultiSquare(this.id, multiMarkerId);
		dst.set(this.marker_transform_mat);
		return dst;
	};

	/**
		Converts the given 3x4 marker transformation matrix in the 12-element transMat array
		into a 4x4 WebGL matrix and writes the result into the 16-element glMat array.

		If scale parameter is given, scales the transform of the glMat by the scale parameter.

		@param {Float64Array} transMat The 3x4 marker transformation matrix.
		@param {Float64Array} glMat The 4x4 GL transformation matrix.
		@param {number} scale The scale for the transform.
	*/ 
	ARController.prototype.transMatToGLMat = function(transMat, glMat, scale) {
		glMat[0 + 0*4] = transMat[0]; // R1C1
		glMat[0 + 1*4] = transMat[1]; // R1C2
		glMat[0 + 2*4] = transMat[2];
		glMat[0 + 3*4] = transMat[3];
		glMat[1 + 0*4] = transMat[4]; // R2
		glMat[1 + 1*4] = transMat[5];
		glMat[1 + 2*4] = transMat[6];
		glMat[1 + 3*4] = transMat[7];
		glMat[2 + 0*4] = transMat[8]; // R3
		glMat[2 + 1*4] = transMat[9];
		glMat[2 + 2*4] = transMat[10];
		glMat[2 + 3*4] = transMat[11];
		glMat[3 + 0*4] = 0.0;
		glMat[3 + 1*4] = 0.0;
		glMat[3 + 2*4] = 0.0;
		glMat[3 + 3*4] = 1.0;
		if (scale != undefined && scale !== 0.0) {
			glMat[12] *= scale;
			glMat[13] *= scale;
			glMat[14] *= scale;
		}
		return glMat;
	};

	/**
		This is the core ARToolKit marker detection function. It calls through to a set of
		internal functions to perform the key marker detection steps of binarization and
		labelling, contour extraction, and template matching and/or matrix code extraction.
        
        Typically, the resulting set of detected markers is retrieved by calling arGetMarkerNum
        to get the number of markers detected and arGetMarker to get an array of ARMarkerInfo
        structures with information on each detected marker, followed by a step in which
        detected markers are possibly examined for some measure of goodness of match (e.g. by
        examining the match confidence value) and pose extraction.

		@param {image} Image to be processed to detect markers.
		@return {number}     0 if the function proceeded without error, or a value less than 0 in case of error.
			A result of 0 does not however, imply any markers were detected.
	*/
	ARController.prototype.detectMarker = function(image) {
		if (this._copyImageToHeap(image)) {
			return artoolkit.detectMarker(this.id);
		}
		return -99;
	};

	/**
		Get the number of markers detected in a video frame.
  
	    @return {number}     The number of detected markers in the most recent image passed to arDetectMarker.
    	    Note that this is actually a count, not an index. A better name for this function would be
        	arGetDetectedMarkerCount, but the current name lives on for historical reasons.
    */
	ARController.prototype.getMarkerNum = function() {
		return artoolkit.getMarkerNum(this.id);
	};

	/**
		Get the marker info struct for the given marker index in detected markers.

		Call this.detectMarker first, then use this.getMarkerNum to get the detected marker count.

		The returned object is the global artoolkit.markerInfo object and will be overwritten
		by subsequent calls. If you need to hang on to it, create a copy using this.cloneMarkerInfo();

		Returns undefined if no marker was found.

		A markerIndex of -1 is used to access the global custom marker.

		The fields of the markerInfo struct are:
		    @field      area Area in pixels of the largest connected region, comprising the marker border and regions connected to it. Note that this is
		        not the same as the actual onscreen area inside the marker border.
			@field      id If pattern detection mode is either pattern mode OR matrix but not both, will be marker ID (>= 0) if marker is valid, or -1 if invalid.
			@field      idPatt If pattern detection mode includes a pattern mode, will be marker ID (>= 0) if marker is valid, or -1 if invalid.
		    @field      idMatrix If pattern detection mode includes a matrix mode, will be marker ID (>= 0) if marker is valid, or -1 if invalid.
			@field      dir If pattern detection mode is either pattern mode OR matrix but not both, and id != -1, will be marker direction (range 0 to 3, inclusive).
			@field      dirPatt If pattern detection mode includes a pattern mode, and id != -1, will be marker direction (range 0 to 3, inclusive).
			@field      dirMatrix If pattern detection mode includes a matrix mode, and id != -1, will be marker direction (range 0 to 3, inclusive).
			@field      cf If pattern detection mode is either pattern mode OR matrix but not both, will be marker matching confidence (range 0.0 to 1.0 inclusive) if marker is valid, or -1.0 if marker is invalid.
			@field      cfPatt If pattern detection mode includes a pattern mode, will be marker matching confidence (range 0.0 to 1.0 inclusive) if marker is valid, or -1.0 if marker is invalid.
			@field      cfMatrix If pattern detection mode includes a matrix mode, will be marker matching confidence (range 0.0 to 1.0 inclusive) if marker is valid, or -1.0 if marker is invalid.
			@field      pos 2D position (in camera image coordinates, origin at top-left) of the centre of the marker.
			@field      line Line equations for the 4 sides of the marker.
			@field      vertex 2D positions (in camera image coordinates, origin at top-left) of the corners of the marker. vertex[(4 - dir)%4][] is the top-left corner of the marker. Other vertices proceed clockwise from this. These are idealised coordinates (i.e. the onscreen position aligns correctly with the undistorted camera image.)


		@param {number} markerIndex The index of the marker to query.
		@returns {Object} The markerInfo struct.
	*/
	ARController.prototype.getMarker = function(markerIndex) {
		if (0 === artoolkit.getMarker(this.id, markerIndex)) {
			return artoolkit.markerInfo;
		}
	};

	/**
		Set marker vertices to the given vertexData[4][2] array.

		Sets the marker pos to the center of the vertices.

		Useful for building custom markers for getTransMatSquare.

		A markerIndex of -1 is used to access the global custom marker.

		@param {number} markerIndex The index of the marker to edit.
	*/
	ARController.prototype.setMarkerInfoVertex = function(markerIndex, vertexData) {
		for (var i=0; i<vertexData.length; i++) {
			this.marker_transform_mat[i*2+0] = vertexData[i][0];
			this.marker_transform_mat[i*2+1] = vertexData[i][1];
		}
		return artoolkit.setMarkerInfoVertex(this.id, markerIndex);
	};

	/**
		Makes a deep copy of the given marker info.

		@param {Object} markerInfo The marker info object to copy.
		@return {Object} The new copy of the marker info.
	*/
	ARController.prototype.cloneMarkerInfo = function(markerInfo) {
		return JSON.parse(JSON.stringify(markerInfo));
	};

	/**
		Get the marker info struct for the given marker index in detected markers.

		Call this.detectMarker first, then use this.getMarkerNum to get the detected marker count.

		The returned object is the global artoolkit.markerInfo object and will be overwritten
		by subsequent calls. If you need to hang on to it, create a copy using this.cloneMarkerInfo();

		Returns undefined if no marker was found.

		@field {number} pattId The index of the marker.
		@field {number} pattType The type of the marker. Either AR_MULTI_PATTERN_TYPE_TEMPLATE or AR_MULTI_PATTERN_TYPE_MATRIX.
		@field {number} visible 0 or larger if the marker is visible
		@field {number} width The width of the marker.

		@param {number} multiMarkerId The multimarker to query.
		@param {number} markerIndex The index of the marker to query.
		@returns {Object} The markerInfo struct.
	*/
	ARController.prototype.getMultiEachMarker = function(multiMarkerId, markerIndex) {
		if (0 === artoolkit.getMultiEachMarker(this.id, multiMarkerId, markerIndex)) {
			return artoolkit.multiEachMarkerInfo;
		}
	};


	/**
		Returns the 16-element WebGL transformation matrix used by ARController.process to 
		pass marker WebGL matrices to event listeners.

		Unique to each ARController.

		@return {Float64Array} The 16-element WebGL transformation matrix used by the ARController.
	*/
	ARController.prototype.getTransformationMatrix = function() {
		return this.transform_mat;
	};

	/**
	 * Returns the projection matrix computed from camera parameters for the ARController.
	 *
	 * @return {Float64Array} The 16-element WebGL camera matrix for the ARController camera parameters.
	 */
	ARController.prototype.getCameraMatrix = function() {
		return this.camera_mat;
	};

	/**
		Returns the shared ARToolKit 3x4 marker transformation matrix, used for passing and receiving
		marker transforms to/from the Emscripten side.

		@return {Float64Array} The 12-element 3x4 row-major marker transformation matrix used by ARToolKit.
	*/
	ARController.prototype.getMarkerTransformationMatrix = function() {
		return this.marker_transform_mat;
	};


	/* Setter / Getter Proxies */

	/**
	 * Enables or disables debug mode in the tracker. When enabled, a black and white debug
	 * image is generated during marker detection. The debug image is useful for visualising
	 * the binarization process and choosing a threshold value.
	 * @param {number} debug		true to enable debug mode, false to disable debug mode
	 * @see				getDebugMode()
	 */
	ARController.prototype.setDebugMode = function(mode) {
		return artoolkit.setDebugMode(this.id, mode);
	};

	/**
	 * Returns whether debug mode is currently enabled.
	 * @return			true when debug mode is enabled, false when debug mode is disabled
	 * @see				setDebugMode()
	 */
	ARController.prototype.getDebugMode = function() {
		return artoolkit.getDebugMode(this.id);
	};

	/**
		Returns the Emscripten HEAP offset to the debug processing image used by ARToolKit.

		@return {number} HEAP offset to the debug processing image.
	*/
	ARController.prototype.getProcessingImage = function() {
		return artoolkit.getProcessingImage(this.id);
	}

	/**
		Sets the logging level to use by ARToolKit.

		@param 
	*/
	ARController.prototype.setLogLevel = function(mode) {
		return artoolkit.setLogLevel(mode);
	};

	ARController.prototype.getLogLevel = function() {
		return artoolkit.getLogLevel();
	};

	ARController.prototype.setMarkerInfoDir = function(markerIndex, dir) {
		return artoolkit.setMarkerInfoDir(this.id, markerIndex, dir);
	};

	ARController.prototype.setProjectionNearPlane = function(value) {
		return artoolkit.setProjectionNearPlane(this.id, value);
	};

	ARController.prototype.getProjectionNearPlane = function() {
		return artoolkit.getProjectionNearPlane(this.id);
	};

	ARController.prototype.setProjectionFarPlane = function(value) {
		return artoolkit.setProjectionFarPlane(this.id, value);
	};

	ARController.prototype.getProjectionFarPlane = function() {
		return artoolkit.getProjectionFarPlane(this.id);
	};


	/**
	    Set the labeling threshold mode (auto/manual).

	    @param {number}		mode An integer specifying the mode. One of:
	        AR_LABELING_THRESH_MODE_MANUAL,
	        AR_LABELING_THRESH_MODE_AUTO_MEDIAN,
	        AR_LABELING_THRESH_MODE_AUTO_OTSU,
	        AR_LABELING_THRESH_MODE_AUTO_ADAPTIVE,
	        AR_LABELING_THRESH_MODE_AUTO_BRACKETING
	 */
 	ARController.prototype.setThresholdMode = function(mode) {
		return artoolkit.setThresholdMode(this.id, mode);
	};

	/**
	 * Gets the current threshold mode used for image binarization.
	 * @return	{number}		The current threshold mode
	 * @see				getVideoThresholdMode()
	 */
	ARController.prototype.getThresholdMode = function() {
		return artoolkit.getThresholdMode(this.id);
	};

	/**
    	Set the labeling threshhold.

        This function forces sets the threshold value.
        The default value is AR_DEFAULT_LABELING_THRESH which is 100.
        
        The current threshold mode is not affected by this call.
        Typically, this function is used when labeling threshold mode
        is AR_LABELING_THRESH_MODE_MANUAL.
 
        The threshold value is not relevant if threshold mode is
        AR_LABELING_THRESH_MODE_AUTO_ADAPTIVE.
 
        Background: The labeling threshold is the value which
		the AR library uses to differentiate between black and white
		portions of an ARToolKit marker. Since the actual brightness,
		contrast, and gamma of incoming images can vary signficantly
		between different cameras and lighting conditions, this
		value typically needs to be adjusted dynamically to a
		suitable midpoint between the observed values for black
		and white portions of the markers in the image.

		@param {number}     thresh An integer in the range [0,255] (inclusive).
	*/
	ARController.prototype.setThreshold = function(threshold) {
		return artoolkit.setThreshold(this.id, threshold);
	};

	/**
	    Get the current labeling threshold.

		This function queries the current labeling threshold. For,
		AR_LABELING_THRESH_MODE_AUTO_MEDIAN, AR_LABELING_THRESH_MODE_AUTO_OTSU,
		and AR_LABELING_THRESH_MODE_AUTO_BRACKETING
		the threshold value is only valid until the next auto-update.

		The current threshold mode is not affected by this call.

		The threshold value is not relevant if threshold mode is
		AR_LABELING_THRESH_MODE_AUTO_ADAPTIVE.

	    @return {number} The current threshold value.
	*/
	ARController.prototype.getThreshold = function() {
		return artoolkit.getThreshold(this.id);
	};


	/**
		Set the pattern detection mode

		The pattern detection determines the method by which ARToolKit
		matches detected squares in the video image to marker templates
		and/or IDs. ARToolKit v4.x can match against pictorial "template" markers,
		whose pattern files are created with the mk_patt utility, in either colour
		or mono, and additionally can match against 2D-barcode-type "matrix"
		markers, which have an embedded marker ID. Two different two-pass modes
		are also available, in which a matrix-detection pass is made first,
		followed by a template-matching pass.

		@param {number} mode
			Options for this field are:
			AR_TEMPLATE_MATCHING_COLOR
			AR_TEMPLATE_MATCHING_MONO
			AR_MATRIX_CODE_DETECTION
			AR_TEMPLATE_MATCHING_COLOR_AND_MATRIX
			AR_TEMPLATE_MATCHING_MONO_AND_MATRIX
			The default mode is AR_TEMPLATE_MATCHING_COLOR.
	*/
	ARController.prototype.setPatternDetectionMode = function(value) {
		return artoolkit.setPatternDetectionMode(this.id, value);
	};

	/**
		Returns the current pattern detection mode.

		@return {number} The current pattern detection mode.
	*/
	ARController.prototype.getPatternDetectionMode = function() {
		return artoolkit.getPatternDetectionMode(this.id);
	};

	/**
		Set the size and ECC algorithm to be used for matrix code (2D barcode) marker detection.

		When matrix-code (2D barcode) marker detection is enabled (see arSetPatternDetectionMode)
		then the size of the barcode pattern and the type of error checking and correction (ECC)
		with which the markers were produced can be set via this function.

		This setting is global to a given ARHandle; It is not possible to have two different matrix
		code types in use at once.

	    @param      type The type of matrix code (2D barcode) in use. Options include:
	        AR_MATRIX_CODE_3x3
	        AR_MATRIX_CODE_3x3_HAMMING63
	        AR_MATRIX_CODE_3x3_PARITY65
	        AR_MATRIX_CODE_4x4
	        AR_MATRIX_CODE_4x4_BCH_13_9_3
	        AR_MATRIX_CODE_4x4_BCH_13_5_5
	        The default mode is AR_MATRIX_CODE_3x3.
	*/
	ARController.prototype.setMatrixCodeType = function(value) {
		return artoolkit.setMatrixCodeType(this.id, value);
	};

	/**
		Returns the current matrix code (2D barcode) marker detection type.

		@return {number} The current matrix code type.
	*/
	ARController.prototype.getMatrixCodeType = function() {
		return artoolkit.getMatrixCodeType(this.id);
	};

	/**
		Select between detection of black markers and white markers.
	
		ARToolKit's labelling algorithm can work with both black-bordered
		markers on a white background (AR_LABELING_BLACK_REGION) or
		white-bordered markers on a black background (AR_LABELING_WHITE_REGION).
		This function allows you to specify the type of markers to look for.
		Note that this does not affect the pattern-detection algorith
		which works on the interior of the marker.

		@param {number}      mode
			Options for this field are:
			AR_LABELING_WHITE_REGION
			AR_LABELING_BLACK_REGION
			The default mode is AR_LABELING_BLACK_REGION.
	*/
	ARController.prototype.setLabelingMode = function(value) {
		return artoolkit.setLabelingMode(this.id, value);
	};

	/**
		Enquire whether detection is looking for black markers or white markers.
	    
	    See discussion for setLabelingMode.

	    @result {number} The current labeling mode.
	*/
	ARController.prototype.getLabelingMode = function() {
		return artoolkit.getLabelingMode(this.id);
	};

	/**
		Set the width/height of the marker pattern space, as a proportion of marker width/height.

	    @param {number}		pattRatio The the width/height of the marker pattern space, as a proportion of marker
	        width/height. To set the default, pass AR_PATT_RATIO.
	        If compatibility with ARToolKit verions 1.0 through 4.4 is required, this value
	        must be 0.5.
	 */
 	ARController.prototype.setPattRatio = function(value) {
		return artoolkit.setPattRatio(this.id, value);
	};

	/**
		Returns the current ratio of the marker pattern to the total marker size.

		@return {number} The current pattern ratio.
	*/
	ARController.prototype.getPattRatio = function() {
		return artoolkit.getPattRatio(this.id);
	};

	/**
	    Set the image processing mode.

        When the image processing mode is AR_IMAGE_PROC_FRAME_IMAGE,
        ARToolKit processes all pixels in each incoming image
        to locate markers. When the mode is AR_IMAGE_PROC_FIELD_IMAGE,
        ARToolKit processes pixels in only every second pixel row and
        column. This is useful both for handling images from interlaced
        video sources (where alternate lines are assembled from alternate
        fields and thus have one field time-difference, resulting in a
        "comb" effect) such as Digital Video cameras.
        The effective reduction by 75% in the pixels processed also
        has utility in accelerating tracking by effectively reducing
        the image size to one quarter size, at the cost of pose accuraccy.

	    @param {number} mode
			Options for this field are:
			AR_IMAGE_PROC_FRAME_IMAGE
			AR_IMAGE_PROC_FIELD_IMAGE
			The default mode is AR_IMAGE_PROC_FRAME_IMAGE.
	*/
	ARController.prototype.setImageProcMode = function(value) {
		return artoolkit.setImageProcMode(this.id, value);
	};

	/**
	    Get the image processing mode.

		See arSetImageProcMode() for a complete description.

	    @return {number} The current image processing mode.
	*/
	ARController.prototype.getImageProcMode = function() {
		return artoolkit.getImageProcMode(this.id);
	};


	/**
		Draw the black and white image and debug markers to the ARController canvas.

		See setDebugMode.
	*/
	ARController.prototype.debugDraw = function() {
		var debugBuffer = new Uint8ClampedArray(Module.HEAPU8.buffer, this._bwpointer, this.framesize);
		var id = new ImageData(debugBuffer, this.canvas.width, this.canvas.height)
		this.ctx.putImageData(id, 0, 0)

		var marker_num = this.getMarkerNum();
		for (var i=0; i<marker_num; i++) {
			this._debugMarker(this.getMarker(i));
		}
	};


	// private

	ARController.prototype._initialize = function() {
		this.id = artoolkit.setup(this.canvas.width, this.canvas.height, this.cameraParam.id);

		var params = artoolkit.frameMalloc;
		this.framepointer = params.framepointer;
		this.framesize = params.framesize;

		this.dataHeap = new Uint8Array(Module.HEAPU8.buffer, this.framepointer, this.framesize);

		this.camera_mat = new Float64Array(Module.HEAPU8.buffer, params.camera, 16);
		this.marker_transform_mat = new Float64Array(Module.HEAPU8.buffer, params.transform, 12);

		this.setProjectionNearPlane(0.1)
		this.setProjectionFarPlane(1000);

		var self = this;
		setTimeout(function() {
			if (self.onload) {
				self.onload();
			}
			self.dispatchEvent({
				name: 'load',
				target: self
			});
		}, 1);
	};

	ARController.prototype._copyImageToHeap = function(image) {
		if (!image) {
			image = this.image;
		}


		if (this.orientation === 'portrait') {
			this.ctx.save();
			this.ctx.translate(this.canvas.width, 0);
			this.ctx.rotate(Math.PI/2);
			this.ctx.drawImage(image, 0, 0, this.canvas.height, this.canvas.width); // draw video
			this.ctx.restore();
		} else {
			this.ctx.drawImage(image, 0, 0, this.canvas.width, this.canvas.height); // draw video
		}

		var imageData = this.ctx.getImageData(0, 0, this.canvas.width, this.canvas.height);
		var data = imageData.data;

		if (this.dataHeap) {
			this.dataHeap.set( data );
			return true;
		}
		return false;
	};

	ARController.prototype._debugMarker = function(marker) {
		var vertex, pos;
		vertex = marker.vertex;
		var ctx = this.ctx;
		ctx.strokeStyle = 'red';

		ctx.beginPath()
		ctx.moveTo(vertex[0][0], vertex[0][1])
		ctx.lineTo(vertex[1][0], vertex[1][1])
		ctx.stroke();

		ctx.beginPath()
		ctx.moveTo(vertex[2][0], vertex[2][1])
		ctx.lineTo(vertex[3][0], vertex[3][1])
		ctx.stroke()

		ctx.strokeStyle = 'green';
		ctx.beginPath()
		ctx.lineTo(vertex[1][0], vertex[1][1])
		ctx.lineTo(vertex[2][0], vertex[2][1])
		ctx.stroke();

		ctx.beginPath()
		ctx.moveTo(vertex[3][0], vertex[3][1])
		ctx.lineTo(vertex[0][0], vertex[0][1])
		ctx.stroke();

		pos = marker.pos
		ctx.beginPath()
		ctx.arc(pos[0], pos[1], 8, 0, Math.PI * 2)
		ctx.fillStyle = 'red'
		ctx.fill()
	};


	// static

	/**
		ARController.getUserMedia gets a device camera video feed and calls the given onSuccess callback with it.

		Tries to start playing the video. Playing the video can fail on Chrome for Android,
		so ARController.getUserMedia adds user input event listeners to the window
		that try to start playing the video. On success, the event listeners are removed.

		To use ARController.getUserMedia, call it with an object with the onSuccess attribute set to a callback function.

			ARController.getUserMedia({
				onSuccess: function(video) {
					console.log("Got video", video);
				}
			});

		The configuration object supports the following attributes:

			{
				onSuccess : function(video),
				onError : function(error),

				width : number | {min: number, ideal: number, max: number},
				height : number | {min: number, ideal: number, max: number},

				facingMode : 'environment' | 'user' | 'left' | 'right' | { exact: 'environment' | ... }
			}

		See https://developer.mozilla.org/en-US/docs/Web/API/MediaDevices/getUserMedia for more information about the
		width, height and facingMode attributes.

		@param {object} configuration The configuration object.
		@return {VideoElement} Returns the created video element.
	*/
	ARController.getUserMedia = function(configuration) {
		var facing = configuration.facingMode || 'environment';

		var onSuccess = configuration.onSuccess;
		var onError = configuration.onError || function(err) { console.error("ARController.getUserMedia", err); };

		var video = document.createElement('video');

		var initProgress = function() {
			if (this.videoWidth !== 0) {
				onSuccess(video);
			}
		};

		var readyToPlay = false;
		var eventNames = [
			'touchstart', 'touchend', 'touchmove', 'touchcancel',
			'click', 'mousedown', 'mouseup', 'mousemove',
			'keydown', 'keyup', 'keypress', 'scroll'
		];
		var play = function(ev) {
			if (readyToPlay) {
				video.play();
				if (!video.paused) {
					eventNames.forEach(function(eventName) {
						window.removeEventListener(eventName, play, true);
					});
				}
			}
		};
		eventNames.forEach(function(eventName) {
			window.addEventListener(eventName, play, true);
		});

		var success = function(stream) {
			video.addEventListener('loadedmetadata', initProgress, false);
			video.src = window.URL.createObjectURL(stream);
			readyToPlay = true;
			play(); // Try playing without user input, should work on non-Android Chrome
		};

		var constraints = {};
		var mediaDevicesConstraints = {};
		if (configuration.width) {
			mediaDevicesConstraints.width = configuration.width;
			if (typeof configuration.width === 'object') {
				if (configuration.width.max) {
					constraints.maxWidth = configuration.width.max;
				}
				if (configuration.width.min) {
					constraints.minWidth = configuration.width.max;
				}
			} else {
				constraints.maxWidth = configuration.width;
			}
		}

		if (configuration.height) {
			mediaDevicesConstraints.height = configuration.height;
			if (typeof configuration.height === 'object') {
				if (configuration.height.max) {
					constraints.maxHeight = configuration.height.max;
				}
				if (configuration.height.min) {
					constraints.minHeight = configuration.height.max;
				}
			} else {
				constraints.maxHeight = configuration.height;
			}
		}

		mediaDevicesConstraints.facingMode = facing;

		navigator.getUserMedia  = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia;
		var hdConstraints = {
			audio: false,
			video: {
				mandatory: constraints
		  	}
		};

		if ( false ) {
		// if ( navigator.mediaDevices || window.MediaStreamTrack) {
			if (navigator.mediaDevices) {
				navigator.mediaDevices.getUserMedia({
					audio: false,
					video: mediaDevicesConstraints
				}).then(success, onError); 
			} else {
				MediaStreamTrack.getSources(function(sources) {
					var facingDir = mediaDevicesConstraints.facingMode;
					if (facing && facing.exact) {
						facingDir = facing.exact;
					}
					for (var i=0; i<sources.length; i++) {
						if (sources[i].kind === 'video' && sources[i].facing === facingDir) {
							hdConstraints.video.mandatory.sourceId = sources[i].id;
							break;
						}
					}
					if (facing && facing.exact && !hdConstraints.video.mandatory.sourceId) {
						onError('Failed to get camera facing the wanted direction');
					} else {
						if (navigator.getUserMedia) {
							navigator.getUserMedia(hdConstraints, success, onError);
						} else {
							onError('navigator.getUserMedia is not supported on your browser');
						}
					}
				});
			}
		} else {
			if (navigator.getUserMedia) {
				navigator.getUserMedia(hdConstraints, success, onError);
			} else {
				onError('navigator.getUserMedia is not supported on your browser');
			}
		}

		return video;
	};

	/**
		ARController.getUserMediaARController gets an ARController for the device camera video feed and calls the 
		given onSuccess callback with it.

		To use ARController.getUserMediaARController, call it with an object with the cameraParam attribute set to
		a camera parameter file URL, and the onSuccess attribute set to a callback function.

			ARController.getUserMediaARController({
				cameraParam: 'Data/camera_para.dat',
				onSuccess: function(arController, arCameraParam) {
					console.log("Got ARController", arController);
					console.log("Got ARCameraParam", arCameraParam);
					console.log("Got video", arController.image);
				}
			});

		The configuration object supports the following attributes:

			{
				onSuccess : function(ARController, ARCameraParam),
				onError : function(error),

				cameraParam: url, // URL to camera parameters definition file.
				maxARVideoSize: number, // Maximum max(width, height) for the AR processing canvas.

				width : number | {min: number, ideal: number, max: number},
				height : number | {min: number, ideal: number, max: number},

				facingMode : 'environment' | 'user' | 'left' | 'right' | { exact: 'environment' | ... }
			}

		See https://developer.mozilla.org/en-US/docs/Web/API/MediaDevices/getUserMedia for more information about the
		width, height and facingMode attributes.

		The orientation attribute of the returned ARController is set to "portrait" if the userMedia video has larger
		height than width. Otherwise it's set to "landscape". The videoWidth and videoHeight attributes of the arController
		are set to be always in landscape configuration so that width is larger than height.

		@param {object} configuration The configuration object.
		@return {VideoElement} Returns the created video element.
	*/
	ARController.getUserMediaARController = function(configuration) {
		var obj = {};
		for (var i in configuration) {
			obj[i] = configuration[i];
		}
		var onSuccess = configuration.onSuccess;
		var cameraParamURL = configuration.cameraParam;

		obj.onSuccess = function() {
			new ARCameraParam(cameraParamURL, function() {
				var arCameraParam = this;
				var maxSize = configuration.maxARVideoSize || Math.max(video.videoWidth, video.videoHeight);
				var f = maxSize / Math.max(video.videoWidth, video.videoHeight);
				var w = f * video.videoWidth;
				var h = f * video.videoHeight;
				if (video.videoWidth < video.videoHeight) {
					var tmp = w;
					w = h;
					h = tmp;
				}
				var arController = new ARController(w, h, arCameraParam);
				arController.image = video;
				if (video.videoWidth < video.videoHeight) {
					arController.orientation = 'portrait';
					arController.videoWidth = video.videoHeight;
					arController.videoHeight = video.videoWidth;
				} else {
					arController.orientation = 'landscape';
					arController.videoWidth = video.videoWidth;
					arController.videoHeight = video.videoHeight;
				}
				onSuccess(arController, arCameraParam);
			}, function(err) {
				console.error("ARController: Failed to load ARCameraParam", err);
			});
		};

		var video = this.getUserMedia(obj);
		return video;
	};


	/** 
		ARCameraParam is used for loading AR camera parameters for use with ARController.
		Use by passing in an URL and a callback function.

			var camera = new ARCameraParam('Data/camera_para.dat', function() {
				console.log('loaded camera', this.id);
			},
			function(err) {
				console.log('failed to load camera', err);
			});

		@exports ARCameraParam
		@constructor
	 
		@param {string} src URL to load camera parameters from.
		@param {string} onload Onload callback to be called on successful parameter loading.
		@param {string} onerror Error callback to called when things don't work out.
	*/
	var ARCameraParam = function(src, onload, onerror) {
		this.id = -1;
		this._src = '';
		this.complete = false;
		this.onload = onload;
		this.onerror = onerror;
		if (src) {
			this.load(src);
		}
	};

	/** 
		Loads the given URL as camera parameters definition file into this ARCameraParam.

		Can only be called on an unloaded ARCameraParam instance. 

		@param {string} src URL to load.
	*/
	ARCameraParam.prototype.load = function(src) {
		if (this._src !== '') {
			throw("ARCameraParam: Trying to load camera parameters twice.")
		}
		this._src = src;
		if (src) {
			var self = this;
			artoolkit.loadCamera(src, function(id) {
				self.id = id;
				self.complete = true;
				self.onload();
			}, function(err) {
				self.onerror(err);
			});
		}
	};

	Object.defineProperty(ARCameraParam.prototype, 'src', {
		set: function(src) {
			this.load(src);
		},
		get: function() {
			return this._src;
		}
	});

	/**
		Destroys the camera parameter and frees associated Emscripten resources.

	*/
	ARCameraParam.prototype.dispose = function() {
		if (this.id !== -1) {
			artoolkit.deleteCamera(this.id);
		}
		this.id = -1;
		this._src = '';
		this.complete = false;
	};



	// ARToolKit exported JS API
	//
	var artoolkit = {

		UNKNOWN_MARKER: -1,
		PATTERN_MARKER: 0,
		BARCODE_MARKER: 1,

		loadCamera: loadCamera,

		addMarker: addMarker,
		addMultiMarker: addMultiMarker,

	};

	var FUNCTIONS = [
		'setup',
		'teardown',

		'setLogLevel',
		'getLogLevel',

		'setDebugMode',
		'getDebugMode',

		'getProcessingImage',

		'setMarkerInfoDir',
		'setMarkerInfoVertex',

		'getTransMatSquare',
		'getTransMatSquareCont',

		'getTransMatMultiSquare',
		'getTransMatMultiSquareRobust',

		'getMultiMarkerNum',
		'getMultiMarkerCount',

		'detectMarker',
		'getMarkerNum',

		'getMarker',
		'getMultiEachMarker',

		'setProjectionNearPlane',
		'getProjectionNearPlane',

		'setProjectionFarPlane',
		'getProjectionFarPlane',

		'setThresholdMode',
		'getThresholdMode',

		'setThreshold',
		'getThreshold',

		'setPatternDetectionMode',
		'getPatternDetectionMode',

		'setMatrixCodeType',
		'getMatrixCodeType',

		'setLabelingMode',
		'getLabelingMode',

		'setPattRatio',
		'getPattRatio',

		'setImageProcMode',
		'getImageProcMode',
	];

	function runWhenLoaded() {
		FUNCTIONS.forEach(function(n) {
			artoolkit[n] = Module[n];
		})

		for (var m in Module) {
			if (m.match(/^AR/))
			artoolkit[m] = Module[m];
		}
	}

	var marker_count = 0;
	function addMarker(arId, url, callback) {
		var filename = '/marker_' + marker_count++;
		ajax(url, filename, function() {
			var id = Module._addMarker(arId, filename);
			if (callback) callback(id);
		});
	}

	function bytesToString(array) {
		return String.fromCharCode.apply(String, array);
	}

	function parseMultiFile(bytes) {
		var str = bytesToString(bytes);

		var lines = str.split('\n');

		var files = [];

		var state = 0; // 0 - read,
		var markers = 0;

		lines.forEach(function(line) {
			line = line.trim();
			if (!line || line.startsWith('#')) return;

			switch (state) {
				case 0:
					markers = +line;
					state = 1;
					return;
				case 1: // filename or barcode
					if (!line.match(/^\d+$/)) {
						files.push(line);
					}
				case 2: // width
				case 3: // matrices
				case 4:
					state++;
					return;
				case 5:
					state = 1;
					return;
			}
		});

		return files;
	}

	var multi_marker_count = 0;

	function addMultiMarker(arId, url, callback) {
		var filename = '/multi_marker_' + multi_marker_count++;
		ajax(url, filename, function(bytes) {
			var files = parseMultiFile(bytes);

			function ok() {
				var markerID = Module._addMultiMarker(arId, filename);
				var markerNum = Module.getMultiMarkerNum(arId, markerID);
				if (callback) callback(markerID, markerNum);
			}

			if (!files.length) return ok();

			var path = url.split('/').slice(0, -1).join('/')
			files = files.map(function(file) {
				// FIXME super kludge - remove it
				// console.assert(file !== '')
				if( file === 'patt.hiro' || file === 'patt.kanji' || file === 'patt2.hiro' || file === 'patt2.kanji' ){
					// debugger
					return ['http://127.0.0.1:8080/data/data/' + file, file]
				}
				return [path + '/' + file, file]
			})
			ajaxDependencies(files, ok);
		});
	}

	var camera_count = 0;
	function loadCamera(url, callback) {
		var filename = '/camera_param_' + camera_count++;
		var writeCallback = function() {
			var id = Module._loadCamera(filename);
			if (callback) callback(id);
		};
		if (typeof url === 'object') { // Maybe it's a byte array
			writeByteArrayToFS(filename, url, writeCallback);
		} else if (url.indexOf("\n") > -1) { // Or a string with the camera param
			writeStringToFS(filename, url, writeCallback);
		} else {
			ajax(url, filename, writeCallback);
		}
	}


	// transfer image

	function writeStringToFS(target, string, callback) {
		var byteArray = new Uint8Array(string.length);
		for (var i=0; i<byteArray.length; i++) {
			byteArray[i] = string.charCodeAt(i) & 0xff;
		}
		writeByteArrayToFS(target, byteArray, callback);
	}

	function writeByteArrayToFS(target, byteArray, callback) {
		FS.writeFile(target, byteArray, { encoding: 'binary' });
		// console.log('FS written', target);

		callback(byteArray);
	}

	// Eg.
	//	ajax('../bin/Data2/markers.dat', '/Data2/markers.dat', callback);
	//	ajax('../bin/Data/patt.hiro', '/patt.hiro', callback);

	function ajax(url, target, callback) {
		var oReq = new XMLHttpRequest();
		oReq.open('GET', url, true);
		oReq.responseType = 'arraybuffer'; // blob arraybuffer

		oReq.onload = function(oEvent) {
			// console.log('ajax done for ', url);
			var arrayBuffer = oReq.response;
			var byteArray = new Uint8Array(arrayBuffer);
	// console.log('writeByteArrayToFS', target, byteArray.length, 'byte. url', url)
			writeByteArrayToFS(target, byteArray, callback);
		};

		oReq.send();
	}

	function ajaxDependencies(files, callback) {
		var next = files.pop();
		if (next) {
			ajax(next[0], next[1], function() {
				ajaxDependencies(files, callback);
			});
		} else {
			callback();
		}
	}

	/* Exports */
	window.artoolkit = artoolkit;
	window.ARController = ARController;
	window.ARCameraParam = ARCameraParam;

	if (window.Module) {
		runWhenLoaded();
	} else {
		window.Module = {
			onRuntimeInitialized: function() {
				runWhenLoaded();
			}
		};
	}

})();
/*
Copyright (c) 2011 Juan Mellado

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

/*
References:
- "ArUco: a minimal library for Augmented Reality applications based on OpenCv"
http://www.uco.es/investiga/grupos/ava/node/26
*/

var AR = AR || {};

AR.Marker = function(id, corners){
        this.id = id;
        this.corners = corners;
};

AR.Detector = function(){
        this.grey = new CV.Image();
        this.thres = new CV.Image();
        this.homography = new CV.Image();
        this.binary = [];
        this.contours = [];
        this.polys = [];
        this.candidates = [];
};

AR.Detector.prototype.detect = function(image){
        CV.grayscale(image, this.grey);
        CV.adaptiveThreshold(this.grey, this.thres, 2, 7);
        
        this.contours = CV.findContours(this.thres, this.binary);
        
        this.candidates = this.findCandidates(this.contours, image.width * 0.20, 0.05, 10);
        this.candidates = this.clockwiseCorners(this.candidates);
        this.candidates = this.notTooNear(this.candidates, 10);
        
        return this.findMarkers(this.grey, this.candidates, 49);
};

AR.Detector.prototype.findCandidates = function(contours, minSize, epsilon, minLength){
        var candidates = [], len = contours.length, contour, poly, i;
        
        this.polys = [];
        
        for (i = 0; i < len; ++ i){
                contour = contours[i];
                
                if (contour.length >= minSize){
                        poly = CV.approxPolyDP(contour, contour.length * epsilon);
                        
                        this.polys.push(poly);
                        
                        if ( (4 === poly.length) && ( CV.isContourConvex(poly) ) ){
                                
                                if ( CV.minEdgeLength(poly) >= minLength){
                                        candidates.push(poly);
                                }
                        }
                }
        }
        
        return candidates;
};

AR.Detector.prototype.clockwiseCorners = function(candidates){
        var len = candidates.length, dx1, dx2, dy1, dy2, swap, i;
        
        for (i = 0; i < len; ++ i){
                dx1 = candidates[i][1].x - candidates[i][0].x;
                dy1 = candidates[i][1].y - candidates[i][0].y;
                dx2 = candidates[i][2].x - candidates[i][0].x;
                dy2 = candidates[i][2].y - candidates[i][0].y;
                
                if ( (dx1 * dy2 - dy1 * dx2) < 0){
                        swap = candidates[i][1];
                        candidates[i][1] = candidates[i][3];
                        candidates[i][3] = swap;
                }
        }
        
        return candidates;
};

AR.Detector.prototype.notTooNear = function(candidates, minDist){
        var notTooNear = [], len = candidates.length, dist, dx, dy, i, j, k;
        
        for (i = 0; i < len; ++ i){
                
                for (j = i + 1; j < len; ++ j){
                        dist = 0;
                        
                        for (k = 0; k < 4; ++ k){
                                dx = candidates[i][k].x - candidates[j][k].x;
                                dy = candidates[i][k].y - candidates[j][k].y;
                                
                                dist += dx * dx + dy * dy;
                        }
                        
                        if ( (dist / 4) < (minDist * minDist) ){
                                
                                if ( CV.perimeter( candidates[i] ) < CV.perimeter( candidates[j] ) ){
                                        candidates[i].tooNear = true;
                                }else{
                                        candidates[j].tooNear = true;
                                }
                        }
                }
        }
        
        for (i = 0; i < len; ++ i){
                if ( !candidates[i].tooNear ){
                        notTooNear.push( candidates[i] );
                }
        }
        
        return notTooNear;
};

AR.Detector.prototype.findMarkers = function(imageSrc, candidates, warpSize){
        var markers = [], len = candidates.length, candidate, marker, i;
        
        for (i = 0; i < len; ++ i){
                candidate = candidates[i];
                
                CV.warp(imageSrc, this.homography, candidate, warpSize);
                
                CV.threshold(this.homography, this.homography, CV.otsu(this.homography) );
                
                marker = this.getMarker(this.homography, candidate);
                if (marker){
                        markers.push(marker);
                }
        }
        
        return markers;
};

AR.Detector.prototype.getMarker = function(imageSrc, candidate){
        var width = (imageSrc.width / 7) >>> 0,
        minZero = (width * width) >> 1,
        bits = [], rotations = [], distances = [],
        square, pair, inc, i, j;
        
        for (i = 0; i < 7; ++ i){
                inc = (0 === i || 6 === i)? 1: 6;
                
                for (j = 0; j < 7; j += inc){
                        square = {x: j * width, y: i * width, width: width, height: width};
                        if ( CV.countNonZero(imageSrc, square) > minZero){
                                return null;
                        }
                }
        }
        
        for (i = 0; i < 5; ++ i){
                bits[i] = [];
                
                for (j = 0; j < 5; ++ j){
                        square = {x: (j + 1) * width, y: (i + 1) * width, width: width, height: width};
                        
                        bits[i][j] = CV.countNonZero(imageSrc, square) > minZero? 1: 0;
                }
        }
        
        rotations[0] = bits;
        distances[0] = this.hammingDistance( rotations[0] );
        
        pair = {first: distances[0], second: 0};
        
        for (i = 1; i < 4; ++ i){
                rotations[i] = this.rotate( rotations[i - 1] );
                distances[i] = this.hammingDistance( rotations[i] );
                
                if (distances[i] < pair.first){
                        pair.first = distances[i];
                        pair.second = i;
                }
        }
        
        if (0 !== pair.first){
                return null;
        }
        
        return new AR.Marker(
                this.mat2id( rotations[pair.second] ), 
                this.rotate2(candidate, 4 - pair.second) 
        );
};

AR.Detector.prototype.hammingDistance = function(bits){
        var ids = [ [1,0,0,0,0], [1,0,1,1,1], [0,1,0,0,1], [0,1,1,1,0] ],
        dist = 0, sum, minSum, i, j, k;
        
        for (i = 0; i < 5; ++ i){
                minSum = Infinity;
                
                for (j = 0; j < 4; ++ j){
                        sum = 0;
                        
                        for (k = 0; k < 5; ++ k){
                                sum += bits[i][k] === ids[j][k]? 0: 1;
                        }
                        
                        if (sum < minSum){
                                minSum = sum;
                        }
                }
                
                dist += minSum;
        }
        
        return dist;
};

AR.Detector.prototype.mat2id = function(bits){
        var id = 0, i;
        
        for (i = 0; i < 5; ++ i){
                id <<= 1;
                id |= bits[i][1];
                id <<= 1;
                id |= bits[i][3];
        }
        
        return id;
};

AR.Detector.prototype.rotate = function(src){
        var dst = [], len = src.length, i, j;
        
        for (i = 0; i < len; ++ i){
                dst[i] = [];
                for (j = 0; j < src[i].length; ++ j){
                        dst[i][j] = src[src[i].length - j - 1][i];
                }
        }
        
        return dst;
};

AR.Detector.prototype.rotate2 = function(src, rotation){
        var dst = [], len = src.length, i;
        
        for (i = 0; i < len; ++ i){
                dst[i] = src[ (rotation + i) % len ];
        }
        
        return dst;
};
/*
Copyright (c) 2011 Juan Mellado

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

/*
References:
- "OpenCV: Open Computer Vision Library"
http://sourceforge.net/projects/opencvlibrary/
- "Stack Blur: Fast But Goodlooking"
http://incubator.quasimondo.com/processing/fast_blur_deluxe.php
*/

var CV = CV || {};

CV.Image = function(width, height, data){
        this.width = width || 0;
        this.height = height || 0;
        this.data = data || [];
};

CV.grayscale = function(imageSrc, imageDst){
        var src = imageSrc.data, dst = imageDst.data, len = src.length,
        i = 0, j = 0;
        
        for (; i < len; i += 4){
                dst[j ++] =
                (src[i] * 0.299 + src[i + 1] * 0.587 + src[i + 2] * 0.114 + 0.5) & 0xff;
        }
        
        imageDst.width = imageSrc.width;
        imageDst.height = imageSrc.height;
        
        return imageDst;
};

CV.threshold = function(imageSrc, imageDst, threshold){
        var src = imageSrc.data, dst = imageDst.data,
        len = src.length, tab = [], i;
        
        for (i = 0; i < 256; ++ i){
                tab[i] = i <= threshold? 0: 255;
        }
        
        for (i = 0; i < len; ++ i){
                dst[i] = tab[ src[i] ];
        }
        
        imageDst.width = imageSrc.width;
        imageDst.height = imageSrc.height;
        
        return imageDst;
};

CV.adaptiveThreshold = function(imageSrc, imageDst, kernelSize, threshold){
        var src = imageSrc.data, dst = imageDst.data, len = src.length, tab = [], i;
        
        CV.stackBoxBlur(imageSrc, imageDst, kernelSize);
        
        for (i = 0; i < 768; ++ i){
                tab[i] = (i - 255 <= -threshold)? 255: 0;
        }
        
        for (i = 0; i < len; ++ i){
                dst[i] = tab[ src[i] - dst[i] + 255 ];
        }
        
        imageDst.width = imageSrc.width;
        imageDst.height = imageSrc.height;
        
        return imageDst;
};

CV.otsu = function(imageSrc){
        var src = imageSrc.data, len = src.length, hist = [],
        threshold = 0, sum = 0, sumB = 0, wB = 0, wF = 0, max = 0,
        mu, between, i;
        
        for (i = 0; i < 256; ++ i){
                hist[i] = 0;
        }
        
        for (i = 0; i < len; ++ i){
                hist[ src[i] ] ++;
        }
        
        for (i = 0; i < 256; ++ i){
                sum += hist[i] * i;
        }
        
        for (i = 0; i < 256; ++ i){
                wB += hist[i];
                if (0 !== wB){
                        
                        wF = len - wB;
                        if (0 === wF){
                                break;
                        }
                        
                        sumB += hist[i] * i;
                        
                        mu = (sumB / wB) - ( (sum - sumB) / wF );
                        
                        between = wB * wF * mu * mu;
                        
                        if (between > max){
                                max = between;
                                threshold = i;
                        }
                }
        }
        
        return threshold;
};

CV.stackBoxBlurMult =
[1, 171, 205, 293, 57, 373, 79, 137, 241, 27, 391, 357, 41, 19, 283, 265];

CV.stackBoxBlurShift =
[0, 9, 10, 11, 9, 12, 10, 11, 12, 9, 13, 13, 10, 9, 13, 13];

CV.BlurStack = function(){
        this.color = 0;
        this.next = null;
};

CV.stackBoxBlur = function(imageSrc, imageDst, kernelSize){
        var src = imageSrc.data, dst = imageDst.data,
        height = imageSrc.height, width = imageSrc.width,
        heightMinus1 = height - 1, widthMinus1 = width - 1,
        size = kernelSize + kernelSize + 1, radius = kernelSize + 1,
        mult = CV.stackBoxBlurMult[kernelSize],
        shift = CV.stackBoxBlurShift[kernelSize],
        stack, stackStart, color, sum, pos, start, p, x, y, i;
        
        stack = stackStart = new CV.BlurStack();
        for (i = 1; i < size; ++ i){
                stack = stack.next = new CV.BlurStack();
        }
        stack.next = stackStart;
        
        pos = 0;
        
        for (y = 0; y < height; ++ y){
                start = pos;
                
                color = src[pos];
                sum = radius * color;
                
                stack = stackStart;
                for (i = 0; i < radius; ++ i){
                        stack.color = color;
                        stack = stack.next;
                }
                for (i = 1; i < radius; ++ i){
                        stack.color = src[pos + i];
                        sum += stack.color;
                        stack = stack.next;
                }
                
                stack = stackStart;
                for (x = 0; x < width; ++ x){
                        dst[pos ++] = (sum * mult) >>> shift;
                        
                        p = x + radius;
                        p = start + (p < widthMinus1? p: widthMinus1);
                        sum -= stack.color - src[p];
                        
                        stack.color = src[p];
                        stack = stack.next;
                }
        }
        
        for (x = 0; x < width; ++ x){
                pos = x;
                start = pos + width;
                
                color = dst[pos];
                sum = radius * color;
                
                stack = stackStart;
                for (i = 0; i < radius; ++ i){
                        stack.color = color;
                        stack = stack.next;
                }
                for (i = 1; i < radius; ++ i){
                        stack.color = dst[start];
                        sum += stack.color;
                        stack = stack.next;
                        
                        start += width;
                }
                
                stack = stackStart;
                for (y = 0; y < height; ++ y){
                        dst[pos] = (sum * mult) >>> shift;
                        
                        p = y + radius;
                        p = x + ( (p < heightMinus1? p: heightMinus1) * width );
                        sum -= stack.color - dst[p];
                        
                        stack.color = dst[p];
                        stack = stack.next;
                        
                        pos += width;
                }
        }
        
        return imageDst;
};

CV.gaussianBlur = function(imageSrc, imageDst, imageMean, kernelSize){
        var kernel = CV.gaussianKernel(kernelSize);
        
        imageDst.width = imageSrc.width;
        imageDst.height = imageSrc.height;
        
        imageMean.width = imageSrc.width;
        imageMean.height = imageSrc.height;
        
        CV.gaussianBlurFilter(imageSrc, imageMean, kernel, true);
        CV.gaussianBlurFilter(imageMean, imageDst, kernel, false);
        
        return imageDst;
};

CV.gaussianBlurFilter = function(imageSrc, imageDst, kernel, horizontal){
        var src = imageSrc.data, dst = imageDst.data,
        height = imageSrc.height, width = imageSrc.width,
        pos = 0, limit = kernel.length >> 1,
        cur, value, i, j, k;
        
        for (i = 0; i < height; ++ i){
                
                for (j = 0; j < width; ++ j){
                        value = 0.0;
                        
                        for (k = -limit; k <= limit; ++ k){
                                
                                if (horizontal){
                                        cur = pos + k;
                                        if (j + k < 0){
                                                cur = pos;
                                        }
                                        else if (j + k >= width){
                                                cur = pos;
                                        }
                                }else{
                                        cur = pos + (k * width);
                                        if (i + k < 0){
                                                cur = pos;
                                        }
                                        else if (i + k >= height){
                                                cur = pos;
                                        }
                                }
                                
                                value += kernel[limit + k] * src[cur];
                        }
                        
                        dst[pos ++] = horizontal? value: (value + 0.5) & 0xff;
                }
        }
        
        return imageDst;
};

CV.gaussianKernel = function(kernelSize){
        var tab =
        [ [1],
        [0.25, 0.5, 0.25],
        [0.0625, 0.25, 0.375, 0.25, 0.0625],
        [0.03125, 0.109375, 0.21875, 0.28125, 0.21875, 0.109375, 0.03125] ],
        kernel = [], center, sigma, scale2X, sum, x, i;
        
        if ( (kernelSize <= 7) && (kernelSize % 2 === 1) ){
                kernel = tab[kernelSize >> 1];
        }else{
                center = (kernelSize - 1.0) * 0.5;
                sigma = 0.8 + (0.3 * (center - 1.0) );
                scale2X = -0.5 / (sigma * sigma);
                sum = 0.0;
                for (i = 0; i < kernelSize; ++ i){
                        x = i - center;
                        sum += kernel[i] = Math.exp(scale2X * x * x);
                }
                sum = 1 / sum;
                for (i = 0; i < kernelSize; ++ i){
                        kernel[i] *= sum;
                }  
        }
        
        return kernel;
};

CV.findContours = function(imageSrc, binary){
        var width = imageSrc.width, height = imageSrc.height, contours = [],
        src, deltas, pos, pix, nbd, outer, hole, i, j;
        
        src = CV.binaryBorder(imageSrc, binary);
        
        deltas = CV.neighborhoodDeltas(width + 2);
        
        pos = width + 3;
        nbd = 1;
        
        for (i = 0; i < height; ++ i, pos += 2){
                
                for (j = 0; j < width; ++ j, ++ pos){
                        pix = src[pos];
                        
                        if (0 !== pix){
                                outer = hole = false;
                                
                                if (1 === pix && 0 === src[pos - 1]){
                                        outer = true;
                                }
                                else if (pix >= 1 && 0 === src[pos + 1]){
                                        hole = true;
                                }
                                
                                if (outer || hole){
                                        ++ nbd;
                                        
                                        contours.push( CV.borderFollowing(src, pos, nbd, {x: j, y: i}, hole, deltas) );
                                }
                        }
                }
        }  
        
        return contours;
};

CV.borderFollowing = function(src, pos, nbd, point, hole, deltas){
        var contour = [], pos1, pos3, pos4, s, s_end, s_prev;
        
        contour.hole = hole;
        
        s = s_end = hole? 0: 4;
        do{
                s = (s - 1) & 7;
                pos1 = pos + deltas[s];
                if (src[pos1] !== 0){
                        break;
                }
        }while(s !== s_end);
        
        if (s === s_end){
                src[pos] = -nbd;
                contour.push( {x: point.x, y: point.y} );
                
        }else{
                pos3 = pos;
                s_prev = s ^ 4;
                
                while(true){
                        s_end = s;
                        
                        do{
                                pos4 = pos3 + deltas[++ s];
                        }while(src[pos4] === 0);
                        
                        s &= 7;
                        
                        if ( ( (s - 1) >>> 0) < (s_end >>> 0) ){
                                src[pos3] = -nbd;
                        }
                        else if (src[pos3] === 1){
                                src[pos3] = nbd;
                        }
                        
                        contour.push( {x: point.x, y: point.y} );
                        
                        s_prev = s;
                        
                        point.x += CV.neighborhood[s][0];
                        point.y += CV.neighborhood[s][1];
                        
                        if ( (pos4 === pos) && (pos3 === pos1) ){
                                break;
                        }
                        
                        pos3 = pos4;
                        s = (s + 4) & 7;
                }
        }
        
        return contour;
};

CV.neighborhood = 
[ [1, 0], [1, -1], [0, -1], [-1, -1], [-1, 0], [-1, 1], [0, 1], [1, 1] ];

CV.neighborhoodDeltas = function(width){
        var deltas = [], len = CV.neighborhood.length, i = 0;
        
        for (; i < len; ++ i){
                deltas[i] = CV.neighborhood[i][0] + (CV.neighborhood[i][1] * width);
        }
        
        return deltas.concat(deltas);
};

CV.approxPolyDP = function(contour, epsilon){
        var slice = {start_index: 0, end_index: 0},
        right_slice = {start_index: 0, end_index: 0},
        poly = [], stack = [], len = contour.length,
        pt, start_pt, end_pt, dist, max_dist, le_eps,
        dx, dy, i, j, k;
        
        epsilon *= epsilon;
        
        k = 0;
        
        for (i = 0; i < 3; ++ i){
                max_dist = 0;
                
                k = (k + right_slice.start_index) % len;
                start_pt = contour[k];
                if (++ k === len) {k = 0;}
                
                for (j = 1; j < len; ++ j){
                        pt = contour[k];
                        if (++ k === len) {k = 0;}
                        
                        dx = pt.x - start_pt.x;
                        dy = pt.y - start_pt.y;
                        dist = dx * dx + dy * dy;
                        
                        if (dist > max_dist){
                                max_dist = dist;
                                right_slice.start_index = j;
                        }
                }
        }
        
        if (max_dist <= epsilon){
                poly.push( {x: start_pt.x, y: start_pt.y} );
                
        }else{
                slice.start_index = k;
                slice.end_index = (right_slice.start_index += slice.start_index);
                
                right_slice.start_index -= right_slice.start_index >= len? len: 0;
                right_slice.end_index = slice.start_index;
                if (right_slice.end_index < right_slice.start_index){
                        right_slice.end_index += len;
                }
                
                stack.push( {start_index: right_slice.start_index, end_index: right_slice.end_index} );
                stack.push( {start_index: slice.start_index, end_index: slice.end_index} );
        }
        
        while(stack.length !== 0){
                slice = stack.pop();
                
                end_pt = contour[slice.end_index % len];
                start_pt = contour[k = slice.start_index % len];
                if (++ k === len) {k = 0;}
                
                if (slice.end_index <= slice.start_index + 1){
                        le_eps = true;
                        
                }else{
                        max_dist = 0;
                        
                        dx = end_pt.x - start_pt.x;
                        dy = end_pt.y - start_pt.y;
                        
                        for (i = slice.start_index + 1; i < slice.end_index; ++ i){
                                pt = contour[k];
                                if (++ k === len) {k = 0;}
                                
                                dist = Math.abs( (pt.y - start_pt.y) * dx - (pt.x - start_pt.x) * dy);
                                
                                if (dist > max_dist){
                                        max_dist = dist;
                                        right_slice.start_index = i;
                                }
                        }
                        
                        le_eps = max_dist * max_dist <= epsilon * (dx * dx + dy * dy);
                }
                
                if (le_eps){
                        poly.push( {x: start_pt.x, y: start_pt.y} );
                        
                }else{
                        right_slice.end_index = slice.end_index;
                        slice.end_index = right_slice.start_index;
                        
                        stack.push( {start_index: right_slice.start_index, end_index: right_slice.end_index} );
                        stack.push( {start_index: slice.start_index, end_index: slice.end_index} );
                }
        }
        
        return poly;
};

CV.warp = function(imageSrc, imageDst, contour, warpSize){
        var src = imageSrc.data, dst = imageDst.data,
        width = imageSrc.width, height = imageSrc.height,
        pos = 0,
        sx1, sx2, dx1, dx2, sy1, sy2, dy1, dy2, p1, p2, p3, p4,
        m, r, s, t, u, v, w, x, y, i, j;
        
        m = CV.getPerspectiveTransform(contour, warpSize - 1);
        
        r = m[8];
        s = m[2];
        t = m[5];
        
        for (i = 0; i < warpSize; ++ i){
                r += m[7];
                s += m[1];
                t += m[4];
                
                u = r;
                v = s;
                w = t;
                
                for (j = 0; j < warpSize; ++ j){
                        u += m[6];
                        v += m[0];
                        w += m[3];
                        
                        x = v / u;
                        y = w / u;
                        
                        sx1 = x >>> 0;
                        sx2 = (sx1 === width - 1)? sx1: sx1 + 1;
                        dx1 = x - sx1;
                        dx2 = 1.0 - dx1;
                        
                        sy1 = y >>> 0;
                        sy2 = (sy1 === height - 1)? sy1: sy1 + 1;
                        dy1 = y - sy1;
                        dy2 = 1.0 - dy1;
                        
                        p1 = p2 = sy1 * width;
                        p3 = p4 = sy2 * width;
                        
                        dst[pos ++] = 
                        (dy2 * (dx2 * src[p1 + sx1] + dx1 * src[p2 + sx2]) +
                        dy1 * (dx2 * src[p3 + sx1] + dx1 * src[p4 + sx2]) ) & 0xff;
                        
                }
        }
        
        imageDst.width = warpSize;
        imageDst.height = warpSize;
        
        return imageDst;
};

CV.getPerspectiveTransform = function(src, size){
        var rq = CV.square2quad(src);
        
        rq[0] /= size;
        rq[1] /= size;
        rq[3] /= size;
        rq[4] /= size;
        rq[6] /= size;
        rq[7] /= size;
        
        return rq;
};

CV.square2quad = function(src){
        var sq = [], px, py, dx1, dx2, dy1, dy2, den;
        
        px = src[0].x - src[1].x + src[2].x - src[3].x;
        py = src[0].y - src[1].y + src[2].y - src[3].y;
        
        if (0 === px && 0 === py){
                sq[0] = src[1].x - src[0].x;
                sq[1] = src[2].x - src[1].x;
                sq[2] = src[0].x;
                sq[3] = src[1].y - src[0].y;
                sq[4] = src[2].y - src[1].y;
                sq[5] = src[0].y;
                sq[6] = 0;
                sq[7] = 0;
                sq[8] = 1;
                
        }else{
                dx1 = src[1].x - src[2].x;
                dx2 = src[3].x - src[2].x;
                dy1 = src[1].y - src[2].y;
                dy2 = src[3].y - src[2].y;
                den = dx1 * dy2 - dx2 * dy1;
                
                sq[6] = (px * dy2 - dx2 * py) / den;
                sq[7] = (dx1 * py - px * dy1) / den;
                sq[8] = 1;
                sq[0] = src[1].x - src[0].x + sq[6] * src[1].x;
                sq[1] = src[3].x - src[0].x + sq[7] * src[3].x;
                sq[2] = src[0].x;
                sq[3] = src[1].y - src[0].y + sq[6] * src[1].y;
                sq[4] = src[3].y - src[0].y + sq[7] * src[3].y;
                sq[5] = src[0].y;
        }
        
        return sq;
};

CV.isContourConvex = function(contour){
        var orientation = 0, convex = true,
        len = contour.length, i = 0, j = 0,
        cur_pt, prev_pt, dxdy0, dydx0, dx0, dy0, dx, dy;
        
        prev_pt = contour[len - 1];
        cur_pt = contour[0];
        
        dx0 = cur_pt.x - prev_pt.x;
        dy0 = cur_pt.y - prev_pt.y;
        
        for (; i < len; ++ i){
                if (++ j === len) {j = 0;}
                
                prev_pt = cur_pt;
                cur_pt = contour[j];
                
                dx = cur_pt.x - prev_pt.x;
                dy = cur_pt.y - prev_pt.y;
                dxdy0 = dx * dy0;
                dydx0 = dy * dx0;
                
                orientation |= dydx0 > dxdy0? 1: (dydx0 < dxdy0? 2: 3);
                
                if (3 === orientation){
                        convex = false;
                        break;
                }
                
                dx0 = dx;
                dy0 = dy;
        }
        
        return convex;
};

CV.perimeter = function(poly){
        var len = poly.length, i = 0, j = len - 1,
        p = 0.0, dx, dy;
        
        for (; i < len; j = i ++){
                dx = poly[i].x - poly[j].x;
                dy = poly[i].y - poly[j].y;
                
                p += Math.sqrt(dx * dx + dy * dy) ;
        }
        
        return p;
};

CV.minEdgeLength = function(poly){
        var len = poly.length, i = 0, j = len - 1, 
        min = Infinity, d, dx, dy;
        
        for (; i < len; j = i ++){
                dx = poly[i].x - poly[j].x;
                dy = poly[i].y - poly[j].y;
                
                d = dx * dx + dy * dy;
                
                if (d < min){
                        min = d;
                }
        }
        
        return Math.sqrt(min);
};

CV.countNonZero = function(imageSrc, square){
        var src = imageSrc.data, height = square.height, width = square.width,
        pos = square.x + (square.y * imageSrc.width),
        span = imageSrc.width - width,
        nz = 0, i, j;
        
        for (i = 0; i < height; ++ i){
                
                for (j = 0; j < width; ++ j){
                        
                        if ( 0 !== src[pos ++] ){
                                ++ nz;
                        }
                }
                
                pos += span;
        }
        
        return nz;
};

CV.binaryBorder = function(imageSrc, dst){
        var src = imageSrc.data, height = imageSrc.height, width = imageSrc.width,
        posSrc = 0, posDst = 0, i, j;
        
        for (j = -2; j < width; ++ j){
                dst[posDst ++] = 0;
        }
        
        for (i = 0; i < height; ++ i){
                dst[posDst ++] = 0;
                
                for (j = 0; j < width; ++ j){
                        dst[posDst ++] = (0 === src[posSrc ++]? 0: 1);
                }
                
                dst[posDst ++] = 0;
        }
        
        for (j = -2; j < width; ++ j){
                dst[posDst ++] = 0;
        }
        
        return dst;
};
/*
Copyright (c) 2012 Juan Mellado

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

/*
References:
- "Iterative Pose Estimation using Coplanar Feature Points"
Denis Oberkampf, Daniel F. DeMenthon, Larry S. Davis
http://www.cfar.umd.edu/~daniel/daniel_papersfordownload/CoplanarPts.pdf
*/

var POS = POS || {};

POS.Posit = function(modelSize, focalLength){
        this.objectPoints = this.buildModel(modelSize);
        this.focalLength = focalLength;
        
        this.objectVectors = [];
        this.objectNormal = [];
        this.objectMatrix = [[],[],[]];
        
        this.init();
};

POS.Posit.prototype.buildModel = function(modelSize){
        var half = modelSize / 2.0;
        
        return [
                [-half,  half, 0.0],
                [ half,  half, 0.0],
                [ half, -half, 0.0],
                [-half, -half, 0.0] 
        ];
};

POS.Posit.prototype.init = function(){
        var np = this.objectPoints.length,
        vectors = [], n = [], len = 0.0, row = 2, i;
        
        for (i = 0; i < np; ++ i){
                this.objectVectors[i] = [this.objectPoints[i][0] - this.objectPoints[0][0],
                this.objectPoints[i][1] - this.objectPoints[0][1],
                this.objectPoints[i][2] - this.objectPoints[0][2]];
                
                vectors[i] = [this.objectVectors[i][0],
                this.objectVectors[i][1],
                this.objectVectors[i][2]];
        }
        
        while(0.0 === len){
                n[0] = this.objectVectors[1][1] * this.objectVectors[row][2] -
                this.objectVectors[1][2] * this.objectVectors[row][1];
                n[1] = this.objectVectors[1][2] * this.objectVectors[row][0] -
                this.objectVectors[1][0] * this.objectVectors[row][2];
                n[2] = this.objectVectors[1][0] * this.objectVectors[row][1] -
                this.objectVectors[1][1] * this.objectVectors[row][0];
                
                len = Math.sqrt(n[0] * n[0] + n[1] * n[1] + n[2] * n[2]);
                
                ++ row;
        }
        
        for (i = 0; i < 3; ++ i){
                this.objectNormal[i] = n[i] / len;
        }
        
        POS.pseudoInverse(vectors, np, this.objectMatrix);
};

POS.Posit.prototype.pose = function(imagePoints){
        var posRotation1 = [[],[],[]], posRotation2 = [[],[],[]], posTranslation = [],
        rotation1 = [[],[],[]], rotation2 = [[],[],[]], translation1 = [], translation2 = [],
        error1, error2, valid1, valid2, i, j;
        
        this.pos(imagePoints, posRotation1, posRotation2, posTranslation);
        
        valid1 = this.isValid(posRotation1, posTranslation);
        if (valid1){
                error1 = this.iterate(imagePoints, posRotation1, posTranslation, rotation1, translation1);
        }else{
                error1 = {euclidean: -1.0, pixels: -1, maximum: -1.0};
        }
        
        valid2 = this.isValid(posRotation2, posTranslation);
        if (valid2){
                error2 = this.iterate(imagePoints, posRotation2, posTranslation, rotation2, translation2);
        }else{
                error2 = {euclidean: -1.0, pixels: -1, maximum: -1.0};
        }
        
        for (i = 0; i < 3; ++ i){
                for (j = 0; j < 3; ++ j){
                        if (valid1){
                                translation1[i] -= rotation1[i][j] * this.objectPoints[0][j];
                        }
                        if (valid2){
                                translation2[i] -= rotation2[i][j] * this.objectPoints[0][j];
                        }
                }
        }
        
        return error1.euclidean < error2.euclidean?
        new POS.Pose(error1.pixels, rotation1, translation1, error2.pixels, rotation2, translation2):
        new POS.Pose(error2.pixels, rotation2, translation2, error1.pixels, rotation1, translation1);
};

POS.Posit.prototype.pos = function(imagePoints, rotation1, rotation2, translation){
        var np = this.objectPoints.length, imageVectors = [],
        i0 = [], j0 = [], ivec = [], jvec = [], row1 = [], row2 = [], row3 = [],
        i0i0, j0j0, i0j0, delta, q, lambda, mu, scale, i, j;
        
        for (i = 0; i < np; ++ i){
                imageVectors[i] = [imagePoints[i].x - imagePoints[0].x,
                imagePoints[i].y - imagePoints[0].y];
        }
        
        //i0 and j0
        for (i = 0; i < 3; ++ i){
                i0[i] = 0.0;
                j0[i] = 0.0;
                for (j = 0; j < np; ++ j){
                        i0[i] += this.objectMatrix[i][j] * imageVectors[j][0];
                        j0[i] += this.objectMatrix[i][j] * imageVectors[j][1];
                }
        }
        
        i0i0 = i0[0] * i0[0] + i0[1] * i0[1] + i0[2] * i0[2];
        j0j0 = j0[0] * j0[0] + j0[1] * j0[1] + j0[2] * j0[2];
        i0j0 = i0[0] * j0[0] + i0[1] * j0[1] + i0[2] * j0[2];
        
        //Lambda and mu
        delta = (j0j0 - i0i0) * (j0j0 - i0i0) + 4.0 * (i0j0 * i0j0);
        
        if (j0j0 - i0i0 >= 0.0){
                q = (j0j0 - i0i0 + Math.sqrt(delta) ) / 2.0;
        }else{
                q = (j0j0 - i0i0 - Math.sqrt(delta) ) / 2.0;
        }
        
        if (q >= 0.0){
                lambda = Math.sqrt(q);
                if (0.0 === lambda){
                        mu = 0.0;
                }else{
                        mu = -i0j0 / lambda;
                }
        }else{
                lambda = Math.sqrt( -(i0j0 * i0j0) / q);
                if (0.0 === lambda){
                        mu = Math.sqrt(i0i0 - j0j0);
                }else{
                        mu = -i0j0 / lambda;
                }
        }
        
        //First rotation
        for (i = 0; i < 3; ++ i){
                ivec[i] = i0[i] + lambda * this.objectNormal[i];
                jvec[i] = j0[i] + mu * this.objectNormal[i];
        }
        
        scale = Math.sqrt(ivec[0] * ivec[0] + ivec[1] * ivec[1] + ivec[2] * ivec[2]);
        
        for (i = 0; i < 3; ++ i){
                row1[i] = ivec[i] / scale;
                row2[i] = jvec[i] / scale;
        }
        
        row3[0] = row1[1] * row2[2] - row1[2] * row2[1];
        row3[1] = row1[2] * row2[0] - row1[0] * row2[2];
        row3[2] = row1[0] * row2[1] - row1[1] * row2[0];
        
        for (i = 0; i < 3; ++ i){
                rotation1[0][i] = row1[i];
                rotation1[1][i] = row2[i];
                rotation1[2][i] = row3[i];
        }
        
        //Second rotation
        for (i = 0; i < 3; ++ i){
                ivec[i] = i0[i] - lambda * this.objectNormal[i];
                jvec[i] = j0[i] - mu * this.objectNormal[i];
        }
        
        for (i = 0; i < 3; ++ i){
                row1[i] = ivec[i] / scale;
                row2[i] = jvec[i] / scale;
        }
        
        row3[0] = row1[1] * row2[2] - row1[2] * row2[1];
        row3[1] = row1[2] * row2[0] - row1[0] * row2[2];
        row3[2] = row1[0] * row2[1] - row1[1] * row2[0];
        
        for (i = 0; i < 3; ++ i){
                rotation2[0][i] = row1[i];
                rotation2[1][i] = row2[i];
                rotation2[2][i] = row3[i];
        }
        
        //Translation
        translation[0] = imagePoints[0].x / scale;
        translation[1] = imagePoints[0].y / scale;
        translation[2] = this.focalLength / scale;
};

POS.Posit.prototype.isValid = function(rotation, translation){
        var np = this.objectPoints.length, zmin = Infinity, i = 0, zi;
        
        for (; i < np; ++ i){
                zi = translation[2] +
                (rotation[2][0] * this.objectVectors[i][0] +
                        rotation[2][1] * this.objectVectors[i][1] +
                        rotation[2][2] * this.objectVectors[i][2]
                );
                if (zi < zmin){
                        zmin = zi;
                }
        }
        
        return zmin >= 0.0;
};

POS.Posit.prototype.iterate = function(imagePoints, posRotation, posTranslation, rotation, translation){
        var np = this.objectPoints.length,
        oldSopImagePoints = [], sopImagePoints = [],
        rotation1 = [[],[],[]], rotation2 = [[],[],[]],
        translation1 = [], translation2 = [],
        converged = false, iteration = 0,
        oldImageDifference, imageDifference, factor,
        error, error1, error2, delta, i, j;
        
        for (i = 0; i < np; ++ i){
                oldSopImagePoints[i] = {x: imagePoints[i].x,
                        y: imagePoints[i].y
                };
        }
        
        for (i = 0; i < 3; ++ i){
                for (j = 0; j < 3; ++ j){
                        rotation[i][j] = posRotation[i][j];
                }
                translation[i] = posTranslation[i];
        }
        
        for (i = 0; i < np; ++ i){
                factor = 0.0;
                for (j = 0; j < 3; ++ j){
                        factor += this.objectVectors[i][j] * rotation[2][j] / translation[2];
                }
                sopImagePoints[i] = {x: (1.0 + factor) * imagePoints[i].x,
                        y: (1.0 + factor) * imagePoints[i].y
                };
        }
        
        imageDifference = 0.0;
        
        for (i = 0; i < np; ++ i){
                imageDifference += Math.abs(sopImagePoints[i].x - oldSopImagePoints[i].x);
                imageDifference += Math.abs(sopImagePoints[i].y - oldSopImagePoints[i].y);
        }
        
        for (i = 0; i < 3; ++ i){
                translation1[i] = translation[i] -
                (rotation[i][0] * this.objectPoints[0][0] +
                        rotation[i][1] * this.objectPoints[0][1] +
                        rotation[i][2] * this.objectPoints[0][2]
                );
        }
        
        error = error1 = this.error(imagePoints, rotation, translation1);
        
        //Convergence
        converged = (0.0 === error1.pixels) || (imageDifference < 0.01);
        
        while( iteration ++ < 100 && !converged ){
                
                for (i = 0; i < np; ++ i){
                        oldSopImagePoints[i].x = sopImagePoints[i].x;
                        oldSopImagePoints[i].y = sopImagePoints[i].y;
                }
                
                this.pos(sopImagePoints, rotation1, rotation2, translation);
                
                for (i = 0; i < 3; ++ i){
                        translation1[i] = translation[i] -
                        (rotation1[i][0] * this.objectPoints[0][0] +
                                rotation1[i][1] * this.objectPoints[0][1] +
                                rotation1[i][2] * this.objectPoints[0][2]
                        );
                        
                        translation2[i] = translation[i] -
                        (rotation2[i][0] * this.objectPoints[0][0] +
                                rotation2[i][1] * this.objectPoints[0][1] +
                                rotation2[i][2] * this.objectPoints[0][2]
                        );
                }
                
                error1 = this.error(imagePoints, rotation1, translation1);
                error2 = this.error(imagePoints, rotation2, translation2);
                
                if ( (error1.euclidean >= 0.0) && (error2.euclidean >= 0.0) ){
                        if (error2.euclidean < error1.euclidean){
                                error = error2;
                                for (i = 0; i < 3; ++ i){
                                        for (j = 0; j < 3; ++ j){
                                                rotation[i][j] = rotation2[i][j];
                                        }
                                }
                        }else{
                                error = error1;
                                for (i = 0; i < 3; ++ i){
                                        for (j = 0; j < 3; ++ j){
                                                rotation[i][j] = rotation1[i][j];
                                        }
                                }
                        }
                }
                
                if ( (error1.euclidean < 0.0) && (error2.euclidean >= 0.0) ){
                        error = error2;
                        for (i = 0; i < 3; ++ i){
                                for (j = 0; j < 3; ++ j){
                                        rotation[i][j] = rotation2[i][j];
                                }
                        }
                }
                
                if ( (error2.euclidean < 0.0) && (error1.euclidean >= 0.0) ){
                        error = error1;
                        for (i = 0; i < 3; ++ i){
                                for (j = 0; j < 3; ++ j){
                                        rotation[i][j] = rotation1[i][j];
                                }
                        }
                }
                
                for (i = 0; i < np; ++ i){
                        factor = 0.0;
                        for (j = 0; j < 3; ++ j){
                                factor += this.objectVectors[i][j] * rotation[2][j] / translation[2];
                        }
                        sopImagePoints[i].x = (1.0 + factor) * imagePoints[i].x;
                        sopImagePoints[i].y = (1.0 + factor) * imagePoints[i].y;
                }
                
                oldImageDifference = imageDifference;
                imageDifference = 0.0;
                
                for (i = 0; i < np; ++ i){
                        imageDifference += Math.abs(sopImagePoints[i].x - oldSopImagePoints[i].x);
                        imageDifference += Math.abs(sopImagePoints[i].y - oldSopImagePoints[i].y);
                }
                
                delta = Math.abs(imageDifference - oldImageDifference);
                
                converged = (0.0 === error.pixels) || (delta < 0.01);
        }
        
        return error;
};

POS.Posit.prototype.error = function(imagePoints, rotation, translation){
        var np = this.objectPoints.length,
        move = [], projection = [], errorvec = [],
        euclidean = 0.0, pixels = 0.0, maximum = 0.0,
        i, j, k;
        
        if ( !this.isValid(rotation, translation) ){
                return {euclidean: -1.0, pixels: -1, maximum: -1.0};
        }
        
        for (i = 0; i < np; ++ i){
                move[i] = [];
                for (j = 0; j < 3; ++ j){
                        move[i][j] = translation[j];
                }
        }
        
        for (i = 0; i < np; ++ i){
                for (j = 0; j < 3; ++ j){
                        for (k = 0; k < 3; ++ k){
                                move[i][j] += rotation[j][k] * this.objectPoints[i][k];
                        }
                }
        }
        
        for (i = 0; i < np; ++ i){
                projection[i] = [];
                for (j = 0; j < 2; ++ j){
                        projection[i][j] = this.focalLength * move[i][j] / move[i][2];
                }
        }
        
        for (i = 0; i < np; ++ i){
                errorvec[i] = [projection[i][0] - imagePoints[i].x,
                projection[i][1] - imagePoints[i].y];
        }
        
        for (i = 0; i < np; ++ i){
                euclidean += Math.sqrt(errorvec[i][0] * errorvec[i][0] +
                        errorvec[i][1] * errorvec[i][1]
                );
                
                pixels += Math.abs( Math.round(projection[i][0]) - Math.round(imagePoints[i].x) ) +
                Math.abs( Math.round(projection[i][1]) - Math.round(imagePoints[i].y) );
                
                if (Math.abs(errorvec[i][0]) > maximum){
                        maximum = Math.abs(errorvec[i][0]);
                }
                if (Math.abs(errorvec[i][1]) > maximum){
                        maximum = Math.abs(errorvec[i][1]);
                }
        }
        
        return {euclidean: euclidean / np, pixels: pixels, maximum: maximum};
};

POS.pseudoInverse = function(a, n, b){
        var w = [], v = [[],[],[]], s = [[],[],[]],
        wmax = 0.0, cn = 0,
        i, j, k;
        
        SVD.svdcmp(a, n, 3, w, v);
        
        for (i = 0; i < 3; ++ i){
                if (w[i] > wmax){
                        wmax = w[i];
                }
        }
        
        wmax *= 0.01;
        
        for (i = 0; i < 3; ++ i){
                if (w[i] < wmax){
                        w[i] = 0.0;
                }
        }
        
        for (j = 0; j < 3; ++ j){
                if (0.0 === w[j]){
                        ++ cn;
                        for (k = j; k < 2; ++ k){
                                for (i = 0; i < n; ++ i){
                                        a[i][k] = a[i][k + 1];
                                }
                                for (i = 0; i < 3; ++ i){
                                        v[i][k] = v[i][k + 1];
                                }
                        }
                }
        }
        
        for (j = 0; j < 2; ++ j){
                if (0.0 === w[j]){
                        w[j] = w[j + 1];
                }
        }
        
        for (i = 0; i < 3; ++ i){
                for (j = 0; j < 3 - cn; ++ j){
                        s[i][j] = v[i][j] / w[j];
                }
        }
        
        for (i = 0; i < 3; ++ i){
                for (j = 0; j < n; ++ j){
                        b[i][j] = 0.0;
                        for (k = 0; k < 3 - cn; ++ k){
                                b[i][j] += s[i][k] * a[j][k];
                        }
                }
        }
};

POS.Pose = function(error1, rotation1, translation1, error2, rotation2, translation2){
        this.bestError = error1;
        this.bestRotation = rotation1;
        this.bestTranslation = translation1;
        this.alternativeError = error2;
        this.alternativeRotation = rotation2;
        this.alternativeTranslation = translation2;
};
/*
Copyright (c) 2012 Juan Mellado

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

/*
References:
- "Numerical Recipes in C - Second Edition"
  http://www.nr.com/
*/

var SVD = SVD || {};

SVD.svdcmp = function(a, m, n, w, v){
  var flag, i, its, j, jj, k, l, nm,
      anorm = 0.0, c, f, g = 0.0, h, s, scale = 0.0, x, y, z, rv1 = [];
      
  //Householder reduction to bidiagonal form
  for (i = 0; i < n; ++ i){
    l = i + 1;
    rv1[i] = scale * g;
    g = s = scale = 0.0;
    if (i < m){
      for (k = i; k < m; ++ k){
        scale += Math.abs( a[k][i] );
      }
      if (0.0 !== scale){
        for (k = i; k < m; ++ k){
          a[k][i] /= scale;
          s += a[k][i] * a[k][i];
        }
        f = a[i][i];
        g = -SVD.sign( Math.sqrt(s), f );
        h = f * g - s;
        a[i][i] = f - g;
        for (j = l; j < n; ++ j){
          for (s = 0.0, k = i; k < m; ++ k){
            s += a[k][i] * a[k][j];
          }
          f = s / h;
          for (k = i; k < m; ++ k){
            a[k][j] += f * a[k][i];
          }
        }
        for (k = i; k < m; ++ k){
          a[k][i] *= scale;
        }
      }
    }
    w[i] = scale * g;
    g = s = scale = 0.0;
    if ( (i < m) && (i !== n - 1) ){
      for (k = l; k < n; ++ k){
        scale += Math.abs( a[i][k] );
      }
      if (0.0 !== scale){
        for (k = l; k < n; ++ k){
          a[i][k] /= scale;
          s += a[i][k] * a[i][k];
        }
        f = a[i][l];
        g = -SVD.sign( Math.sqrt(s), f );
        h = f * g - s;
        a[i][l] = f - g;
        for (k = l; k < n; ++ k){
          rv1[k] = a[i][k] / h;
        }
        for (j = l; j < m; ++ j){
          for (s = 0.0, k = l; k < n; ++ k){
            s += a[j][k] * a[i][k];
          }
          for (k = l; k < n; ++ k){
            a[j][k] += s * rv1[k];
          }
        }
        for (k = l; k < n; ++ k){
          a[i][k] *= scale;
        }
      }
    }
    anorm = Math.max(anorm, ( Math.abs( w[i] ) + Math.abs( rv1[i] ) ) );
  }

  //Acumulation of right-hand transformation
  for (i = n - 1; i >= 0; -- i){
    if (i < n - 1){
      if (0.0 !== g){
        for (j = l; j < n; ++ j){
          v[j][i] = ( a[i][j] / a[i][l] ) / g;
        }
        for (j = l; j < n; ++ j){
          for (s = 0.0, k = l; k < n; ++ k){
            s += a[i][k] * v[k][j];
          }
          for (k = l; k < n; ++ k){
            v[k][j] += s * v[k][i];
          }
        }
      }
      for (j = l; j < n; ++ j){
        v[i][j] = v[j][i] = 0.0;
      }
    }
    v[i][i] = 1.0;
    g = rv1[i];
    l = i;
  }

  //Acumulation of left-hand transformation
  for (i = Math.min(n, m) - 1; i >= 0; -- i){
    l = i + 1;
    g = w[i];
    for (j = l; j < n; ++ j){
      a[i][j] = 0.0;
    }
    if (0.0 !== g){
      g = 1.0 / g;
      for (j = l; j < n; ++ j){
        for (s = 0.0, k = l; k < m; ++ k){
          s += a[k][i] * a[k][j];
        }
        f = (s / a[i][i]) * g;
        for (k = i; k < m; ++ k){
          a[k][j] += f * a[k][i];
        }
      }
      for (j = i; j < m; ++ j){
        a[j][i] *= g;
      }
    }else{
        for (j = i; j < m; ++ j){
          a[j][i] = 0.0;
        }
    }
    ++ a[i][i];
  }

  //Diagonalization of the bidiagonal form
  for (k = n - 1; k >= 0; -- k){
    for (its = 1; its <= 30; ++ its){
      flag = true;
      for (l = k; l >= 0; -- l){
        nm = l - 1;
        if ( Math.abs( rv1[l] ) + anorm === anorm ){
          flag = false;
          break;
        }
        if ( Math.abs( w[nm] ) + anorm === anorm ){
          break;
        }
      }
      if (flag){
        c = 0.0;
        s = 1.0;
        for (i = l; i <= k; ++ i){
          f = s * rv1[i];
          if ( Math.abs(f) + anorm === anorm ){
            break;
          }
          g = w[i];
          h = SVD.pythag(f, g);
          w[i] = h;
          h = 1.0 / h;
          c = g * h;
          s = -f * h;
          for (j = 1; j <= m; ++ j){
            y = a[j][nm];
            z = a[j][i];
            a[j][nm] = y * c + z * s;
            a[j][i] = z * c - y * s;
          }
        }
      }

      //Convergence
      z = w[k];
      if (l === k){
        if (z < 0.0){
          w[k] = -z;
          for (j = 0; j < n; ++ j){
            v[j][k] = -v[j][k];
          }
        }
        break;
      }

      if (30 === its){
        return false;
      }

      //Shift from bottom 2-by-2 minor
      x = w[l];
      nm = k - 1;
      y = w[nm];
      g = rv1[nm];
      h = rv1[k];
      f = ( (y - z) * (y + z) + (g - h) * (g + h) ) / (2.0 * h * y);
      g = SVD.pythag( f, 1.0 );
      f = ( (x - z) * (x + z) + h * ( (y / (f + SVD.sign(g, f) ) ) - h) ) / x;

      //Next QR transformation
      c = s = 1.0;
      for (j = l; j <= nm; ++ j){
        i = j + 1;
        g = rv1[i];
        y = w[i];
        h = s * g;
        g = c * g;
        z = SVD.pythag(f, h);
        rv1[j] = z;
        c = f / z;
        s = h / z;
        f = x * c + g * s;
        g = g * c - x * s;
        h = y * s;
        y *= c;
        for (jj = 0; jj < n; ++ jj){
          x = v[jj][j];
          z = v[jj][i];
          v[jj][j] = x * c + z * s;
          v[jj][i] = z * c - x * s;
        }
        z = SVD.pythag(f, h);
        w[j] = z;
        if (0.0 !== z){
          z = 1.0 / z;
          c = f * z;
          s = h * z;
        }
        f = c * g + s * y;
        x = c * y - s * g;
        for (jj = 0; jj < m; ++ jj){
          y = a[jj][j];
          z = a[jj][i];
          a[jj][j] = y * c + z * s;
          a[jj][i] = z * c - y * s;
        }
      }
      rv1[l] = 0.0;
      rv1[k] = f;
      w[k] = x;
    }
  }

  return true;
};

SVD.pythag = function(a, b){
  var at = Math.abs(a), bt = Math.abs(b), ct;

  if (at > bt){
    ct = bt / at;
    return at * Math.sqrt(1.0 + ct * ct);
  }
    
  if (0.0 === bt){
    return 0.0;
  }

  ct = at / bt;
  return bt * Math.sqrt(1.0 + ct * ct);
};

SVD.sign = function(a, b){
  return b >= 0.0? Math.abs(a): -Math.abs(a);
};
var THREEx = THREEx || {}

THREEx.ArucoContext = function(parameters){
	// handle default parameters
	parameters = parameters || {}
	this.parameters = {
		// debug - true if one should display artoolkit debug canvas, false otherwise
		debug: parameters.debug !== undefined ? parameters.debug : false,
		// resolution of at which we detect pose in the source image
		canvasWidth: parameters.canvasWidth !== undefined ? parameters.canvasWidth : 640,
		canvasHeight: parameters.canvasHeight !== undefined ? parameters.canvasHeight : 480,
	}


        this.canvas = document.createElement('canvas');
                
        this.detector = new AR.Detector()
        
        // setup THREEx.ArucoDebug if needed
        this.debug = null
        if( parameters.debug == true ){
                this.debug = new THREEx.ArucoDebug(this)
        }
	
	// honor parameters.canvasWidth/.canvasHeight
	this.setSize(this.parameters.canvasWidth, this.parameters.canvasHeight)
}

THREEx.ArucoContext.prototype.setSize = function (width, height) {
        this.canvas.width = width
        this.canvas.height = height
        if( this.debug !== null ){
                this.debug.setSize(width, height)
        }
}

THREEx.ArucoContext.prototype.detect = function (videoElement) {
	var _this = this
        var canvas = this.canvas
        
        // get imageData from videoElement
        var context = canvas.getContext('2d');
        context.drawImage(videoElement, 0, 0, canvas.width, canvas.height);
        var imageData = context.getImageData(0, 0, canvas.width, canvas.height);
        
        // detect markers in imageData
        var detectedMarkers = this.detector.detect(imageData);
	return detectedMarkers
};

/**
 * crappy function to update a object3d with a detectedMarker - super crappy
 */
THREEx.ArucoContext.prototype.updateObject3D = function(object3D, arucoPosit, markerSize, detectedMarker){
        var markerCorners = detectedMarker.corners;
        var canvas = this.canvas

        // convert the corners
        var poseCorners = new Array(markerCorners.length)
        for (var i = 0; i < markerCorners.length; ++ i){
                var markerCorner = markerCorners[i];        
                poseCorners[i] = {
                        x:  markerCorner.x - (canvas.width / 2),
                        y: -markerCorner.y + (canvas.height/ 2)
                }
        }
        
        // estimate pose from corners
        var pose = arucoPosit.pose(poseCorners);


	var rotation    = pose.bestRotation
	var translation = pose.bestTranslation
	
        object3D.position.x =  translation[0];
        object3D.position.y =  translation[1];
        object3D.position.z = -translation[2];
        
        object3D.rotation.x = -Math.asin(-rotation[1][2]);
        object3D.rotation.y = -Math.atan2(rotation[0][2], rotation[2][2]);
        object3D.rotation.z =  Math.atan2(rotation[1][0], rotation[1][1]);
        
        object3D.scale.x = markerSize;
        object3D.scale.y = markerSize;
        object3D.scale.z = markerSize;
}
var THREEx	= THREEx || {};


//////////////////////////////////////////////////////////////////////////////
//		monkey patch AR.Detector
//////////////////////////////////////////////////////////////////////////////

AR.Detector.prototype.detect = function(image){
	var opts = this.datGUIOptions

        CV.grayscale(image, this.grey);
        CV.adaptiveThreshold(this.grey, this.thres, opts.adaptativeThreshold.kernelSize, opts.adaptativeThreshold.threshold);
        
        this.contours = CV.findContours(this.thres, this.binary);
        
        this.candidates = this.findCandidates(this.contours, image.width * opts.candidates.minSize, opts.candidates.epsilon, opts.candidates.minLength);
        this.candidates = this.clockwiseCorners(this.candidates);
        this.candidates = this.notTooNear(this.candidates, opts.notTooNear.minDist);
        
        return this.findMarkers(this.grey, this.candidates, opts.findMarkers.warpSize);
};

// make names explicits - make unit explicit too
AR.Detector.prototype.datGUIOptions = {
	adaptativeThreshold : {
		kernelSize: 2,
		threshold: 7,
	},
	candidates: {
		minSize: 0.20,
		epsilon: 0.05,
		minLength: 10,
	},
	notTooNear : {
		minDist: 10,
	},
	findMarkers : {
		warpSize: 49,
	}
}

// see https://github.com/jeromeetienne/threex.geometricglow/blob/master/threex.atmospherematerialdatgui.js

THREEx.addArucoDatGui	= function(arucoContext, datGui){
	var datGUIOptions = arucoContext.detector.datGUIOptions
	var options  = {
		resolution: '640x480',
	}
	var onChange = function(){
		// honor option resolution
		var matches = options.resolution.match(/(\d+)x(\d+)/)
		var width = parseInt(matches[1])
		var height = parseInt(matches[2])
		arucoContext.setSize(width, height)
	}
	onChange();

	datGui.add( options, 'resolution', [ '320x240', '640x480' ]).onChange( onChange )
	
	var folder = datGui.addFolder('Adaptative Threshold')
	folder.open()
	folder.add( arucoContext.detector.datGUIOptions.adaptativeThreshold, 'kernelSize').min(0).step(1)
		.onChange( onChange )
	folder.add( arucoContext.detector.datGUIOptions.adaptativeThreshold, 'threshold').min(0).step(1)
		.onChange( onChange )
	
	var folder = datGui.addFolder('Candidates')
	folder.open()
	folder.add( arucoContext.detector.datGUIOptions.candidates, 'minSize').min(0).max(1)
		.onChange( onChange )
	folder.add( arucoContext.detector.datGUIOptions.candidates, 'epsilon').min(0)
		.onChange( onChange )
	folder.add( arucoContext.detector.datGUIOptions.candidates, 'minLength').min(0).step(1)
		.onChange( onChange )

	var folder = datGui.addFolder('notTooNear')
	folder.open()
	folder.add( arucoContext.detector.datGUIOptions.notTooNear, 'minDist').min(0).step(1)
		.onChange( onChange )
		
	var folder = datGui.addFolder('findMarkers')
	folder.open()
	folder.add( arucoContext.detector.datGUIOptions.findMarkers, 'warpSize').min(0).step(1)
		.onChange( onChange )
}
var THREEx = THREEx || {}

THREEx.ArucoDebug = function(arucoContext){
	this.arucoContext = arucoContext

// TODO to rename canvasElement into canvas
	this.canvasElement = document.createElement('canvas');
	this.canvasElement.width = this.arucoContext.canvas.width
	this.canvasElement.height = this.arucoContext.canvas.height
}

THREEx.ArucoDebug.prototype.setSize = function (width, height) {
        if( this.canvasElement.width !== width )	this.canvasElement.width = width
        if( this.canvasElement.height !== height )	this.canvasElement.height = height
}


THREEx.ArucoDebug.prototype.clear = function(){
	var canvas = this.canvasElement
	var context = canvas.getContext('2d');
	context.clearRect(0,0,canvas.width, canvas.height)
	
}
	
THREEx.ArucoDebug.prototype.drawContoursContours = function(){
	var contours = this.arucoContext.detector.contours
	var canvas = this.canvasElement
	this.drawContours(contours, 0, 0, canvas.width, canvas.height, function(hole){
		return hole? "magenta": "blue"
	})
}

THREEx.ArucoDebug.prototype.drawContoursPolys = function(){
	var contours = this.arucoContext.detector.polys
	var canvas = this.canvasElement
	this.drawContours(contours, 0, 0, canvas.width, canvas.height, function(){
		return 'green'
	})
}


THREEx.ArucoDebug.prototype.drawContoursCandidates = function(){
	var contours = this.arucoContext.detector.candidates
	var canvas = this.canvasElement
	this.drawContours(contours, 0, 0, canvas.width, canvas.height, function(){
		return 'red'
	})
}

THREEx.ArucoDebug.prototype.drawContours = function(contours, x, y, width, height, fn){
	var i = contours.length, j, contour, point;
	var canvas = this.canvasElement
	var context = canvas.getContext('2d');
	
	context.save();
	while(i --){
		contour = contours[i];
		context.strokeStyle = fn(contour.hole);
		context.beginPath();
		for (j = 0; j < contour.length; ++ j){
			point = contour[j];
			context.moveTo(x + point.x, y + point.y);
			point = contour[(j + 1) % contour.length];
			context.lineTo(x + point.x, y + point.y);
		}
		
		context.stroke();
		context.closePath();
	}
	context.restore();
}

//////////////////////////////////////////////////////////////////////////////
//		Code Separator
//////////////////////////////////////////////////////////////////////////////    

THREEx.ArucoDebug.prototype.drawDetectorGrey = function(){
	var cvImage = arucoContext.detector.grey
        this.drawCVImage( cvImage )
}

THREEx.ArucoDebug.prototype.drawDetectorThreshold = function(){
	var cvImage = arucoContext.detector.thres
        this.drawCVImage( cvImage )
}

//////////////////////////////////////////////////////////////////////////////
//		Code Separator
//////////////////////////////////////////////////////////////////////////////
THREEx.ArucoDebug.prototype.drawCVImage = function(cvImage){
	var detector = this.arucoContext.detector

	var canvas = this.canvasElement
	var context = canvas.getContext('2d');

	var imageData = context.createImageData(canvas.width, canvas.height);
	this.copyCVImage2ImageData(cvImage, imageData)
	context.putImageData( imageData, 0, 0);
}


THREEx.ArucoDebug.prototype.copyCVImage2ImageData = function(cvImage, imageData){
	var i = cvImage.data.length, j = (i * 4) + 3;
	
	while(i --){
		imageData.data[j -= 4] = 255;
		imageData.data[j - 1] = imageData.data[j - 2] = imageData.data[j - 3] = cvImage.data[i];
	}
	
	return imageData;
};

//////////////////////////////////////////////////////////////////////////////
//		Code Separator
//////////////////////////////////////////////////////////////////////////////
THREEx.ArucoDebug.prototype.drawVideo = function(videoElement){
	var canvas = this.canvasElement
	var context = canvas.getContext('2d');
	context.drawImage(videoElement, 0, 0, canvas.width, canvas.height);
}

//////////////////////////////////////////////////////////////////////////////
//		Code Separator
//////////////////////////////////////////////////////////////////////////////
THREEx.ArucoDebug.prototype.drawMarkerIDs = function(markers){
	var canvas = this.canvasElement
	var context = canvas.getContext('2d');
	var corners, corner, x, y, i, j;
	
	context.save();
	context.strokeStyle = "blue";
	context.lineWidth = 1;
	
	for (i = 0; i !== markers.length; ++ i){
		corners = markers[i].corners;
		
		x = Infinity;
		y = Infinity;
		
		for (j = 0; j !== corners.length; ++ j){
			corner = corners[j];
			
			x = Math.min(x, corner.x);
			y = Math.min(y, corner.y);
		}
		context.strokeText(markers[i].id, x, y)
	}
	context.restore();
}

//////////////////////////////////////////////////////////////////////////////
//		Code Separator
//////////////////////////////////////////////////////////////////////////////
THREEx.ArucoDebug.prototype.drawMarkerCorners = function(markers){
	var canvas = this.canvasElement
        var corners, corner, i, j;
        var context = canvas.getContext('2d');
	context.save();
        context.lineWidth = 3;
        
        for (i = 0; i < markers.length; ++ i){
                corners = markers[i].corners;
                
                context.strokeStyle = 'red';
                context.beginPath();
                
                for (j = 0; j < corners.length; ++ j){
                        corner = corners[j];
                        context.moveTo(corner.x, corner.y);
                        corner = corners[(j + 1) % corners.length];
                        context.lineTo(corner.x, corner.y);
                }
                
                context.stroke();
                context.closePath();
                
                context.strokeStyle = 'green';
                context.strokeRect(corners[0].x - 2, corners[0].y - 2, 4, 4);
        }
	context.restore();

}
var THREEx = THREEx || {}

THREEx.ArucoMarkerGenerator = function(){
	
}

//////////////////////////////////////////////////////////////////////////////
//		Code Separator
//////////////////////////////////////////////////////////////////////////////
THREEx.ArucoMarkerGenerator.createSVG = function(markerId, svgSize){
	var domElement = document.createElement('div');
	domElement.innerHTML = new ArucoMarker(markerId).toSVG(svgSize);	
	return domElement
}

THREEx.ArucoMarkerGenerator.createImage = function(markerId, width){
	// create canvas
	var canvas = this.createCanvas(markerId, width)
	var imageURL = canvas.toDataURL()

	// create imageElement
	var imageElement = document.createElement('img');
	imageElement.src = imageURL

	// return imageElement
	return imageElement;
}

//////////////////////////////////////////////////////////////////////////////
//		Code Separator
//////////////////////////////////////////////////////////////////////////////
THREEx.ArucoMarkerGenerator.createCanvas = function(markerId, width){
	var canvas = document.createElement('canvas');
	var context = canvas.getContext('2d')
	canvas.width = width
	canvas.height = width

	var arucoMarker = new ArucoMarker(markerId)
	var marker = arucoMarker.markerMatrix()
	
	var margin = canvas.width*0.1
	var innerW = width-margin*2
	var squareW = innerW/7
	
	context.fillStyle = 'white'
	context.fillRect(0, 0, canvas.width, canvas.height)
	context.fillStyle = 'black'
	context.fillRect(margin, margin, canvas.width-margin*2, canvas.height-margin*2)

	for(var y = 0; y < 5; y++){
		for(var x = 0; x < 5; x++){
			if (marker[x][y] !== 1) continue
			context.fillStyle = 'white'
			context.fillRect(margin+(x+1)*squareW, margin+(y+1)*squareW, squareW+1, squareW+1)
		}
	}
	
	return canvas
}
/*
 * Copyright 2017 Google Inc. All Rights Reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
* @namespace
*/
var THREE = THREE || require("three");

/**
* The WebAR namespace inside the THREE namespace. This namespace includes different utilities to be able to handle WebAR functionalities on top of the ThreeJS framework/engine in an easier way.
* 
* NOTE: As a coding standard all the variables/functions starting with an underscore '_' are considered as private and should not be used/called outside of the namespace/class they are defined in.
* @namespace
*/
THREE.WebAR = {};

THREE.WebAR.MAX_FLOAT32_VALUE = 3.4028e38;

/**
* A class that allows to manage the point cloud acquisition and representation in ThreeJS. A buffer geometry is generated to represent the point cloud. The point cloud is provided using a VRDisplay instance that shows the capability to do so. The point cloud is actually exposed using a TypedArray. The array includes 3 values per point in the cloud. There are 2 ways of exposing this array:
* 1.- Using a new TypedArray for every frame/update. The advantage is that the TypedArray is always of the correct size depending on the number of points detected. The disadvantage is that there is a performance hit from the creation and copying of the array (and future garbage collection).
* 2.- Using the same reference to a single TypedArray. The advantage is that the performance is as good as it can get with no creation/destruction and copy penalties. The disadvantage is that the size of the array is the biggest possible point cloud provided by the underlying hardware. The non used values are filled with THREE.WebAR.MAX_FLOAT32_VALUE.
* @constructor
* @param {window.VRDisplay} vrDisplay The reference to the VRDisplay instance that is capable of providing the point cloud.
*
* NOTE: The buffer geometry that can be retrieved from instances of this class can be used along with THREE.Point and THREE.PointMaterial to render the point cloud using points. This class represents the vertices colors with the color white.
*/
THREE.WebAR.VRPointCloud = function(vrDisplay) {

  this._vrDisplay = vrDisplay;

  this._numberOfPointsInLastPointCloud = 0;

  this._bufferGeometry = new THREE.BufferGeometry();
  this._bufferGeometry.frustumCulled = false;

  var positions = null;
  if (vrDisplay) {
    this._pointCloud = new VRPointCloud();
    vrDisplay.getPointCloud(this._pointCloud, false, 0, false);
    positions = this._pointCloud.points;
  }
  else {
    positions = new Float32Array(
      [-1, 1, -2, 1, 1, -2, 1, -1, -2, -1, -1, -2 ]);
  }
  var colors = new Float32Array( positions.length );

  var color = new THREE.Color();

  for ( var i = 0; i < colors.length; i += 3 ) {
    if (vrDisplay) {
      positions[ i ]     = THREE.WebAR.MAX_FLOAT32_VALUE;
      positions[ i + 1 ] = THREE.WebAR.MAX_FLOAT32_VALUE;
      positions[ i + 2 ] = THREE.WebAR.MAX_FLOAT32_VALUE;
    }
    color.setRGB( 1, 1, 1 );
    colors[ i ]     = color.r;
    colors[ i + 1 ] = color.g;
    colors[ i + 2 ] = color.b;
  }

  this._positions = new THREE.BufferAttribute( positions, 3 );
  this._bufferGeometry.addAttribute( 'position', this._positions );
  this._colors = new THREE.BufferAttribute( colors, 3 );
  this._bufferGeometry.addAttribute( 'color', this._colors );

  this._bufferGeometry.computeBoundingSphere();

  return this;
};

/**
* Returns the THREE.BufferGeometry instance that represents the points in the pont cloud.
* @return {THREE.BufferGeometry} The buffer geometry that represents the points in the point cloud.
*
* NOTE: A possible way to render the point cloud could be to use the THREE.BufferGeometry instance returned by this method along with THREE.Points and THREE.PointMaterial.

  var pointCloud = new THREE.VRPointCloud(vrDisplay, true);
  var material = new THREE.PointsMaterial( { size: 0.01, vertexColors: THREE.VertexColors } );
  var points = new THREE.Points( pointCloud.getBufferGeometry(), material );
*/
THREE.WebAR.VRPointCloud.prototype.getBufferGeometry = function() {
  return this._bufferGeometry;
};

/**
* Update the point cloud. The THREE.BufferGeometry that this class provides will automatically be updated with the point cloud retrieved by the underlying hardware.
* @param {boolean} updateBufferGeometry A flag to indicate if the underlying THREE.BufferGeometry should also be updated. Updating the THREE.BufferGeometry is very cost innefficient so it is better to only do it if necessary (only if the buffer geometry is going to be rendered for example). If this flag is set to false,  then the underlying point cloud is updated but not buffer geometry that represents it. Updating the point cloud is important to be able to call functions that operate with it, like the getPickingPointAndPlaneInPointCloud function.
* @param {number} pointsToSkip A positive integer from 0-N that specifies the number of points to skip when returning the point cloud. If the updateBufferGeometry flag is activated (true) then this parameter allows to specify the density of the point cloud. A values of 0 means all the detected points need to be returned. A number of 1 means that 1 every other point needs to be skipped and thus, half of the detected points will be retrieved, and so on. If the parameter is not specified, 0 is considered.
* @param {boolean} transformPoints A flag to specify if the points should be transformed in the native side or not. If the points are not transformed in the native side, they should be transformed in the JS side (in a vertex shader for example).
*/
THREE.WebAR.VRPointCloud.prototype.update = function(updateBufferGeometry, pointsToSkip, transformPoints) {
  if (!this._vrDisplay) return;
  this._vrDisplay.getPointCloud(this._pointCloud, 
    !updateBufferGeometry, typeof(pointsToSkip) === "number" ? 
      pointsToSkip : 0, !!transformPoints);
  if (!updateBufferGeometry) return;
  if (this._pointCloud.numberOfPoints > 0) {
    this._positions.needsUpdate = true;
  }
};

/**
* Provides an index based on an orientation angle. The corresponding index to the angle is:
* orientation =   0 <-> index = 0
* orientation =  90 <-> index = 1
* orientation = 180 <-> index = 2
* orientation = 270 <-> index = 3
* @param {number} orientation The orientation angle. Values are: 0, 90, 180, 270.
* @return {number} An index from 0 to 3 that corresponds to the give orientation angle.
*/
THREE.WebAR.getIndexFromOrientation = function(orientation) {
  var index = 0;
  switch (orientation) {
    case 90:
      index = 1;
      break;
    case 180:
      index = 2;
      break;
    case 270:
      index = 3;
      break;
    default:
      index = 0;
      break;
  }
  return index;
};

/**
* Returns an index that is based on the combination between the display orientation and the see through camera orientation. This index will always be device natural orientation independent.
* @param {VRDisplay} vrDisplay The VRDisplay that is capable to provide a correct VRSeeThroughCamera instance.
* @return {number} The index from 0 to 3 that represents the combination of the device and see through camera orientations.
*/
THREE.WebAR.getIndexFromScreenAndSeeThroughCameraOrientations = function(vrDisplay) {
  var screenOrientation = screen.orientation.angle;
  var seeThroughCamera = vrDisplay ? vrDisplay.getSeeThroughCamera() : null;
  var seeThroughCameraOrientation = seeThroughCamera ? 
    seeThroughCamera.orientation : 0;
  var seeThroughCameraOrientationIndex = 
    THREE.WebAR.getIndexFromOrientation(seeThroughCameraOrientation);
  var screenOrientationIndex = 
    THREE.WebAR.getIndexFromOrientation(screenOrientation);
  ret = screenOrientationIndex - seeThroughCameraOrientationIndex;
  if (ret < 0) {
    ret += 4;
  }
  return (ret % 4);
}

/**
* A utility function that helps create a THREE.Mesh instance to be able to show the VRSeeThroughCamera as a background quad with the correct texture coordinates and a THREE.VideoTexture instance.
* @param {VRDisplay} vrDisplay The VRDisplay that is capable to provide a correct VRSeeThroughCamera instance. It can be null/undefined.
* @param {string} fallbackVideoPath The path to a video in case there is no vrDisplay. If this parameter is not provided, a default video at path "../resources/sintel.webm" will be used.
* @return {THREE.Mesh} The THREE.Mesh instance that represents a quad to be able to present the see through camera.
*/
THREE.WebAR.createVRSeeThroughCameraMesh = function(vrDisplay,
  fallbackVideoPath) {
  var video;
  var geometry = new THREE.BufferGeometry();

  // The camera or video and the texture coordinates may vary depending if the vrDisplay has the see through camera.
  if (vrDisplay) {
    var seeThroughCamera = vrDisplay.getSeeThroughCamera();

    if (!seeThroughCamera) 
      throw "ERROR: Could not get the see through camera!";
    
    video = seeThroughCamera;
    // HACK: Needed to tell the THREE.VideoTexture that the video is ready and
    // that the texture needs update.
    video.readyState = 2;
    video.HAVE_CURRENT_DATA = 2;

    // All the possible texture coordinates for the 4 possible orientations.
    // The ratio between the texture size and the camera size is used in order
    // to be compatible with the YUV to RGB conversion option (not recommended
    // but still available).
    var u = seeThroughCamera.width / seeThroughCamera.textureWidth;
    var v = seeThroughCamera.height / seeThroughCamera.textureHeight;
    geometry.WebAR_textureCoords = [
      new Float32Array([ 
        0.0, 0.0,
        0.0, v,
        u, 0.0,
        u, v
      ]),
      new Float32Array([ 
        u, 0.0,
        0.0, 0.0,
        u, v,
        0.0, v
      ]),
      new Float32Array([
        u, v,
        u, 0.0,
        0.0, v,
        0.0, 0.0
      ]),
      new Float32Array([
        0.0, v,
        u, v,
        0.0, 0.0,
        u, 0.0
      ])
    ];
  }
  else {
    var video = document.createElement("video");
    video.src = typeof(fallbackVideoPath) === "string" ? 
      fallbackVideoPath : "../../resources/videos/sintel.webm";
    video.play();

    // All the possible texture coordinates for the 4 possible orientations.
    geometry.WebAR_textureCoords = [
      new Float32Array([0.0, 0.0, 0.0, 1.0, 1.0, 0.0, 1.0, 1.0]),
      new Float32Array([1.0, 0.0, 0.0, 0.0, 1.0, 1.0, 0.0, 1.0]),
      new Float32Array([1.0, 1.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0]),
      new Float32Array([0.0, 1.0, 1.0, 1.0, 0.0, 0.0, 1.0, 0.0])
    ];
  }

  geometry.addAttribute("position", new THREE.BufferAttribute( 
    new Float32Array([
    -1.0,  1.0, 0.0, 
    -1.0, -1.0, 0.0,
     1.0,  1.0, 0.0, 
     1.0, -1.0, 0.0
  ]), 3));

  geometry.setIndex(new THREE.BufferAttribute(
    new Uint16Array([0, 1, 2, 2, 1, 3]), 1));
  geometry.WebAR_textureCoordIndex = 
    THREE.WebAR.getIndexFromScreenAndSeeThroughCameraOrientations(vrDisplay);
  var textureCoords = 
    geometry.WebAR_textureCoords[geometry.WebAR_textureCoordIndex];

  geometry.addAttribute("uv", new THREE.BufferAttribute(
    new Float32Array(textureCoords), 2 ));
  geometry.computeBoundingSphere();

  var videoTexture = new THREE.VideoTexture(video);
  videoTexture.minFilter = THREE.NearestFilter;
  videoTexture.magFilter = THREE.NearestFilter;
  videoTexture.format = THREE.RGBFormat;
  videoTexture.flipY = false;

  // The material is different if the see through camera is provided inside the vrDisplay or not.
  var material;
  if (vrDisplay) {
    var vertexShaderSource = [
      'attribute vec3 position;',
      'attribute vec2 uv;',
      '',
      'uniform mat4 modelViewMatrix;',
      'uniform mat4 projectionMatrix;',
      '',
      'varying vec2 vUV;',
      '',
      'void main(void) {',
      '    gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);',
      '    vUV = uv;',
      '}'
    ];

    var fragmentShaderSource = [
      '#extension GL_OES_EGL_image_external : require',
      'precision mediump float;',
      '',
      'varying vec2 vUV;',
      '',
      'uniform samplerExternalOES map;',
      '',
      'void main(void) {',
      '   gl_FragColor = texture2D(map, vUV);',
      '}'
    ];

    material = new THREE.RawShaderMaterial({
      uniforms: {
        map: {type: 't', value: videoTexture},
      },
      vertexShader: vertexShaderSource.join( '\r\n' ),
      fragmentShader: fragmentShaderSource.join( '\r\n' ),
      side: THREE.DoubleSide,
    });
  }
  else {
    material = new THREE.MeshBasicMaterial( 
      {color: 0xFFFFFF, side: THREE.DoubleSide, map: videoTexture } );
  }

  var mesh = new THREE.Mesh(geometry, material);

  return mesh;
};

/**
* Updates the camera mesh texture coordinates depending on the orientation of the current screen and the see through camera.
* @param {VRDisplay} vrDisplay The VRDisplay that holds the VRSeeThroughCamera. If could be null/undefined.
* @param {THREE.Mesh} cameraMesh The ThreeJS mesh that represents the camera quad that needs to be updated/rotated depending on the device and camera orientations. This instance should have been created by calling THREE.WebAR.createVRSeeThroughCameraMesh.
*/
THREE.WebAR.updateCameraMeshOrientation = function(vrDisplay, cameraMesh) {
  var textureCoordIndex = THREE.WebAR.getIndexFromScreenAndSeeThroughCameraOrientations(vrDisplay);
  if (textureCoordIndex != cameraMesh.geometry.WebAR_textureCoordIndex) {
    var uvs = cameraMesh.geometry.getAttribute("uv");
    var textureCoords = 
      cameraMesh.geometry.WebAR_textureCoords[textureCoordIndex];
    cameraMesh.geometry.WebAR_textureCoordIndex = textureCoordIndex;
    for (var i = 0; i < uvs.length; i++) {
      uvs.array[i] = textureCoords[i];
    }
    uvs.needsUpdate = true;
  }
};

/**
* A utility function to create a THREE.Camera instance with as frustum that is obtainer from the underlying vrdisplay see through camera information. This camera can be used to correctly render 3D objects on top of the underlying camera image.
* @param {VRDisplay} vrDisplay - The VRDisplay that is capable to provide a correct VRSeeThroughCamera instance in order to obtain the camera lens information and create the correct projection matrix/frustum. It could be null/undefined.
* @param {number} near The near plane value to be used to create the correct projection matrix frustum.
* @param {number} far The far plane value to be used to create the correct projection matrix frustum.
* @return {THREE.Camera} A camera instance to be used to correctly render a scene on top of the camera video feed.
*/
THREE.WebAR.createVRSeeThroughCamera = function(vrDisplay, near, far) {
  var camera = new THREE.PerspectiveCamera( 60, 
    window.innerWidth / window.innerHeight, near, far );
  if (vrDisplay) {
    THREE.WebAR.resizeVRSeeThroughCamera(vrDisplay, camera);
  }
  return camera;
};

/**
* Recalculate a camera projection matrix depending on the current device and see through camera orientation and specification.
* @param {VRDisplay} vrDisplay The VRDisplay that handles the see through camera.
* @param {THREE.Camera} camera The ThreeJS camera instance to update its projection matrix depending on the current device orientation and see through camera properties.
*/
THREE.WebAR.resizeVRSeeThroughCamera = function(vrDisplay, camera) {
  camera.aspect = window.innerWidth / window.innerHeight;
  if (vrDisplay) {
    var windowWidthBiggerThanHeight = window.innerWidth > window.innerHeight;
    var seeThroughCamera = vrDisplay.getSeeThroughCamera();
    if (seeThroughCamera) {
      var cameraWidthBiggerThanHeight = 
        seeThroughCamera.width > seeThroughCamera.height;
      var swapWidthAndHeight = 
        !(windowWidthBiggerThanHeight && cameraWidthBiggerThanHeight);

      var width = swapWidthAndHeight ? 
        seeThroughCamera.height : seeThroughCamera.width;
      var height = swapWidthAndHeight ? 
        seeThroughCamera.width : seeThroughCamera.height;
      var fx = swapWidthAndHeight ? 
        seeThroughCamera.focalLengthY : seeThroughCamera.focalLengthX;
      var fy = swapWidthAndHeight ? 
        seeThroughCamera.focalLengthX : seeThroughCamera.focalLengthY;
      var cx = swapWidthAndHeight ? 
        seeThroughCamera.pointY : seeThroughCamera.pointX;
      var cy = swapWidthAndHeight ? 
        seeThroughCamera.pointX : seeThroughCamera.pointY;

      var xscale = camera.near / fx;
      var yscale = camera.near / fy;

      var xoffset = (cx - (width / 2.0)) * xscale;
      // Color camera's coordinates has Y pointing downwards so we negate this term.
      var yoffset = -(cy - (height / 2.0)) * yscale;

      var left = xscale * -width / 2.0 - xoffset;
      var right = xscale * width / 2.0 - xoffset;
      var bottom = yscale * -height / 2.0 - yoffset;
      var top = yscale * height / 2.0 - yoffset;

      // camera.projectionMatrix.makeFrustum(
      //   left, right, bottom, top, camera.near, camera.far);

      camera.projectionMatrix.makePerspective(
        left, right, top, bottom, camera.near, camera.far);

      // Recalculate the fov as threejs is not doing it.
      camera.fov = THREE.Math.radToDeg(
        Math.atan((top * camera.zoom) / camera.near)) * 2.0;
    }
  }
  else {
    camera.updateProjectionMatrix();
  }
}

// Some precalculated private objects to avoid garbage collection
THREE.WebAR._worldUp = new THREE.Vector3(0.0, 1.0, 0.0);
THREE.WebAR._normalY = new THREE.Vector3();
THREE.WebAR._normalZ = new THREE.Vector3();
THREE.WebAR._rotationMatrix = new THREE.Matrix4();
THREE.WebAR._planeNormal = new THREE.Vector3();

THREE.WebAR.rotateObject3D = function(normal1, normal2, object3d) {
  if (normal1 instanceof THREE.Vector3 || normal1 instanceof THREE.Vector4) {
    THREE.WebAR._planeNormal.set(normal1.x, normal1.y, normal1.z);
  }
  else if (normal1 instanceof Float32Array) {
    THREE.WebAR._planeNormal.set(normal1[0], normal1[1], normal1[2]);
  }
  else {
    throw "Unknown normal1 type.";
  }
  if (normal2 instanceof THREE.Vector3 || normal2 instanceof THREE.Vector4) {
    THREE.WebAR._normalZ.set(normal2.x, normal2.y, normal2.z);
  }
  else if (normal1 instanceof Float32Array) {
    THREE.WebAR._normalZ.set(normal2[0], normal2[1], normal2[2]);
  }
  else {
    throw "Unknown normal2 type.";
  }
  THREE.WebAR._normalY.crossVectors(THREE.WebAR._planeNormal, 
    THREE.WebAR._normalZ).normalize();
  THREE.WebAR._rotationMatrix.elements[ 0] = THREE.WebAR._planeNormal.x;
  THREE.WebAR._rotationMatrix.elements[ 1] = THREE.WebAR._planeNormal.y;
  THREE.WebAR._rotationMatrix.elements[ 2] = THREE.WebAR._planeNormal.z;
  THREE.WebAR._rotationMatrix.elements[ 4] = THREE.WebAR._normalZ.x;
  THREE.WebAR._rotationMatrix.elements[ 5] = THREE.WebAR._normalZ.y;
  THREE.WebAR._rotationMatrix.elements[ 6] = THREE.WebAR._normalZ.z;
  THREE.WebAR._rotationMatrix.elements[ 8] = THREE.WebAR._normalY.x;
  THREE.WebAR._rotationMatrix.elements[ 9] = THREE.WebAR._normalY.y;
  THREE.WebAR._rotationMatrix.elements[10] = THREE.WebAR._normalY.z;
  object3d.quaternion.setFromRotationMatrix(THREE.WebAR._rotationMatrix);
};

/**
* Transform a given THREE.Object3D instance to be correctly oriented according to a given plane normal.
* @param {THREE.Vector3|THREE.Vector4|Float32Array} plane A vector that represents the normal of the plane to be used to orient the object3d.
* @param {THREE.Object3D} object3d The object3d to be transformed so it is oriented according to the given plane.
*/
THREE.WebAR.rotateObject3DWithPickingPlane = function(plane, object3d) {
  if (plane instanceof THREE.Vector3 || plane instanceof THREE.Vector4) {
    THREE.WebAR._planeNormal.set(plane.x, plane.y, plane.z);
  }
  else if (plane instanceof Float32Array) {
    THREE.WebAR._planeNormal.set(plane[0], plane[1], plane[2]);
  }
  else {
    throw "Unknown plane type.";
  }
  THREE.WebAR._normalY.set(0.0, 1.0, 0.0);
  var threshold = 0.5;
  if (Math.abs(THREE.WebAR._planeNormal.dot(THREE.WebAR._worldUp)) > 
    threshold) {
    THREE.WebAR._normalY.set(0.0, 0.0, 1.0);
  }
  THREE.WebAR._normalZ.crossVectors(THREE.WebAR._planeNormal, 
    THREE.WebAR._normalY).normalize();
  THREE.WebAR._normalY.crossVectors(THREE.WebAR._normalZ, 
    THREE.WebAR._planeNormal).normalize();
  THREE.WebAR._rotationMatrix.elements[ 0] = THREE.WebAR._planeNormal.x;
  THREE.WebAR._rotationMatrix.elements[ 1] = THREE.WebAR._planeNormal.y;
  THREE.WebAR._rotationMatrix.elements[ 2] = THREE.WebAR._planeNormal.z;
  THREE.WebAR._rotationMatrix.elements[ 4] = THREE.WebAR._normalY.x;
  THREE.WebAR._rotationMatrix.elements[ 5] = THREE.WebAR._normalY.y;
  THREE.WebAR._rotationMatrix.elements[ 6] = THREE.WebAR._normalY.z;
  THREE.WebAR._rotationMatrix.elements[ 8] = THREE.WebAR._normalZ.x;
  THREE.WebAR._rotationMatrix.elements[ 9] = THREE.WebAR._normalZ.y;
  THREE.WebAR._rotationMatrix.elements[10] = THREE.WebAR._normalZ.z;
  object3d.quaternion.setFromRotationMatrix(THREE.WebAR._rotationMatrix);
};

/**
* Transform a given THREE.Object3D instance to be correctly positioned according to a given point position.
* @param {THREE.Vector3|THREE.Vector4|Float32Array} point A vector that represents the position where the object3d should be positioned.
* @param {THREE.Object3D} object3d The object3d to be transformed so it is positioned according to the given point.
*/
THREE.WebAR.positionObject3DWithPickingPoint = function(point, object3d) {
  if (point instanceof THREE.Vector3 || point instanceof THREE.Vector4) {
    object3d.position.set(point.x, point.y, point.z);
  }
  else if (point instanceof Float32Array) {
    object3d.position.set(point[0], point[1], point[2]);
  }
  else {
    throw "Unknown point type.";
  }
};

/**
* Transform a given THREE.Object3D instance to be correctly positioned and oriented according to a given VRPickingPointAndPlane and a scale (half the size of the object3d for example).
* @param {VRPickingPointandPlane} pointAndPlane The point and plane retrieved using the VRDisplay.getPickingPointAndPlaneInPointCloud function.
* @param {THREE.Object3D} object3d The object3d to be transformed so it is positioned and oriented according to the given point and plane.
* @param {number} scale The value the object3d will be positioned in the direction of the normal of the plane to be correctly positioned. Objects usually have their position value referenced as the center of the geometry. In this case, positioning the object in the picking point would lead to have the object3d positioned in the plane, not on top of it. this scale value will allow to correctly position the object in the picking point and in the direction of the normal of the plane. Half the size of the object3d would be a correct value in this case.
*/
THREE.WebAR.positionAndRotateObject3DWithPickingPointAndPlaneInPointCloud = 
  function(pointAndPlane, object3d, scale) {
  THREE.WebAR.rotateObject3DWithPickingPlane(pointAndPlane.plane, object3d);
  THREE.WebAR.positionObject3DWithPickingPoint(pointAndPlane.point, object3d);
  object3d.position.add(THREE.WebAR._planeNormal.multiplyScalar(scale));
};

/**
* Transform a given THREE.Object3D instance to be correctly positioned and oriented according to an axis formed by 2 plane normals, a position and a scale (half the size of the object3d for example).
* @param {VRPickingPointandPlane} pointAndPlane The point and plane retrieved using the VRDisplay.getPickingPointAndPlaneInPointCloud function.
* @param {THREE.Object3D} object3d The object3d to be transformed so it is positioned and oriented according to the given point and plane.
* @param {number} scale The value the object3d will be positioned in the direction of the normal of the plane to be correctly positioned. Objects usually have their position value referenced as the center of the geometry. In this case, positioning the object in the picking point would lead to have the object3d positioned in the plane, not on top of it. this scale value will allow to correctly position the object in the picking point and in the direction of the normal of the plane. Half the size of the object3d would be a correct value in this case.
*/
THREE.WebAR.positionAndRotateObject3D = 
  function(position, normal1, normal2, object3d, scale) {
  THREE.WebAR.rotateObject3D(normal1, normal2, object3d);
  THREE.WebAR.positionObject3DWithPickingPoint(position, object3d);
  object3d.position.add(THREE.WebAR._planeNormal.multiplyScalar(scale));
};

// UMD
(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['WebAR'], factory);
  } else if (typeof exports === 'object') {
    module.exports = factory();
  } else {
    root.WebAR = factory();
  }
}(this, function() {
    return THREE.WebAR;
}));
var THREEx = THREEx || {}

THREEx.ArBaseControls = function(object3d){
	this.id = THREEx.ArBaseControls.id++

	this.object3d = object3d
	this.object3d.matrixAutoUpdate = false;
	this.object3d.visible = false

	// Events to honor
	// this.dispatchEvent({ type: 'becameVisible' })
	// this.dispatchEvent({ type: 'markerVisible' })	// replace markerFound
	// this.dispatchEvent({ type: 'becameUnVisible' })
}

THREEx.ArBaseControls.id = 0

Object.assign( THREEx.ArBaseControls.prototype, THREE.EventDispatcher.prototype );

//////////////////////////////////////////////////////////////////////////////
//		Functions
//////////////////////////////////////////////////////////////////////////////
/**
 * error catching function for update()
 */
THREEx.ArBaseControls.prototype.update = function(){
	console.assert(false, 'you need to implement your own update')
}

/**
 * error catching function for name()
 */
THREEx.ArBaseControls.prototype.name = function(){
	console.assert(false, 'you need to implement your own .name()')
	return 'Not yet implemented - name()'
}
var THREEx = THREEx || {}

// TODO this is useless - prefere arjs-HitTesting.js

/**
 * - maybe support .onClickFcts in each object3d
 * - seems an easy light layer for clickable object
 * - up to 
 */
THREEx.ARClickability = function(sourceElement){
	this._sourceElement = sourceElement
	// Create cameraPicking
	var fullWidth = parseInt(sourceElement.style.width)
	var fullHeight = parseInt(sourceElement.style.height)
	this._cameraPicking = new THREE.PerspectiveCamera(42, fullWidth / fullHeight, 0.1, 100);	

console.warn('THREEx.ARClickability works only in modelViewMatrix')
console.warn('OBSOLETE OBSOLETE! instead use THREEx.HitTestingPlane or THREEx.HitTestingTango')
}

THREEx.ARClickability.prototype.onResize = function(){
	var sourceElement = this._sourceElement
	var cameraPicking = this._cameraPicking
	
	var fullWidth = parseInt(sourceElement.style.width)
	var fullHeight = parseInt(sourceElement.style.height)
	cameraPicking.aspect = fullWidth / fullHeight;
	cameraPicking.updateProjectionMatrix();
}

THREEx.ARClickability.prototype.computeIntersects = function(domEvent, objects){
	var sourceElement = this._sourceElement
	var cameraPicking = this._cameraPicking

	// compute mouse coordinatge with [-1,1]
	var eventCoords = new THREE.Vector3();
	eventCoords.x =   ( domEvent.layerX / parseInt(sourceElement.style.width)  ) * 2 - 1;
	eventCoords.y = - ( domEvent.layerY / parseInt(sourceElement.style.height) ) * 2 + 1;

	// compute intersections between eventCoords and pickingPlane
	var raycaster = new THREE.Raycaster();
	raycaster.setFromCamera( eventCoords, cameraPicking );
	var intersects = raycaster.intersectObjects( objects )
	
	return intersects
}

THREEx.ARClickability.prototype.update = function(){

}

//////////////////////////////////////////////////////////////////////////////
//		Code Separator
//////////////////////////////////////////////////////////////////////////////

THREEx.ARClickability.tangoPickingPointCloud = function(artoolkitContext, mouseX, mouseY){
	
// THIS IS CRAP!!!! use THREEx.HitTestingTango
	
	var vrDisplay = artoolkitContext._tangoContext.vrDisplay
        if (vrDisplay === null ) return null
        var pointAndPlane = vrDisplay.getPickingPointAndPlaneInPointCloud(mouseX, mouseY)
        if( pointAndPlane == null ) {
                console.warn('Could not retrieve the correct point and plane.')
                return null
        }
	
	// FIXME not sure what this is
	var boundingSphereRadius = 0.01	
	
	// the bigger the number the likeliest it crash chromium-webar

        // Orient and position the model in the picking point according
        // to the picking plane. The offset is half of the model size.
        var object3d = new THREE.Object3D
        THREE.WebAR.positionAndRotateObject3DWithPickingPointAndPlaneInPointCloud(
                pointAndPlane, object3d, boundingSphereRadius
        )
	object3d.rotateZ(-Math.PI/2)

	// return the result
	var result = {}
	result.position = object3d.position
	result.quaternion = object3d.quaternion
	return result
}
var THREEx = THREEx || {}
/**
 * - videoTexture
 * - cloakWidth
 * - cloakHeight
 * - cloakSegmentsHeight
 * - remove all mentions of cache, for cloak
 */
THREEx.ArMarkerCloak = function(videoTexture){
        var updateInShaderEnabled = true

        // build cloakMesh
        // TODO if webgl2 use repeat warp, and not multi segment, this will reduce the geometry to draw
	var geometry = new THREE.PlaneGeometry(1.3+0.25,1.85+0.25, 1, 8).translate(0,-0.3,0)
	var material = new THREE.ShaderMaterial( {
		vertexShader: THREEx.ArMarkerCloak.vertexShader,
		fragmentShader: THREEx.ArMarkerCloak.fragmentShader,
                transparent: true,
		uniforms: {
			texture: {
				value: videoTexture
			},
                        opacity: {
                                value: 0.5
                        }
		},
		defines: {
			updateInShaderEnabled: updateInShaderEnabled ? 1 : 0,
		}
	});

	var cloakMesh = new THREE.Mesh( geometry, material );
        cloakMesh.rotation.x = -Math.PI/2
	this.object3d = cloakMesh

	//////////////////////////////////////////////////////////////////////////////
	//		Code Separator
	//////////////////////////////////////////////////////////////////////////////

	var xMin = -0.65
	var xMax =  0.65
	var yMin =  0.65 + 0.1
	var yMax =  0.95 + 0.1

	//////////////////////////////////////////////////////////////////////////////
	//		originalsFaceVertexUvs
	//////////////////////////////////////////////////////////////////////////////
        var originalsFaceVertexUvs = [[]]

        // build originalsFaceVertexUvs array
	for(var faceIndex = 0; faceIndex < cloakMesh.geometry.faces.length; faceIndex ++ ){
		originalsFaceVertexUvs[0][faceIndex] = []
		originalsFaceVertexUvs[0][faceIndex][0] = new THREE.Vector2()
		originalsFaceVertexUvs[0][faceIndex][1] = new THREE.Vector2()
		originalsFaceVertexUvs[0][faceIndex][2] = new THREE.Vector2()
        }

	// set values in originalsFaceVertexUvs
	for(var i = 0; i < cloakMesh.geometry.parameters.heightSegments/2; i ++ ){
		// one segment height - even row - normale orientation
		originalsFaceVertexUvs[0][i*4+0][0].set( xMin/2+0.5, yMax/2+0.5 )
		originalsFaceVertexUvs[0][i*4+0][1].set( xMin/2+0.5, yMin/2+0.5 )
		originalsFaceVertexUvs[0][i*4+0][2].set( xMax/2+0.5, yMax/2+0.5 )
		
		originalsFaceVertexUvs[0][i*4+1][0].set( xMin/2+0.5, yMin/2+0.5 )
		originalsFaceVertexUvs[0][i*4+1][1].set( xMax/2+0.5, yMin/2+0.5 )
		originalsFaceVertexUvs[0][i*4+1][2].set( xMax/2+0.5, yMax/2+0.5 )

		// one segment height - odd row - mirror-y orientation
		originalsFaceVertexUvs[0][i*4+2][0].set( xMin/2+0.5, yMin/2+0.5 )
		originalsFaceVertexUvs[0][i*4+2][1].set( xMin/2+0.5, yMax/2+0.5 )
		originalsFaceVertexUvs[0][i*4+2][2].set( xMax/2+0.5, yMin/2+0.5 )
		
		originalsFaceVertexUvs[0][i*4+3][0].set( xMin/2+0.5, yMax/2+0.5 )
		originalsFaceVertexUvs[0][i*4+3][1].set( xMax/2+0.5, yMax/2+0.5 )
		originalsFaceVertexUvs[0][i*4+3][2].set( xMax/2+0.5, yMin/2+0.5 )
	}

        if( updateInShaderEnabled === true ){
                cloakMesh.geometry.faceVertexUvs = originalsFaceVertexUvs
                cloakMesh.geometry.uvsNeedUpdate = true                
        }

	//////////////////////////////////////////////////////////////////////////////
	//		Code Separator
	//////////////////////////////////////////////////////////////////////////////

	var originalOrthoVertices = []
	originalOrthoVertices.push( new THREE.Vector3(xMin, yMax, 0))
	originalOrthoVertices.push( new THREE.Vector3(xMax, yMax, 0))
	originalOrthoVertices.push( new THREE.Vector3(xMin, yMin, 0))
	originalOrthoVertices.push( new THREE.Vector3(xMax, yMin, 0))

	// build debugMesh
        var material = new THREE.MeshNormalMaterial({
		transparent : true,
		opacity: 0.5,
		side: THREE.DoubleSide
	});
        var geometry = new THREE.PlaneGeometry(1,1);
        var orthoMesh = new THREE.Mesh(geometry, material);
	this.orthoMesh = orthoMesh

        //////////////////////////////////////////////////////////////////////////////
        //                Code Separator
        //////////////////////////////////////////////////////////////////////////////

	this.update = function(modelViewMatrix, cameraProjectionMatrix){
                updateOrtho(modelViewMatrix, cameraProjectionMatrix)

                if( updateInShaderEnabled === false ){
                        updateUvs(modelViewMatrix, cameraProjectionMatrix)
                }
	}
        
        return

        // update cloakMesh
	function updateUvs(modelViewMatrix, cameraProjectionMatrix){
		var transformedUv = new THREE.Vector3()
                originalsFaceVertexUvs[0].forEach(function(faceVertexUvs, faceIndex){
                        faceVertexUvs.forEach(function(originalUv, uvIndex){
                                // set transformedUv - from UV coord to clip coord
                                transformedUv.x = originalUv.x * 2.0 - 1.0;
                                transformedUv.y = originalUv.y * 2.0 - 1.0;
                                transformedUv.z = 0
        			// apply modelViewMatrix and projectionMatrix
        			transformedUv.applyMatrix4( modelViewMatrix )
        			transformedUv.applyMatrix4( cameraProjectionMatrix )
        			// apply perspective
        			transformedUv.x /= transformedUv.z
        			transformedUv.y /= transformedUv.z
                                // set back from clip coord to Uv coord
                                transformedUv.x = transformedUv.x / 2.0 + 0.5;
                                transformedUv.y = transformedUv.y / 2.0 + 0.5;
                                // copy the trasnformedUv into the geometry
                                cloakMesh.geometry.faceVertexUvs[0][faceIndex][uvIndex].set(transformedUv.x, transformedUv.y)
                        })
                })
        
                // cloakMesh.geometry.faceVertexUvs = faceVertexUvs
                cloakMesh.geometry.uvsNeedUpdate = true
        }

        // update orthoMesh
	function updateOrtho(modelViewMatrix, cameraProjectionMatrix){
		// compute transformedUvs
		var transformedUvs = []
		originalOrthoVertices.forEach(function(originalOrthoVertices, index){
			var transformedUv = originalOrthoVertices.clone()
			// apply modelViewMatrix and projectionMatrix
			transformedUv.applyMatrix4( modelViewMatrix )
			transformedUv.applyMatrix4( cameraProjectionMatrix )
			// apply perspective
			transformedUv.x /= transformedUv.z
			transformedUv.y /= transformedUv.z
			// store it
			transformedUvs.push(transformedUv)
		})

		// change orthoMesh vertices
		for(var i = 0; i < transformedUvs.length; i++){
			orthoMesh.geometry.vertices[i].copy(transformedUvs[i])
		}
		orthoMesh.geometry.computeBoundingSphere()
		orthoMesh.geometry.verticesNeedUpdate = true
        }

}

//////////////////////////////////////////////////////////////////////////////
//                Shaders
//////////////////////////////////////////////////////////////////////////////

THREEx.ArMarkerCloak.markerSpaceShaderFunction = '\n'+
'        vec2 transformUvToMarkerSpace(vec2 originalUv){\n'+
'                vec3 transformedUv;\n'+
'                // set transformedUv - from UV coord to clip coord\n'+
'                transformedUv.x = originalUv.x * 2.0 - 1.0;\n'+
'                transformedUv.y = originalUv.y * 2.0 - 1.0;\n'+
'                transformedUv.z = 0.0;\n'+
'\n'+
'		// apply modelViewMatrix and projectionMatrix\n'+
'                transformedUv = (projectionMatrix * modelViewMatrix * vec4( transformedUv, 1.0 ) ).xyz;\n'+
'\n'+
'		// apply perspective\n'+
'		transformedUv.x /= transformedUv.z;\n'+
'		transformedUv.y /= transformedUv.z;\n'+
'\n'+
'                // set back from clip coord to Uv coord\n'+
'                transformedUv.x = transformedUv.x / 2.0 + 0.5;\n'+
'                transformedUv.y = transformedUv.y / 2.0 + 0.5;\n'+
'\n'+
'                // return the result\n'+
'                return transformedUv.xy;\n'+
'        }'

THREEx.ArMarkerCloak.vertexShader = THREEx.ArMarkerCloak.markerSpaceShaderFunction +
'	varying vec2 vUv;\n'+
'\n'+
'	void main(){\n'+
'                // pass the UV to the fragment\n'+
'                #if (updateInShaderEnabled == 1)\n'+
'		        vUv = transformUvToMarkerSpace(uv);\n'+
'                #else\n'+
'		        vUv = uv;\n'+
'                #endif\n'+
'\n'+
'                // compute gl_Position\n'+
'		vec4 mvPosition = modelViewMatrix * vec4( position, 1.0 );\n'+
'		gl_Position = projectionMatrix * mvPosition;\n'+
'	}';

THREEx.ArMarkerCloak.fragmentShader = '\n'+
'	varying vec2 vUv;\n'+
'	uniform sampler2D texture;\n'+
'	uniform float opacity;\n'+
'\n'+
'	void main(void){\n'+
'		vec3 color = texture2D( texture, vUv ).rgb;\n'+
'\n'+
'		gl_FragColor = vec4( color, opacity);\n'+
'	}'
var THREEx = THREEx || {}

THREEx.ArMarkerControls = function(context, object3d, parameters){
	var _this = this

	THREEx.ArBaseControls.call(this, object3d)

	this.context = context
	// handle default parameters
	this.parameters = {
		// size of the marker in meter
		size : 1,
		// type of marker - ['pattern', 'barcode', 'unknown' ]
		type : 'unknown',
		// url of the pattern - IIF type='pattern'
		patternUrl : null,
		// value of the barcode - IIF type='barcode'
		barcodeValue : null,
		// change matrix mode - [modelViewMatrix, cameraTransformMatrix]
		changeMatrixMode : 'modelViewMatrix',
		// minimal confidence in the marke recognition - between [0, 1] - default to 1
		minConfidence: 0.6,
	}

	// sanity check
	var possibleValues = ['pattern', 'barcode', 'unknown']
	console.assert(possibleValues.indexOf(this.parameters.type) !== -1, 'illegal value', this.parameters.type)
	var possibleValues = ['modelViewMatrix', 'cameraTransformMatrix' ]
	console.assert(possibleValues.indexOf(this.parameters.changeMatrixMode) !== -1, 'illegal value', this.parameters.changeMatrixMode)


        // create the marker Root
	this.object3d = object3d
	this.object3d.matrixAutoUpdate = false;
	this.object3d.visible = false
	
	//////////////////////////////////////////////////////////////////////////////
	//		setParameters
	//////////////////////////////////////////////////////////////////////////////
	setParameters(parameters)
	function setParameters(parameters){
		if( parameters === undefined )	return
		for( var key in parameters ){
			var newValue = parameters[ key ]

			if( newValue === undefined ){
				console.warn( "THREEx.ArMarkerControls: '" + key + "' parameter is undefined." )
				continue
			}

			var currentValue = _this.parameters[ key ]

			if( currentValue === undefined ){
				console.warn( "THREEx.ArMarkerControls: '" + key + "' is not a property of this material." )
				continue
			}

			_this.parameters[ key ] = newValue
		}
	}

	//////////////////////////////////////////////////////////////////////////////
	//		Code Separator
	//////////////////////////////////////////////////////////////////////////////
	// add this marker to artoolkitsystem
	// TODO rename that .addMarkerControls
	context.addMarker(this)

	if( _this.context.parameters.trackingBackend === 'artoolkit' ){
		this._initArtoolkit()
	}else if( _this.context.parameters.trackingBackend === 'aruco' ){
		// TODO create a ._initAruco
		// put aruco second
		this._arucoPosit = new POS.Posit(this.parameters.size, _this.context.arucoContext.canvas.width)
	}else if( _this.context.parameters.trackingBackend === 'tango' ){
		this._initTango()
	}else console.assert(false)
}

THREEx.ArMarkerControls.prototype = Object.create( THREEx.ArBaseControls.prototype );
THREEx.ArMarkerControls.prototype.constructor = THREEx.ArMarkerControls;

THREEx.ArMarkerControls.prototype.dispose = function(){
	this.context.removeMarker(this)

	// TODO remove the event listener if needed
	// unloadMaker ???
}

//////////////////////////////////////////////////////////////////////////////
//		update controls with new modelViewMatrix
//////////////////////////////////////////////////////////////////////////////

/**
 * When you actually got a new modelViewMatrix, you need to perfom a whole bunch 
 * of things. it is done here.
 */
THREEx.ArMarkerControls.prototype.updateWithModelViewMatrix = function(modelViewMatrix){
	var markerObject3D = this.object3d;

	// mark object as visible
	markerObject3D.visible = true

	if( this.context.parameters.trackingBackend === 'artoolkit' ){
		// apply context._axisTransformMatrix - change artoolkit axis to match usual webgl one
		var tmpMatrix = new THREE.Matrix4().copy(this.context._artoolkitProjectionAxisTransformMatrix)
		tmpMatrix.multiply(modelViewMatrix)
		
		modelViewMatrix.copy(tmpMatrix)		
	}else if( this.context.parameters.trackingBackend === 'aruco' ){
		// ...
	}else if( this.context.parameters.trackingBackend === 'tango' ){
		// ...
	}else console.assert(false)


	if( this.context.parameters.trackingBackend !== 'tango' ){

		// change axis orientation on marker - artoolkit say Z is normal to the marker - ar.js say Y is normal to the marker
		var markerAxisTransformMatrix = new THREE.Matrix4().makeRotationX(Math.PI/2)
		modelViewMatrix.multiply(markerAxisTransformMatrix)
	}

	// change markerObject3D.matrix based on parameters.changeMatrixMode
	if( this.parameters.changeMatrixMode === 'modelViewMatrix' ){
		markerObject3D.matrix.copy(modelViewMatrix)
	}else if( this.parameters.changeMatrixMode === 'cameraTransformMatrix' ){
		markerObject3D.matrix.getInverse( modelViewMatrix )
	}else {
		console.assert(false)
	}

	// decompose - the matrix into .position, .quaternion, .scale
	markerObject3D.matrix.decompose(markerObject3D.position, markerObject3D.quaternion, markerObject3D.scale)

	// dispatchEvent
	this.dispatchEvent( { type: 'markerFound' } );
}

//////////////////////////////////////////////////////////////////////////////
//		utility functions
//////////////////////////////////////////////////////////////////////////////

/**
 * provide a name for a marker 
 * - silly heuristic for now
 * - should be improved
 */
THREEx.ArMarkerControls.prototype.name = function(){
	var name = ''
	name += this.parameters.type;
	if( this.parameters.type === 'pattern' ){
		var url = this.parameters.patternUrl
		var basename = url.replace(/^.*\//g, '')
		name += ' - ' + basename
	}else if( this.parameters.type === 'barcode' ){
		name += ' - ' + this.parameters.barcodeValue
	}else{
		console.assert(false, 'no .name() implemented for this marker controls')
	}
	return name
}

//////////////////////////////////////////////////////////////////////////////
//		init for Artoolkit
//////////////////////////////////////////////////////////////////////////////
THREEx.ArMarkerControls.prototype._initArtoolkit = function(){
	var _this = this

	var artoolkitMarkerId = null

	var delayedInitTimerId = setInterval(function(){
		// check if arController is init
		var arController = _this.context.arController
		if( arController === null )	return
		// stop looping if it is init
		clearInterval(delayedInitTimerId)
		delayedInitTimerId = null
		// launch the _postInitArtoolkit
		postInit()
	}, 1000/50)

	return
	
	function postInit(){
		// check if arController is init
		var arController = _this.context.arController
		console.assert(arController !== null )

		// start tracking this pattern
		if( _this.parameters.type === 'pattern' ){
	                arController.loadMarker(_this.parameters.patternUrl, function(markerId) {
				artoolkitMarkerId = markerId
	                        arController.trackPatternMarkerId(artoolkitMarkerId, _this.parameters.size);
	                });
		}else if( _this.parameters.type === 'barcode' ){
			artoolkitMarkerId = _this.parameters.barcodeValue
			arController.trackBarcodeMarkerId(artoolkitMarkerId, _this.parameters.size);
		}else if( _this.parameters.type === 'unknown' ){
			artoolkitMarkerId = null
		}else{
			console.log(false, 'invalid marker type', _this.parameters.type)
		}

		// listen to the event
		arController.addEventListener('getMarker', function(event){
			if( event.data.type === artoolkit.PATTERN_MARKER && _this.parameters.type === 'pattern' ){
				if( artoolkitMarkerId === null )	return
				if( event.data.marker.idPatt === artoolkitMarkerId ) onMarkerFound(event)
			}else if( event.data.type === artoolkit.BARCODE_MARKER && _this.parameters.type === 'barcode' ){
				// console.log('BARCODE_MARKER idMatrix', event.data.marker.idMatrix, artoolkitMarkerId )
				if( artoolkitMarkerId === null )	return
				if( event.data.marker.idMatrix === artoolkitMarkerId )  onMarkerFound(event)
			}else if( event.data.type === artoolkit.UNKNOWN_MARKER && _this.parameters.type === 'unknown'){
				onMarkerFound(event)
			}
		})
		
	}

	function onMarkerFound(event){
		// honor his.parameters.minConfidence
		if( event.data.type === artoolkit.PATTERN_MARKER && event.data.marker.cfPatt < _this.parameters.minConfidence )	return
		if( event.data.type === artoolkit.BARCODE_MARKER && event.data.marker.cfMatt < _this.parameters.minConfidence )	return

		var modelViewMatrix = new THREE.Matrix4().fromArray(event.data.matrix)
		_this.updateWithModelViewMatrix(modelViewMatrix)
	}
}

//////////////////////////////////////////////////////////////////////////////
//		aruco specific
//////////////////////////////////////////////////////////////////////////////
THREEx.ArMarkerControls.prototype._initAruco = function(){
	this._arucoPosit = new POS.Posit(this.parameters.size, _this.context.arucoContext.canvas.width)
}

//////////////////////////////////////////////////////////////////////////////
//		init for Artoolkit
//////////////////////////////////////////////////////////////////////////////
THREEx.ArMarkerControls.prototype._initTango = function(){
	var _this = this
	console.log('init tango ArMarkerControls')
}
var THREEx = THREEx || {}

THREEx.ArMarkerHelper = function(markerControls){
	this.object3d = new THREE.Group

	var mesh = new THREE.AxisHelper()
	this.object3d.add(mesh)

	var text = markerControls.id
	// debugger
	// var text = markerControls.parameters.patternUrl.slice(-1).toUpperCase();

	var canvas = document.createElement( 'canvas' );
	canvas.width =  64;
	canvas.height = 64;

	var context = canvas.getContext( '2d' );
	var texture = new THREE.CanvasTexture( canvas );

	// put the text in the sprite
	context.font = '48px monospace';
	context.fillStyle = 'rgba(192,192,255, 0.5)';
	context.fillRect( 0, 0, canvas.width, canvas.height );
	context.fillStyle = 'darkblue';
	context.fillText(text, canvas.width/4, 3*canvas.height/4 )
	texture.needsUpdate = true

	// var geometry = new THREE.CubeGeometry(1, 1, 1)
	var geometry = new THREE.PlaneGeometry(1, 1)
	var material = new THREE.MeshBasicMaterial({
		map: texture, 
		transparent: true
	});
	var mesh = new THREE.Mesh(geometry, material)
	mesh.rotation.x = -Math.PI/2

	this.object3d.add(mesh)
	
}
var THREEx = THREEx || {}

/**
 * - lerp position/quaternino/scale
 * - minDelayDetected
 * - minDelayUndetected
 * @param {[type]} object3d   [description]
 * @param {[type]} parameters [description]
 */
THREEx.ArSmoothedControls = function(object3d, parameters){
	var _this = this
	
	THREEx.ArBaseControls.call(this, object3d)
	
	// copy parameters
	this.object3d.visible = false
	
	this._lastLerpStepAt = null
	this._visibleStartedAt = null
	this._unvisibleStartedAt = null

	// handle default parameters
	parameters = parameters || {}
	this.parameters = {
		// lerp coeficient for the position - between [0,1] - default to 1
		lerpPosition: 0.8,
		// lerp coeficient for the quaternion - between [0,1] - default to 1
		lerpQuaternion: 0.2,
		// lerp coeficient for the scale - between [0,1] - default to 1
		lerpScale: 0.7,
		// delay for lerp fixed steps - in seconds - default to 1/120
		lerpStepDelay: 1/60,
		// minimum delay the sub-control must be visible before this controls become visible - default to 0 seconds
		minVisibleDelay: 0.0,
		// minimum delay the sub-control must be unvisible before this controls become unvisible - default to 0 seconds
		minUnvisibleDelay: 0.2,
	}
	
	//////////////////////////////////////////////////////////////////////////////
	//		setParameters
	//////////////////////////////////////////////////////////////////////////////
	setParameters(parameters)
	function setParameters(parameters){
		if( parameters === undefined )	return
		for( var key in parameters ){
			var newValue = parameters[ key ]

			if( newValue === undefined ){
				console.warn( "THREEx.ArSmoothedControls: '" + key + "' parameter is undefined." )
				continue
			}

			var currentValue = _this.parameters[ key ]

			if( currentValue === undefined ){
				console.warn( "THREEx.ArSmoothedControls: '" + key + "' is not a property of this material." )
				continue
			}

			_this.parameters[ key ] = newValue
		}
	}
}
	
THREEx.ArSmoothedControls.prototype = Object.create( THREEx.ArBaseControls.prototype );
THREEx.ArSmoothedControls.prototype.constructor = THREEx.ArSmoothedControls;

//////////////////////////////////////////////////////////////////////////////
//		update function
//////////////////////////////////////////////////////////////////////////////

THREEx.ArSmoothedControls.prototype.update = function(targetObject3d){
	var object3d = this.object3d
	var parameters = this.parameters
	var wasVisible = object3d.visible
	var present = performance.now()/1000


	//////////////////////////////////////////////////////////////////////////////
	//		handle object3d.visible with minVisibleDelay/minUnvisibleDelay
	//////////////////////////////////////////////////////////////////////////////
	if( targetObject3d.visible === false )	this._visibleStartedAt = null
	if( targetObject3d.visible === true )	this._unvisibleStartedAt = null

	if( targetObject3d.visible === true && this._visibleStartedAt === null )	this._visibleStartedAt = present
	if( targetObject3d.visible === false && this._unvisibleStartedAt === null )	this._unvisibleStartedAt = present

	if( wasVisible === false && targetObject3d.visible === true ){
		var visibleFor = present - this._visibleStartedAt
		if( visibleFor >= this.parameters.minVisibleDelay ){
			object3d.visible = true
			snapDirectlyToTarget()
		}
		// console.log('visibleFor', visibleFor)
	}

	if( wasVisible === true && targetObject3d.visible === false ){
		var unvisibleFor = present - this._unvisibleStartedAt
		if( unvisibleFor >= this.parameters.minUnvisibleDelay ){
			object3d.visible = false			
		}
	}
	
	//////////////////////////////////////////////////////////////////////////////
	//		apply lerp on positon/quaternion/scale
	//////////////////////////////////////////////////////////////////////////////

	// apply lerp steps - require fix time steps to behave the same no matter the fps
	if( this._lastLerpStepAt === null ){
		applyOneSlerpStep()
		this._lastLerpStepAt = present
	}else{
		var nStepsToDo = Math.floor( (present - this._lastLerpStepAt)/this.parameters.lerpStepDelay )
		for(var i = 0; i < nStepsToDo; i++){
			applyOneSlerpStep()
			this._lastLerpStepAt += this.parameters.lerpStepDelay
		}
	}

	// disable the lerp by directly copying targetObject3d position/quaternion/scale
	if( false ){		
		snapDirectlyToTarget()
	}

	// update the matrix
	this.object3d.updateMatrix()

	//////////////////////////////////////////////////////////////////////////////
	//		honor becameVisible/becameUnVisible event
	//////////////////////////////////////////////////////////////////////////////
	// honor becameVisible event
	if( wasVisible === false && object3d.visible === true ){
		this.dispatchEvent({ type: 'becameVisible' })
	}
	// honor becameUnVisible event
	if( wasVisible === true && object3d.visible === false ){
		this.dispatchEvent({ type: 'becameUnVisible' })
	}
	return

	function snapDirectlyToTarget(){
		object3d.position.copy( targetObject3d.position )
		object3d.quaternion.copy( targetObject3d.quaternion )
		object3d.scale.copy( targetObject3d.scale )
	}	
	
	function applyOneSlerpStep(){
		object3d.position.lerp(targetObject3d.position, parameters.lerpPosition)
		object3d.quaternion.slerp(targetObject3d.quaternion, parameters.lerpQuaternion)
		object3d.scale.lerp(targetObject3d.scale, parameters.lerpScale)
	}
}
var ARjs = ARjs || {}
var THREEx = THREEx || {}

ARjs.Context = THREEx.ArToolkitContext = function(parameters){
	var _this = this
	
	_this._updatedAt = null
	
	// handle default parameters
	this.parameters = {
		// AR backend - ['artoolkit', 'aruco', 'tango']
		trackingBackend: 'artoolkit',
		// debug - true if one should display artoolkit debug canvas, false otherwise
		debug: false,
		// the mode of detection - ['color', 'color_and_matrix', 'mono', 'mono_and_matrix']
		detectionMode: 'mono',
		// type of matrix code - valid iif detectionMode end with 'matrix' - [3x3, 3x3_HAMMING63, 3x3_PARITY65, 4x4, 4x4_BCH_13_9_3, 4x4_BCH_13_5_5]
		matrixCodeType: '3x3',
		
		// url of the camera parameters
		cameraParametersUrl: ARjs.Context.baseURL + 'parameters/camera_para.dat',

		// tune the maximum rate of pose detection in the source image
		maxDetectionRate: 60,
		// resolution of at which we detect pose in the source image
		canvasWidth: 640,
		canvasHeight: 480,
		
		// enable image smoothing or not for canvas copy - default to true
		// https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/imageSmoothingEnabled
		imageSmoothingEnabled : false,
	}
	// parameters sanity check
	console.assert(['artoolkit', 'aruco', 'tango'].indexOf(this.parameters.trackingBackend) !== -1, 'invalid parameter trackingBackend', this.parameters.trackingBackend)
	console.assert(['color', 'color_and_matrix', 'mono', 'mono_and_matrix'].indexOf(this.parameters.detectionMode) !== -1, 'invalid parameter detectionMode', this.parameters.detectionMode)
	
        this.arController = null;
        this.arucoContext = null;
	
	_this.initialized = false


	this._arMarkersControls = []
	
	//////////////////////////////////////////////////////////////////////////////
	//		setParameters
	//////////////////////////////////////////////////////////////////////////////
	setParameters(parameters)
	function setParameters(parameters){
		if( parameters === undefined )	return
		for( var key in parameters ){
			var newValue = parameters[ key ]

			if( newValue === undefined ){
				console.warn( "THREEx.ArToolkitContext: '" + key + "' parameter is undefined." )
				continue
			}

			var currentValue = _this.parameters[ key ]

			if( currentValue === undefined ){
				console.warn( "THREEx.ArToolkitContext: '" + key + "' is not a property of this material." )
				continue
			}

			_this.parameters[ key ] = newValue
		}
	}
}

Object.assign( ARjs.Context.prototype, THREE.EventDispatcher.prototype );

// ARjs.Context.baseURL = '../'
// default to github page
ARjs.Context.baseURL = 'https://jeromeetienne.github.io/AR.js/three.js/'
ARjs.Context.REVISION = '1.5.0'



/**
 * Create a default camera for this trackingBackend
 * @param {string} trackingBackend - the tracking to user
 * @return {THREE.Camera} the created camera
 */
ARjs.Context.createDefaultCamera = function( trackingBackend ){
	console.assert(false, 'use ARjs.Utils.createDefaultCamera instead')
	// Create a camera
	if( trackingBackend === 'artoolkit' ){
		var camera = new THREE.Camera();
	}else if( trackingBackend === 'aruco' ){
		var camera = new THREE.PerspectiveCamera(42, renderer.domElement.width / renderer.domElement.height, 0.01, 100);
	}else if( trackingBackend === 'tango' ){
		var camera = new THREE.PerspectiveCamera(42, renderer.domElement.width / renderer.domElement.height, 0.01, 100);
	}else console.assert(false)
	return camera
}


//////////////////////////////////////////////////////////////////////////////
//		init functions
//////////////////////////////////////////////////////////////////////////////
ARjs.Context.prototype.init = function(onCompleted){
	var _this = this
	if( this.parameters.trackingBackend === 'artoolkit' ){
		this._initArtoolkit(done)
	}else if( this.parameters.trackingBackend === 'aruco' ){
		this._initAruco(done)
	}else if( this.parameters.trackingBackend === 'tango' ){
		this._initTango(done)
	}else console.assert(false)
	return
	
	function done(){
		// dispatch event
		_this.dispatchEvent({
			type: 'initialized'
		});

		_this.initialized = true
		
		onCompleted && onCompleted()
	}

}
////////////////////////////////////////////////////////////////////////////////
//          update function
////////////////////////////////////////////////////////////////////////////////
ARjs.Context.prototype.update = function(srcElement){

	// be sure arController is fully initialized
        if(this.parameters.trackingBackend === 'artoolkit' && this.arController === null) return false;

	// honor this.parameters.maxDetectionRate
	var present = performance.now()
	if( this._updatedAt !== null && present - this._updatedAt < 1000/this.parameters.maxDetectionRate ){
		return false
	}
	this._updatedAt = present

	// mark all markers to invisible before processing this frame
	this._arMarkersControls.forEach(function(markerControls){
		markerControls.object3d.visible = false
	})

	// process this frame
	if(this.parameters.trackingBackend === 'artoolkit'){
		this._updateArtoolkit(srcElement)		
	}else if( this.parameters.trackingBackend === 'aruco' ){
		this._updateAruco(srcElement)
	}else if( this.parameters.trackingBackend === 'tango' ){
		this._updateTango(srcElement)
	}else{
		console.assert(false)
	}

	// dispatch event
	this.dispatchEvent({
		type: 'sourceProcessed'
	});


	// return true as we processed the frame
	return true;
}

////////////////////////////////////////////////////////////////////////////////
//          Add/Remove markerControls
////////////////////////////////////////////////////////////////////////////////
ARjs.Context.prototype.addMarker = function(arMarkerControls){
	console.assert(arMarkerControls instanceof THREEx.ArMarkerControls)
	this._arMarkersControls.push(arMarkerControls)
}

ARjs.Context.prototype.removeMarker = function(arMarkerControls){
	console.assert(arMarkerControls instanceof THREEx.ArMarkerControls)
	// console.log('remove marker for', arMarkerControls)
	var index = this.arMarkerControlss.indexOf(artoolkitMarker);
	console.assert(index !== index )
	this._arMarkersControls.splice(index, 1)
}

//////////////////////////////////////////////////////////////////////////////
//		artoolkit specific
//////////////////////////////////////////////////////////////////////////////
ARjs.Context.prototype._initArtoolkit = function(onCompleted){
        var _this = this

	// set this._artoolkitProjectionAxisTransformMatrix to change artoolkit projection matrix axis to match usual webgl one
	this._artoolkitProjectionAxisTransformMatrix = new THREE.Matrix4()
	this._artoolkitProjectionAxisTransformMatrix.multiply(new THREE.Matrix4().makeRotationY(Math.PI))
	this._artoolkitProjectionAxisTransformMatrix.multiply(new THREE.Matrix4().makeRotationZ(Math.PI))

	// get cameraParameters
        var cameraParameters = new ARCameraParam(_this.parameters.cameraParametersUrl, function(){
        	// init controller
                var arController = new ARController(_this.parameters.canvasWidth, _this.parameters.canvasHeight, cameraParameters);
                _this.arController = arController
                
		// honor this.parameters.imageSmoothingEnabled
		arController.ctx.mozImageSmoothingEnabled = _this.parameters.imageSmoothingEnabled;
		arController.ctx.webkitImageSmoothingEnabled = _this.parameters.imageSmoothingEnabled;
		arController.ctx.msImageSmoothingEnabled = _this.parameters.imageSmoothingEnabled;
		arController.ctx.imageSmoothingEnabled = _this.parameters.imageSmoothingEnabled;			
 		
		// honor this.parameters.debug
                if( _this.parameters.debug === true ){
			arController.debugSetup();
			arController.canvas.style.position = 'absolute'
			arController.canvas.style.top = '0px'
			arController.canvas.style.opacity = '0.6'
			arController.canvas.style.pointerEvents = 'none'
			arController.canvas.style.zIndex = '-1'
		}

		// setPatternDetectionMode
		var detectionModes = {
			'color'			: artoolkit.AR_TEMPLATE_MATCHING_COLOR,
			'color_and_matrix'	: artoolkit.AR_TEMPLATE_MATCHING_COLOR_AND_MATRIX,
			'mono'			: artoolkit.AR_TEMPLATE_MATCHING_MONO,
			'mono_and_matrix'	: artoolkit.AR_TEMPLATE_MATCHING_MONO_AND_MATRIX,
		}
		var detectionMode = detectionModes[_this.parameters.detectionMode]
		console.assert(detectionMode !== undefined)
		arController.setPatternDetectionMode(detectionMode);

		// setMatrixCodeType
		var matrixCodeTypes = {
			'3x3'		: artoolkit.AR_MATRIX_CODE_3x3,
			'3x3_HAMMING63'	: artoolkit.AR_MATRIX_CODE_3x3_HAMMING63,
			'3x3_PARITY65'	: artoolkit.AR_MATRIX_CODE_3x3_PARITY65,
			'4x4'		: artoolkit.AR_MATRIX_CODE_4x4,
			'4x4_BCH_13_9_3': artoolkit.AR_MATRIX_CODE_4x4_BCH_13_9_3,
			'4x4_BCH_13_5_5': artoolkit.AR_MATRIX_CODE_4x4_BCH_13_5_5,
		}
		var matrixCodeType = matrixCodeTypes[_this.parameters.matrixCodeType]
		console.assert(matrixCodeType !== undefined)
		arController.setMatrixCodeType(matrixCodeType);
		

		// set thresholding in artoolkit
		// this seems to be the default
		// arController.setThresholdMode(artoolkit.AR_LABELING_THRESH_MODE_MANUAL)
		// adatative consume a LOT of cpu...
		// arController.setThresholdMode(artoolkit.AR_LABELING_THRESH_MODE_AUTO_ADAPTIVE)
		// arController.setThresholdMode(artoolkit.AR_LABELING_THRESH_MODE_AUTO_OTSU)

		// notify
                onCompleted()                
        })		
	return this
}

/**
 * return the projection matrix
 */
ARjs.Context.prototype.getProjectionMatrix = function(srcElement){
	
	
// FIXME rename this function to say it is artoolkit specific - getArtoolkitProjectMatrix
// keep a backward compatibility with a console.warn
	
	
	if( this.parameters.trackingBackend === 'aruco' ){
		console.assert(false, 'dont call this function with aruco')
	}else if( this.parameters.trackingBackend === 'artoolkit' ){
		console.assert(this.arController, 'arController MUST be initialized to call this function')
		// get projectionMatrixArr from artoolkit
		var projectionMatrixArr = this.arController.getCameraMatrix();
		var projectionMatrix = new THREE.Matrix4().fromArray(projectionMatrixArr)		
	}else console.assert(false)
		
	// apply context._axisTransformMatrix - change artoolkit axis to match usual webgl one
	projectionMatrix.multiply(this._artoolkitProjectionAxisTransformMatrix)
	
	// return the result
	return projectionMatrix
}

ARjs.Context.prototype._updateArtoolkit = function(srcElement){
	this.arController.process(srcElement)
}

//////////////////////////////////////////////////////////////////////////////
//		aruco specific 
//////////////////////////////////////////////////////////////////////////////
ARjs.Context.prototype._initAruco = function(onCompleted){
	this.arucoContext = new THREEx.ArucoContext()
	
	// honor this.parameters.canvasWidth/.canvasHeight
	this.arucoContext.canvas.width = this.parameters.canvasWidth
	this.arucoContext.canvas.height = this.parameters.canvasHeight

	// honor this.parameters.imageSmoothingEnabled
	var context = this.arucoContext.canvas.getContext('2d')
	// context.mozImageSmoothingEnabled = this.parameters.imageSmoothingEnabled;
	context.webkitImageSmoothingEnabled = this.parameters.imageSmoothingEnabled;
	context.msImageSmoothingEnabled = this.parameters.imageSmoothingEnabled;
	context.imageSmoothingEnabled = this.parameters.imageSmoothingEnabled;			

	
	setTimeout(function(){
		onCompleted()
	}, 0)
}


ARjs.Context.prototype._updateAruco = function(srcElement){
	// console.log('update aruco here')
	var _this = this
	var arMarkersControls = this._arMarkersControls
        var detectedMarkers = this.arucoContext.detect(srcElement)
	
	detectedMarkers.forEach(function(detectedMarker){
		var foundControls = null
		for(var i = 0; i < arMarkersControls.length; i++){
			console.assert( arMarkersControls[i].parameters.type === 'barcode' )
			if( arMarkersControls[i].parameters.barcodeValue === detectedMarker.id ){
				foundControls = arMarkersControls[i]
				break;
			}
		}
		if( foundControls === null )	return

		var tmpObject3d = new THREE.Object3D
                _this.arucoContext.updateObject3D(tmpObject3d, foundControls._arucoPosit, foundControls.parameters.size, detectedMarker);
		tmpObject3d.updateMatrix()

		foundControls.updateWithModelViewMatrix(tmpObject3d.matrix)
	})
}

//////////////////////////////////////////////////////////////////////////////
//		tango specific 
//////////////////////////////////////////////////////////////////////////////
ARjs.Context.prototype._initTango = function(onCompleted){
	var _this = this
	// check webvr is available
	if (navigator.getVRDisplays){
		// do nothing
	} else if (navigator.getVRDevices){
		alert("Your browser supports WebVR but not the latest version. See <a href='http://webvr.info'>webvr.info</a> for more info.");
	} else {
		alert("Your browser does not support WebVR. See <a href='http://webvr.info'>webvr.info</a> for assistance.");
	}


	this._tangoContext = {
		vrDisplay: null,
		vrPointCloud: null,
		frameData: new VRFrameData(),
	}
	

	// get vrDisplay
	navigator.getVRDisplays().then(function (vrDisplays){
		if( vrDisplays.length === 0 )	alert('no vrDisplays available')
		var vrDisplay = _this._tangoContext.vrDisplay = vrDisplays[0]

		console.log('vrDisplays.displayName :', vrDisplay.displayName)

		// init vrPointCloud
		if( vrDisplay.displayName === "Tango VR Device" ){
                	_this._tangoContext.vrPointCloud = new THREE.WebAR.VRPointCloud(vrDisplay, true)
		}

		// NOTE it doesnt seem necessary and it fails on tango
		// var canvasElement = document.createElement('canvas')
		// document.body.appendChild(canvasElement)
		// _this._tangoContext.requestPresent([{ source: canvasElement }]).then(function(){
		// 	console.log('vrdisplay request accepted')
		// });

		onCompleted()
	});
}


ARjs.Context.prototype._updateTango = function(srcElement){
	// console.log('update aruco here')
	var _this = this
	var arMarkersControls = this._arMarkersControls
	var tangoContext= this._tangoContext
	var vrDisplay = this._tangoContext.vrDisplay

	// check vrDisplay is already initialized
	if( vrDisplay === null )	return


        // Update the point cloud. Only if the point cloud will be shown the geometry is also updated.
	if( vrDisplay.displayName === "Tango VR Device" ){
	        var showPointCloud = true
		var pointsToSkip = 0
	        _this._tangoContext.vrPointCloud.update(showPointCloud, pointsToSkip, true)                        		
	}


	if( this._arMarkersControls.length === 0 )	return

	// TODO here do a fake search on barcode/1001 ?

	var foundControls = this._arMarkersControls[0]
	
	var frameData = this._tangoContext.frameData

	// read frameData
	vrDisplay.getFrameData(frameData);

	if( frameData.pose.position === null )		return
	if( frameData.pose.orientation === null )	return

	// create cameraTransformMatrix
	var position = new THREE.Vector3().fromArray(frameData.pose.position)
	var quaternion = new THREE.Quaternion().fromArray(frameData.pose.orientation)
	var scale = new THREE.Vector3(1,1,1)
	var cameraTransformMatrix = new THREE.Matrix4().compose(position, quaternion, scale)
	// compute modelViewMatrix from cameraTransformMatrix
	var modelViewMatrix = new THREE.Matrix4()
	modelViewMatrix.getInverse( cameraTransformMatrix )	

	foundControls.updateWithModelViewMatrix(modelViewMatrix)
		
	// console.log('position', position)
	// if( position.x !== 0 ||  position.y !== 0 ||  position.z !== 0 ){		
	// 	console.log('vrDisplay tracking')
	// }else{
	// 	console.log('vrDisplay NOT tracking')
	// }

}
var ARjs = ARjs || {}
var THREEx = THREEx || {}

/**
 * ArToolkitProfile helps you build parameters for artoolkit
 * - it is fully independent of the rest of the code
 * - all the other classes are still expecting normal parameters
 * - you can use this class to understand how to tune your specific usecase
 * - it is made to help people to build parameters without understanding all the underlying details.
 */
ARjs.Profile = THREEx.ArToolkitProfile = function(){
	this.reset()

	this.performance('default')
}


ARjs.Profile.prototype._guessPerformanceLabel = function() {
	var isMobile = navigator.userAgent.match(/Android/i)
			|| navigator.userAgent.match(/webOS/i)
			|| navigator.userAgent.match(/iPhone/i)
			|| navigator.userAgent.match(/iPad/i)
			|| navigator.userAgent.match(/iPod/i)
			|| navigator.userAgent.match(/BlackBerry/i)
			|| navigator.userAgent.match(/Windows Phone/i)
			? true : false 
	if( isMobile === true ){
		return 'phone-normal'
	}
	return 'desktop-normal'
}

//////////////////////////////////////////////////////////////////////////////
//		Code Separator
//////////////////////////////////////////////////////////////////////////////

/**
 * reset all parameters
 */
ARjs.Profile.prototype.reset = function () {
	this.sourceParameters = {
		// to read from the webcam 
		sourceType : 'webcam',
	}

	this.contextParameters = {
		cameraParametersUrl: THREEx.ArToolkitContext.baseURL + '../data/data/camera_para.dat',
		detectionMode: 'mono',
	}
	this.defaultMarkerParameters = {
		type : 'pattern',
		patternUrl : THREEx.ArToolkitContext.baseURL + '../data/data/patt.hiro',
		changeMatrixMode: 'modelViewMatrix',
	}
	return this
};

//////////////////////////////////////////////////////////////////////////////
//		Performance
//////////////////////////////////////////////////////////////////////////////



ARjs.Profile.prototype.performance = function(label) {

	if( label === 'default' ){
		label = this._guessPerformanceLabel()
	}

	if( label === 'desktop-fast' ){
		this.contextParameters.canvasWidth = 640*3
		this.contextParameters.canvasHeight = 480*3

		this.contextParameters.maxDetectionRate = 30
	}else if( label === 'desktop-normal' ){
		this.contextParameters.canvasWidth = 640
		this.contextParameters.canvasHeight = 480

		this.contextParameters.maxDetectionRate = 60
	}else if( label === 'phone-normal' ){
		this.contextParameters.canvasWidth = 80*4
		this.contextParameters.canvasHeight = 60*4

		this.contextParameters.maxDetectionRate = 30
	}else if( label === 'phone-slow' ){
		this.contextParameters.canvasWidth = 80*3
		this.contextParameters.canvasHeight = 60*3

		this.contextParameters.maxDetectionRate = 30		
	}else {
		console.assert(false, 'unknonwn label '+label)
	}
	return this
}

//////////////////////////////////////////////////////////////////////////////
//		Marker
//////////////////////////////////////////////////////////////////////////////


ARjs.Profile.prototype.defaultMarker = function (trackingBackend) {
	trackingBackend = trackingBackend || this.contextParameters.trackingBackend

	if( trackingBackend === 'artoolkit' ){
		this.contextParameters.detectionMode = 'mono'
		this.defaultMarkerParameters.type = 'pattern'
		this.defaultMarkerParameters.patternUrl = THREEx.ArToolkitContext.baseURL + '../data/data/patt.hiro'
	}else if( trackingBackend === 'aruco' ){
		this.contextParameters.detectionMode = 'mono'
		this.defaultMarkerParameters.type = 'barcode'
		this.defaultMarkerParameters.barcodeValue = 1001
	}else if( trackingBackend === 'tango' ){
		// FIXME temporary placeholder - to reevaluate later
		this.defaultMarkerParameters.type = 'barcode'
		this.defaultMarkerParameters.barcodeValue = 1001
	}else console.assert(false)

	return this
}
//////////////////////////////////////////////////////////////////////////////
//		Source
//////////////////////////////////////////////////////////////////////////////
ARjs.Profile.prototype.sourceWebcam = function () {
	this.sourceParameters.sourceType = 'webcam'
	delete this.sourceParameters.sourceUrl
	return this
}

ARjs.Profile.prototype.sourceVideo = function (url) {
	this.sourceParameters.sourceType = 'video'
	this.sourceParameters.sourceUrl = url
	return this
}

ARjs.Profile.prototype.sourceImage = function (url) {
	this.sourceParameters.sourceType = 'image'
	this.sourceParameters.sourceUrl = url
	return this
}

//////////////////////////////////////////////////////////////////////////////
//		trackingBackend
//////////////////////////////////////////////////////////////////////////////
ARjs.Profile.prototype.trackingBackend = function (trackingBackend) {
	console.warn('stop profile.trackingBackend() obsolete function. use .trackingMethod instead')
	this.contextParameters.trackingBackend = trackingBackend
	return this
}

//////////////////////////////////////////////////////////////////////////////
//		trackingBackend
//////////////////////////////////////////////////////////////////////////////
ARjs.Profile.prototype.changeMatrixMode = function (changeMatrixMode) {
	this.defaultMarkerParameters.changeMatrixMode = changeMatrixMode
	return this
}

//////////////////////////////////////////////////////////////////////////////
//		trackingBackend
//////////////////////////////////////////////////////////////////////////////
ARjs.Profile.prototype.trackingMethod = function (trackingMethod) {
	var data = ARjs.Utils.parseTrackingMethod(trackingMethod)
	this.defaultMarkerParameters.markersAreaEnabled = data.markersAreaEnabled
	this.contextParameters.trackingBackend = data.trackingBackend	
	return this
}

/**
 * check if the profile is valid. Throw an exception is not valid
 */
ARjs.Profile.prototype.checkIfValid = function () {
	if( this.contextParameters.trackingBackend === 'tango' ){
		this.sourceImage(THREEx.ArToolkitContext.baseURL + '../data/images/img.jpg')
	}
	return this
}
var ARjs = ARjs || {}
var THREEx = THREEx || {}

ARjs.Source = THREEx.ArToolkitSource = function(parameters){	
	var _this = this

	this.ready = false
        this.domElement = null

	// handle default parameters
	this.parameters = {
		// type of source - ['webcam', 'image', 'video']
		sourceType : 'webcam',
		// url of the source - valid if sourceType = image|video
		sourceUrl : null,
		
		// resolution of at which we initialize in the source image
		sourceWidth: 640,
		sourceHeight: 480,
		// resolution displayed for the source 
		displayWidth: 640,
		displayHeight: 480,
	}
	//////////////////////////////////////////////////////////////////////////////
	//		setParameters
	//////////////////////////////////////////////////////////////////////////////
	setParameters(parameters)
	function setParameters(parameters){
		if( parameters === undefined )	return
		for( var key in parameters ){
			var newValue = parameters[ key ]

			if( newValue === undefined ){
				console.warn( "THREEx.ArToolkitSource: '" + key + "' parameter is undefined." )
				continue
			}

			var currentValue = _this.parameters[ key ]

			if( currentValue === undefined ){
				console.warn( "THREEx.ArToolkitSource: '" + key + "' is not a property of this material." )
				continue
			}

			_this.parameters[ key ] = newValue
		}
	}	
}

//////////////////////////////////////////////////////////////////////////////
//		Code Separator
//////////////////////////////////////////////////////////////////////////////
ARjs.Source.prototype.init = function(onReady, onError){
	var _this = this

        if( this.parameters.sourceType === 'image' ){
                var domElement = this._initSourceImage(onSourceReady, onError)                        
        }else if( this.parameters.sourceType === 'video' ){
                var domElement = this._initSourceVideo(onSourceReady, onError)                        
        }else if( this.parameters.sourceType === 'webcam' ){
                // var domElement = this._initSourceWebcamOld(onSourceReady)                        
                var domElement = this._initSourceWebcam(onSourceReady, onError)                        
        }else{
                console.assert(false)
        }

	// attach
        this.domElement = domElement
        this.domElement.style.position = 'absolute'
        this.domElement.style.top = '0px'
        this.domElement.style.left = '0px'
        this.domElement.style.zIndex = '-2'

	return this
        function onSourceReady(){
		document.body.appendChild(_this.domElement);

		_this.ready = true

		onReady && onReady()
        }
} 

////////////////////////////////////////////////////////////////////////////////
//          init image source
////////////////////////////////////////////////////////////////////////////////


ARjs.Source.prototype._initSourceImage = function(onReady) {
	// TODO make it static
        var domElement = document.createElement('img')
	domElement.src = this.parameters.sourceUrl

	domElement.width = this.parameters.sourceWidth
	domElement.height = this.parameters.sourceHeight
	domElement.style.width = this.parameters.displayWidth+'px'
	domElement.style.height = this.parameters.displayHeight+'px'

	// wait until the video stream is ready
	var interval = setInterval(function() {
		if (!domElement.naturalWidth)	return;
		onReady()
		clearInterval(interval)
	}, 1000/50);

	return domElement                
}

////////////////////////////////////////////////////////////////////////////////
//          init video source
////////////////////////////////////////////////////////////////////////////////


ARjs.Source.prototype._initSourceVideo = function(onReady) {
	// TODO make it static
	var domElement = document.createElement('video');
	domElement.src = this.parameters.sourceUrl

	domElement.style.objectFit = 'initial'

	domElement.autoplay = true;
	domElement.webkitPlaysinline = true;
	domElement.controls = false;
	domElement.loop = true;
	domElement.muted = true

	// trick to trigger the video on android
	document.body.addEventListener('click', function onClick(){
		document.body.removeEventListener('click', onClick);
		domElement.play()
	})

	domElement.width = this.parameters.sourceWidth
	domElement.height = this.parameters.sourceHeight
	domElement.style.width = this.parameters.displayWidth+'px'
	domElement.style.height = this.parameters.displayHeight+'px'
	
	// wait until the video stream is ready
	var interval = setInterval(function() {
		if (!domElement.videoWidth)	return;
		onReady()
		clearInterval(interval)
	}, 1000/50);
	return domElement
}

////////////////////////////////////////////////////////////////////////////////
//          handle webcam source
////////////////////////////////////////////////////////////////////////////////

ARjs.Source.prototype._initSourceWebcam = function(onReady, onError) {
	var _this = this

	// init default value
	onError = onError || function(error){	
		alert('Webcam Error\nName: '+error.name + '\nMessage: '+error.message)
	}

	var domElement = document.createElement('video');
	domElement.setAttribute('autoplay', '');
	domElement.setAttribute('muted', '');
	domElement.setAttribute('playsinline', '');
	domElement.style.width = this.parameters.displayWidth+'px'
	domElement.style.height = this.parameters.displayHeight+'px'

	// check API is available
	if (navigator.mediaDevices === undefined 
			|| navigator.mediaDevices.enumerateDevices === undefined 
			|| navigator.mediaDevices.getUserMedia === undefined  ){
		if( navigator.mediaDevices === undefined )				var fctName = 'navigator.mediaDevices'
		else if( navigator.mediaDevices.enumerateDevices === undefined )	var fctName = 'navigator.mediaDevices.enumerateDevices'
		else if( navigator.mediaDevices.getUserMedia === undefined )		var fctName = 'navigator.mediaDevices.getUserMedia'
		else console.assert(false)
		onError({
			name: '',
			message: 'WebRTC issue-! '+fctName+' not present in your browser'
		})
		return null
	}

	// get available devices
	navigator.mediaDevices.enumerateDevices().then(function(devices) {
                var userMediaConstraints = {
			audio: false,
			video: {
				facingMode: 'environment',
				width: {
					ideal: _this.parameters.sourceWidth,
					// min: 1024,
					// max: 1920
				},
				height: {
					ideal: _this.parameters.sourceHeight,
					// min: 776,
					// max: 1080
				}
		  	}
                }
		// get a device which satisfy the constraints
		navigator.mediaDevices.getUserMedia(userMediaConstraints).then(function success(stream) {
			// set the .src of the domElement
			domElement.srcObject = stream;
			// to start the video, when it is possible to start it only on userevent. like in android
			document.body.addEventListener('click', function(){
				domElement.play();
			})
			// domElement.play();

// TODO listen to loadedmetadata instead
			// wait until the video stream is ready
			var interval = setInterval(function() {
				if (!domElement.videoWidth)	return;
				onReady()
				clearInterval(interval)
			}, 1000/50);
		}).catch(function(error) {
			onError({
				name: error.name,
				message: error.message
			});
		});
	}).catch(function(error) {
		onError({
			message: error.message
		});
	});

	return domElement
}

//////////////////////////////////////////////////////////////////////////////
//		Handle Mobile Torch
//////////////////////////////////////////////////////////////////////////////
ARjs.Source.prototype.hasMobileTorch = function(){
	var stream = arToolkitSource.domElement.srcObject
	if( stream instanceof MediaStream === false )	return false

	if( this._currentTorchStatus === undefined ){
		this._currentTorchStatus = false
	}

	var videoTrack = stream.getVideoTracks()[0];

	// if videoTrack.getCapabilities() doesnt exist, return false now
	if( videoTrack.getCapabilities === undefined )	return false

	var capabilities = videoTrack.getCapabilities()
	
	return capabilities.torch ? true : false
}

/**
 * toggle the flash/torch of the mobile fun if applicable.
 * Great post about it https://www.oberhofer.co/mediastreamtrack-and-its-capabilities/
 */
ARjs.Source.prototype.toggleMobileTorch = function(){
	// sanity check
	console.assert(this.hasMobileTorch() === true)
		
	var stream = arToolkitSource.domElement.srcObject
	if( stream instanceof MediaStream === false ){
		alert('enabling mobile torch is available only on webcam')
		return
	}

	if( this._currentTorchStatus === undefined ){
		this._currentTorchStatus = false
	}

	var videoTrack = stream.getVideoTracks()[0];
	var capabilities = videoTrack.getCapabilities()
	
	if( !capabilities.torch ){
		alert('no mobile torch is available on your camera')
		return
	}

	this._currentTorchStatus = this._currentTorchStatus === false ? true : false
	videoTrack.applyConstraints({
		advanced: [{
			torch: this._currentTorchStatus
		}]
	}).catch(function(error){
		console.log(error)
	});
}

////////////////////////////////////////////////////////////////////////////////
//          handle resize
////////////////////////////////////////////////////////////////////////////////

ARjs.Source.prototype.onResizeElement = function(mirrorDomElements){
	var _this = this
	var screenWidth = window.innerWidth
	var screenHeight = window.innerHeight

	// compute sourceWidth, sourceHeight
	if( this.domElement.nodeName === "IMG" ){
		var sourceWidth = this.domElement.naturalWidth
		var sourceHeight = this.domElement.naturalHeight
	}else if( this.domElement.nodeName === "VIDEO" ){
		var sourceWidth = this.domElement.videoWidth
		var sourceHeight = this.domElement.videoHeight
	}else{
		console.assert(false)
	}
	
	// compute sourceAspect
	var sourceAspect = sourceWidth / sourceHeight
	// compute screenAspect
	var screenAspect = screenWidth / screenHeight

	// if screenAspect < sourceAspect, then change the width, else change the height
	if( screenAspect < sourceAspect ){
		// compute newWidth and set .width/.marginLeft
		var newWidth = sourceAspect * screenHeight
		this.domElement.style.width = newWidth+'px'
		this.domElement.style.marginLeft = -(newWidth-screenWidth)/2+'px'
		
		// init style.height/.marginTop to normal value
		this.domElement.style.height = screenHeight+'px'
		this.domElement.style.marginTop = '0px'
	}else{
		// compute newHeight and set .height/.marginTop
		var newHeight = 1 / (sourceAspect / screenWidth)
		this.domElement.style.height = newHeight+'px'
		this.domElement.style.marginTop = -(newHeight-screenHeight)/2+'px'
		
		// init style.width/.marginLeft to normal value
		this.domElement.style.width = screenWidth+'px'
		this.domElement.style.marginLeft = '0px'
	}
	
	
	if( arguments.length !== 0 ){
		debugger
		console.warn('use bad signature for arToolkitSource.copyElementSizeTo')
	}
	// honor default parameters
	// if( mirrorDomElements !== undefined )	console.warn('still use the old resize. fix it')
	if( mirrorDomElements === undefined )	mirrorDomElements = []
	if( mirrorDomElements instanceof Array === false )	mirrorDomElements = [mirrorDomElements]	

	// Mirror _this.domElement.style to mirrorDomElements
	mirrorDomElements.forEach(function(domElement){
		_this.copyElementSizeTo(domElement)
	})
}

ARjs.Source.prototype.copyElementSizeTo = function(otherElement){
	otherElement.style.width = this.domElement.style.width
	otherElement.style.height = this.domElement.style.height	
	otherElement.style.marginLeft = this.domElement.style.marginLeft
	otherElement.style.marginTop = this.domElement.style.marginTop
}

//////////////////////////////////////////////////////////////////////////////
//		Code Separator
//////////////////////////////////////////////////////////////////////////////

ARjs.Source.prototype.copySizeTo = function(){
	console.warn('obsolete function arToolkitSource.copySizeTo. Use arToolkitSource.copyElementSizeTo' )
	this.copyElementSizeTo.apply(this, arguments)
}

//////////////////////////////////////////////////////////////////////////////
//		Code Separator
//////////////////////////////////////////////////////////////////////////////

ARjs.Source.prototype.onResize	= function(arToolkitContext, renderer, camera){
	if( arguments.length !== 3 ){
		console.warn('obsolete function arToolkitSource.onResize. Use arToolkitSource.onResizeElement' )
		return this.onResizeElement.apply(this, arguments)
	}

	var trackingBackend = arToolkitContext.parameters.trackingBackend
	

	// RESIZE DOMELEMENT
	if( trackingBackend === 'artoolkit' ){

		this.onResizeElement()
		
		var isAframe = renderer.domElement.dataset.aframeCanvas ? true : false
		if( isAframe === false ){
			this.copyElementSizeTo(renderer.domElement)	
		}else{
			
		}

		if( arToolkitContext.arController !== null ){
			this.copyElementSizeTo(arToolkitContext.arController.canvas)	
		}
	}else if( trackingBackend === 'aruco' ){
		this.onResizeElement()
		this.copyElementSizeTo(renderer.domElement)	

		this.copyElementSizeTo(arToolkitContext.arucoContext.canvas)	
	}else if( trackingBackend === 'tango' ){
		renderer.setSize( window.innerWidth, window.innerHeight )
	}else console.assert(false, 'unhandled trackingBackend '+trackingBackend)


	// UPDATE CAMERA
	if( trackingBackend === 'artoolkit' ){
		if( arToolkitContext.arController !== null ){
			camera.projectionMatrix.copy( arToolkitContext.getProjectionMatrix() );			
		}
	}else if( trackingBackend === 'aruco' ){	
		camera.aspect = renderer.domElement.width / renderer.domElement.height;
		camera.updateProjectionMatrix();			
	}else if( trackingBackend === 'tango' ){
		var vrDisplay = arToolkitContext._tangoContext.vrDisplay
		// make camera fit vrDisplay
		if( vrDisplay && vrDisplay.displayName === "Tango VR Device" ) THREE.WebAR.resizeVRSeeThroughCamera(vrDisplay, camera)
	}else console.assert(false, 'unhandled trackingBackend '+trackingBackend)	
}
var THREEx = THREEx || {}

THREEx.ArVideoInWebgl = function(videoTexture){	
	var _this = this
	
	//////////////////////////////////////////////////////////////////////////////
	//	plane always in front of the camera, exactly as big as the viewport
	//////////////////////////////////////////////////////////////////////////////
	var geometry = new THREE.PlaneGeometry(2, 2);
	var material = new THREE.MeshBasicMaterial({
		// map : new THREE.TextureLoader().load('images/water.jpg'),
		map : videoTexture,
		// side: THREE.DoubleSide,
		// opacity: 0.5,
		// color: 'pink',
		// transparent: true,
	});
	var seethruPlane = new THREE.Mesh(geometry, material);
	this.object3d = seethruPlane
	// scene.add(seethruPlane);
	
	// arToolkitSource.domElement.style.visibility = 'hidden'

	// TODO extract the fov from the projectionMatrix
	// camera.fov = 43.1
	this.update = function(camera){
		camera.updateMatrixWorld(true)
		
		// get seethruPlane position
		var position = new THREE.Vector3(-0,0,-20)	// TODO how come you got that offset on x ???
		var position = new THREE.Vector3(-0,0,-20)	// TODO how come you got that offset on x ???
		seethruPlane.position.copy(position)
		camera.localToWorld(seethruPlane.position)
		
		// get seethruPlane quaternion
		camera.matrixWorld.decompose( camera.position, camera.quaternion, camera.scale );	
		seethruPlane.quaternion.copy( camera.quaternion )
		
		// extract the fov from the projectionMatrix
		var fov = THREE.Math.radToDeg(Math.atan(1/camera.projectionMatrix.elements[5]))*2;
	// console.log('fov', fov)
		
		var elementWidth = parseFloat( arToolkitSource.domElement.style.width.replace(/px$/,''), 10 )
		var elementHeight = parseFloat( arToolkitSource.domElement.style.height.replace(/px$/,''), 10 )
		
		var aspect = elementWidth / elementHeight
		
		// camera.fov = fov
		// if( vrDisplay.isPresenting ){
		// 	fov *= 2
		// 	aspect *= 2
		// }
		
		// get seethruPlane height relative to fov
		seethruPlane.scale.y = Math.tan(THREE.Math.DEG2RAD * fov/2)*position.length() 
		// get seethruPlane aspect
		seethruPlane.scale.x = seethruPlane.scale.y * aspect
	}

	//////////////////////////////////////////////////////////////////////////////
	//		Code Separator
	//////////////////////////////////////////////////////////////////////////////
	// var video = arToolkitSource.domElement;
	// 
	// window.addEventListener('resize', function(){
	// 	updateSeeThruAspectUv(seethruPlane)	
	// })
	// video.addEventListener('canplaythrough', function(){
	// 	updateSeeThruAspectUv(seethruPlane)
	// })
	// function updateSeeThruAspectUv(plane){
	// 
	// 	// if video isnt yet ready to play
	// 	if( video.videoWidth === 0 || video.videoHeight === 0 )	return
	// 
	// 	var faceVertexUvs = plane.geometry.faceVertexUvs[0]
	// 	var screenAspect = window.innerWidth / window.innerHeight
	// 	var videoAspect = video.videoWidth / video.videoHeight
	// 	
	// 	plane.geometry.uvsNeedUpdate = true
	// 	if( screenAspect >= videoAspect ){
	// 		var actualHeight = videoAspect / screenAspect;
	// 		// faceVertexUvs y 0
	// 		faceVertexUvs[0][1].y = 0.5 - actualHeight/2
	// 		faceVertexUvs[1][0].y = 0.5 - actualHeight/2
	// 		faceVertexUvs[1][1].y = 0.5 - actualHeight/2
	// 		// faceVertexUvs y 1
	// 		faceVertexUvs[0][0].y = 0.5 + actualHeight/2
	// 		faceVertexUvs[0][2].y = 0.5 + actualHeight/2
	// 		faceVertexUvs[1][2].y = 0.5 + actualHeight/2
	// 	}else{
	// 		var actualWidth = screenAspect / videoAspect;
	// 		// faceVertexUvs x 0
	// 		faceVertexUvs[0][0].x = 0.5 - actualWidth/2
	// 		faceVertexUvs[0][1].x = 0.5 - actualWidth/2
	// 		faceVertexUvs[1][0].x = 0.5 - actualWidth/2
	// 		
	// 		// faceVertexUvs x 1
	// 		faceVertexUvs[0][2].x = 0.5 + actualWidth/2
	// 		faceVertexUvs[1][1].x = 0.5 + actualWidth/2
	// 		faceVertexUvs[1][2].x = 0.5 + actualWidth/2
	// 	}
	// }

}
var THREEx = THREEx || {}

// TODO this is useless - prefere arjs-HitTesting.js

/**
 * - maybe support .onClickFcts in each object3d
 * - seems an easy light layer for clickable object
 * - up to 
 */
THREEx.HitTestingPlane = function(sourceElement){
	this._sourceElement = sourceElement

	// create _pickingScene
	this._pickingScene = new THREE.Scene
	
	// create _pickingPlane
	var geometry = new THREE.PlaneGeometry(20,20,19,19).rotateX(-Math.PI/2)
	// var geometry = new THREE.PlaneGeometry(20,20).rotateX(-Math.PI/2)
	var material = new THREE.MeshBasicMaterial({
		// opacity: 0.5,
		// transparent: true,
		wireframe: true
	})
	// material.visible = false
	this._pickingPlane = new THREE.Mesh(geometry, material)
	this._pickingScene.add(this._pickingPlane)

	// Create pickingCamera
	var fullWidth = parseInt(sourceElement.style.width)
	var fullHeight = parseInt(sourceElement.style.height)
	// TODO hardcoded fov - couch
	this._pickingCamera = new THREE.PerspectiveCamera(42, fullWidth / fullHeight, 0.1, 30);	
}

//////////////////////////////////////////////////////////////////////////////
//		update function
//////////////////////////////////////////////////////////////////////////////

THREEx.HitTestingPlane.prototype.update = function(camera, pickingRoot, changeMatrixMode){

	this.onResize()
	

	if( changeMatrixMode === 'modelViewMatrix' ){
		// set pickingPlane position
		var pickingPlane = this._pickingPlane
		pickingRoot.parent.updateMatrixWorld()
		pickingPlane.matrix.copy(pickingRoot.parent.matrixWorld)
		// set position/quaternion/scale from pickingPlane.matrix
		pickingPlane.matrix.decompose(pickingPlane.position, pickingPlane.quaternion, pickingPlane.scale)
	}else if( changeMatrixMode === 'cameraTransformMatrix' ){
		// set pickingPlane position
		var pickingCamera = this._pickingCamera
		camera.updateMatrixWorld()
		pickingCamera.matrix.copy(camera.matrixWorld)
		// set position/quaternion/scale from pickingCamera.matrix
		pickingCamera.matrix.decompose(pickingCamera.position, pickingCamera.quaternion, pickingCamera.scale)
	}else console.assert(false)


// var position = this._pickingPlane.position
// console.log('pickingPlane position', position.x.toFixed(2), position.y.toFixed(2), position.z.toFixed(2))
// var position = this._pickingCamera.position
// console.log('his._pickingCamera position', position.x.toFixed(2), position.y.toFixed(2), position.z.toFixed(2))
	
}

//////////////////////////////////////////////////////////////////////////////
//		resize camera
//////////////////////////////////////////////////////////////////////////////

THREEx.HitTestingPlane.prototype.onResize = function(){
	var sourceElement = this._sourceElement
	var pickingCamera = this._pickingCamera
	
// FIXME why using css here ??? not even computed style
// should get the size of the elment directly independantly 
	var fullWidth = parseInt(sourceElement.style.width)
	var fullHeight = parseInt(sourceElement.style.height)
	pickingCamera.aspect = fullWidth / fullHeight

	pickingCamera.updateProjectionMatrix()
}

//////////////////////////////////////////////////////////////////////////////
//		Perform test
//////////////////////////////////////////////////////////////////////////////
THREEx.HitTestingPlane.prototype.test = function(mouseX, mouseY){
	// convert mouseX, mouseY to [-1, +1]
	mouseX = (mouseX-0.5)*2
	mouseY =-(mouseY-0.5)*2
	
	this._pickingScene.updateMatrixWorld(true)

	// compute intersections between mouseVector3 and pickingPlane
	var raycaster = new THREE.Raycaster();
	var mouseVector3 = new THREE.Vector3(mouseX, mouseY, 1);
	raycaster.setFromCamera( mouseVector3, this._pickingCamera )
	var intersects = raycaster.intersectObjects( [this._pickingPlane] )

	if( intersects.length === 0 )	return null

	// set new demoRoot position
	var position = this._pickingPlane.worldToLocal( intersects[0].point.clone() )
	// TODO here do a look at the camera ?
	var quaternion = new THREE.Quaternion
	var scale = new THREE.Vector3(1,1,1)//.multiplyScalar(1)
	
	return {
		position : position,
		quaternion : quaternion,
		scale : scale
	}
}

//////////////////////////////////////////////////////////////////////////////
//		render the pickingPlane for debug
//////////////////////////////////////////////////////////////////////////////

THREEx.HitTestingPlane.prototype.renderDebug = function(renderer){
	// render sceneOrtho
	renderer.render( this._pickingScene, this._pickingCamera )
}
var THREEx = THREEx || {}

/**
 * @class
 * 
 * @return {[type]} [description]
 */
THREEx.HitTestingTango = function(arContext){
	this._arContext = arContext
	// seems to be the object bounding sphere for picking
	this.boundingSphereRadius = 0.01
	// default result scale
	this.resultScale = new THREE.Vector3(1,1,1).multiplyScalar(1)
}

//////////////////////////////////////////////////////////////////////////////
//		update function
//////////////////////////////////////////////////////////////////////////////

THREEx.HitTestingTango.prototype.update = function(){
}

//////////////////////////////////////////////////////////////////////////////
//		Code Separator
//////////////////////////////////////////////////////////////////////////////
/**
 * do the actual testing
 * 
 * @param {ARjs.Context} arContext - context to use
 * @param {Number} mouseX    - mouse x coordinate in [0, 1]
 * @param {Numer} mouseY    - mouse y coordinate in [0, 1]
 * @return {Object} - result
 */
THREEx.HitTestingTango.prototype.test = function(mouseX, mouseY){
	var vrDisplay = this._arContext._tangoContext.vrDisplay
        if (vrDisplay === null ) return null
	
	if( vrDisplay.displayName !== "Tango VR Device" )	return null
	
        var pointAndPlane = vrDisplay.getPickingPointAndPlaneInPointCloud(mouseX, mouseY)
        if( pointAndPlane == null ) {
                console.warn('Could not retrieve the correct point and plane.')
                return null
        }
	
	// FIXME not sure what this is
	var boundingSphereRadius = 0.01	
	
	// the bigger the number the likeliest it crash chromium-webar

        // Orient and position the model in the picking point according
        // to the picking plane. The offset is half of the model size.
        var object3d = new THREE.Object3D
        THREE.WebAR.positionAndRotateObject3DWithPickingPointAndPlaneInPointCloud(
                pointAndPlane, object3d, this.boundingSphereRadius
        )
	object3d.rotateZ(-Math.PI/2)

	// return the result
	var result = {
		position : object3d.position,
		quaternion : object3d.quaternion,
		scale : this.resultScale,
	}

	return result
}
// @namespace
var ARjs = ARjs || {}

// TODO this is a controls... should i give the object3d here ?
// not according to 'no three.js dependancy'

/**
 * Create an anchor in the real world
 * 
 * @param {ARjs.Session} arSession - the session on which we create the anchor
 * @param {Object} markerParameters - parameter of this anchor
 */
ARjs.Anchor = function(arSession, markerParameters){
	var _this = this
	var arContext = arSession.arContext
	var scene = arSession.parameters.scene
	var camera = arSession.parameters.camera
	
	this.arSession = arSession
	this.parameters = markerParameters
	
	// log to debug
	console.log('ARjs.Anchor -', 'changeMatrixMode:', this.parameters.changeMatrixMode, '/ markersAreaEnabled:', markerParameters.markersAreaEnabled)


	var markerRoot = new THREE.Group
	scene.add(markerRoot)

	// set controlledObject depending on changeMatrixMode
	if( markerParameters.changeMatrixMode === 'modelViewMatrix' ){
		var controlledObject = markerRoot
	}else if( markerParameters.changeMatrixMode === 'cameraTransformMatrix' ){
		var controlledObject = camera
	}else console.assert(false)

	if( markerParameters.markersAreaEnabled === false ){
		var markerControls = new THREEx.ArMarkerControls(arContext, controlledObject, markerParameters)		
	}else{
		// sanity check - MUST be a trackingBackend with markers
		console.assert( arContext.parameters.trackingBackend === 'artoolkit' || arContext.parameters.trackingBackend === 'aruco' )
		// for multi marker
		if( localStorage.getItem('ARjsMultiMarkerFile') === null ){
			ARjs.MarkersAreaUtils.storeDefaultMultiMarkerFile(arContext.parameters.trackingBackend)
		}
		
		// get multiMarkerFile from localStorage
		console.assert( localStorage.getItem('ARjsMultiMarkerFile') !== null )
		var multiMarkerFile = localStorage.getItem('ARjsMultiMarkerFile')

		// set controlledObject depending on changeMatrixMode
		if( markerParameters.changeMatrixMode === 'modelViewMatrix' ){
			var parent3D = scene
		}else if( markerParameters.changeMatrixMode === 'cameraTransformMatrix' ){
			var parent3D = camera
		}else console.assert(false)
	
		// build a multiMarkerControls
		var multiMarkerControls = ARjs.MarkersAreaControls.fromJSON(arContext, parent3D, controlledObject, multiMarkerFile)
		
		// honor markerParameters.changeMatrixMode
		multiMarkerControls.parameters.changeMatrixMode = markerParameters.changeMatrixMode

		// create ArMarkerHelper - useful to debug
		var markerHelpers = []
		multiMarkerControls.subMarkersControls.forEach(function(subMarkerControls){
			// add an helper to visuable each sub-marker
			var markerHelper = new THREEx.ArMarkerHelper(subMarkerControls)
			markerHelper.object3d.visible = false
			// subMarkerControls.object3d.add( markerHelper.object3d )		
			subMarkerControls.object3d.add( markerHelper.object3d )		
			// add it to markerHelpers
			markerHelpers.push(markerHelper)
		})
		// define API specific to markersArea
		this.markersArea = {}
		this.markersArea.setSubMarkersVisibility = function(visible){
			markerHelpers.forEach(function(markerHelper){
				markerHelper.object3d.visible = visible
			})
		}
	}
	
	this.object3d = new THREE.Group()
		
	//////////////////////////////////////////////////////////////////////////////
	//		THREEx.ArSmoothedControls
	//////////////////////////////////////////////////////////////////////////////
	
	var shouldBeSmoothed = true
	if( arContext.parameters.trackingBackend === 'tango' ) shouldBeSmoothed = false 

	if( shouldBeSmoothed === true ){
		// build a smoothedControls
		var smoothedRoot = new THREE.Group()
		scene.add(smoothedRoot)
		var smoothedControls = new THREEx.ArSmoothedControls(smoothedRoot)
		smoothedRoot.add(this.object3d)	
	}else{
		markerRoot.add(this.object3d)
	}


	//////////////////////////////////////////////////////////////////////////////
	//		Code Separator
	//////////////////////////////////////////////////////////////////////////////
	this.update = function(){	
		// update _this.object3d.visible
		_this.object3d.visible = _this.object3d.parent.visible

		// console.log('controlledObject.visible', _this.object3d.parent.visible)
		if( smoothedControls !== undefined ){
			// update smoothedControls parameters depending on how many markers are visible in multiMarkerControls
			if( multiMarkerControls !== undefined ){
				multiMarkerControls.updateSmoothedControls(smoothedControls)
			}

			// update smoothedControls
			smoothedControls.update(markerRoot)			
		}
	}
}


/**
 * Apply ARjs.Session.HitTestResult to the controlled object3d
 * 
 * @param {ARjs.HitTesting.Result} hitTestResult - the result to apply
 */
ARjs.Anchor.prototype.applyHitTestResult = function(hitTestResult){
	console.warn('obsolete anchro.applyHitTestResult - use hitTestResult.apply(object3d) instead')
	hitTestResult.apply(this.object3d)
	// object3d.position.copy(hitTestResult.position)
	// object3d.quaternion.copy(hitTestResult.quaternion)
	// object3d.scale.copy(hitTestResult.scale)
	// 
	// object3d.updateMatrix()
}
// @namespace
var ARjs = ARjs || {}

/**
 * Create an debug UI for an ARjs.Anchor
 * 
 * @param {ARjs.Anchor} arAnchor - the anchor to user
 */
ARjs.SessionDebugUI = function(arSession, tangoPointCloud){
	var trackingBackend = arSession.arContext.parameters.trackingBackend

	this.domElement = document.createElement('div')
	this.domElement.style.color = 'rgba(0,0,0,0.9)'
	this.domElement.style.backgroundColor = 'rgba(127,127,127,0.5)'
	this.domElement.style.display = 'inline-block'
	this.domElement.style.padding = '0.5em'
	this.domElement.style.margin = '0.5em'
	this.domElement.style.textAlign = 'left'

	//////////////////////////////////////////////////////////////////////////////
	//		add title
	//////////////////////////////////////////////////////////////////////////////
	// var domElement = document.createElement('div')
	// domElement.style.display = 'block'
	// domElement.style.fontWeight = 'bold'
	// domElement.style.fontSize = '120%'
	// this.domElement.appendChild(domElement)
	// domElement.innerHTML = 'AR.js Session Debug'

	//////////////////////////////////////////////////////////////////////////////
	//		current-tracking-backend
	//////////////////////////////////////////////////////////////////////////////

	var domElement = document.createElement('span')
	domElement.style.display = 'block'
	this.domElement.appendChild(domElement)
	domElement.innerHTML = '<b>trackingBackend</b> : ' +trackingBackend
	
	//////////////////////////////////////////////////////////////////////////////
	//		augmented-websites
	//////////////////////////////////////////////////////////////////////////////
	var domElement = document.createElement('a')
	domElement.innerHTML = 'Share on augmented-websites'
	domElement.style.display = 'block'
	domElement.setAttribute('target', '_blank')
	domElement.href = 'https://webxr.io/augmented-website?'+location.href
	this.domElement.appendChild(domElement)				

	//////////////////////////////////////////////////////////////////////////////
	//		toggle-point-cloud
	//////////////////////////////////////////////////////////////////////////////

	if( trackingBackend === 'tango' && tangoPointCloud ){
		var domElement = document.createElement('button')
		this.domElement.appendChild(domElement)

		domElement.id= 'buttonTangoTogglePointCloud'
		domElement.innerHTML = 'toggle-point-cloud'
		domElement.href='javascript:void(0)'

		domElement.addEventListener('click', function(){
			var scene = arSession.parameters.scene
	// TODO how tangoPointCloud, get connected here ???
	// in arguments simply ?
			if( tangoPointCloud.object3d.parent ){
				scene.remove(tangoPointCloud.object3d)
			}else{
				scene.add(tangoPointCloud.object3d)			
			}
		})
	}
}

//////////////////////////////////////////////////////////////////////////////
//		ARjs.AnchorDebugUI
//////////////////////////////////////////////////////////////////////////////

/**
 * Create an debug UI for an ARjs.Anchor
 * 
 * @param {ARjs.Anchor} arAnchor - the anchor to user
 */
ARjs.AnchorDebugUI = function(arAnchor){
	var _this = this 
	var arSession = arAnchor.arSession
	var trackingBackend = arSession.arContext.parameters.trackingBackend
	
	this.domElement = document.createElement('div')
	this.domElement.style.color = 'rgba(0,0,0,0.9)'
	this.domElement.style.backgroundColor = 'rgba(127,127,127,0.5)'
	this.domElement.style.display = 'inline-block'
	this.domElement.style.padding = '0.5em'
	this.domElement.style.margin = '0.5em'
	this.domElement.style.textAlign = 'left'

	//////////////////////////////////////////////////////////////////////////////
	//		add title
	//////////////////////////////////////////////////////////////////////////////

	// var domElement = document.createElement('div')
	// domElement.style.display = 'block'
	// domElement.style.fontWeight = 'bold'
	// domElement.style.fontSize = '120%'
	// this.domElement.appendChild(domElement)
	// domElement.innerHTML = 'Anchor Marker Debug'

	//////////////////////////////////////////////////////////////////////////////
	//		current-tracking-backend
	//////////////////////////////////////////////////////////////////////////////

	var domElement = document.createElement('span')
	domElement.style.display = 'block'
	this.domElement.appendChild(domElement)
	domElement.innerHTML = '<b>markersAreaEnabled</b> :' +arAnchor.parameters.markersAreaEnabled

	//////////////////////////////////////////////////////////////////////////////
	//		toggle-marker-helper
	//////////////////////////////////////////////////////////////////////////////

	if( arAnchor.parameters.markersAreaEnabled ){
		var domElement = document.createElement('button')
		domElement.style.display = 'block'
		this.domElement.appendChild(domElement)

		domElement.id= 'buttonToggleMarkerHelpers'
		domElement.innerHTML = 'toggle-marker-helper'
		domElement.href='javascript:void(0)'

		var subMarkerHelpersVisible = false
		domElement.addEventListener('click', function(){
			subMarkerHelpersVisible = subMarkerHelpersVisible ? false : true
			arAnchor.markersArea.setSubMarkersVisibility(subMarkerHelpersVisible)		
		})
	}
	
	//////////////////////////////////////////////////////////////////////////////
	//		Learn-new-marker-area
	//////////////////////////////////////////////////////////////////////////////

	if( arAnchor.parameters.markersAreaEnabled ){
		var domElement = document.createElement('button')
		domElement.style.display = 'block'
		this.domElement.appendChild(domElement)

		domElement.id = 'buttonMarkersAreaLearner'
		domElement.innerHTML = 'Learn-new-marker-area'
		domElement.href ='javascript:void(0)'

		domElement.addEventListener('click', function(){
			var learnerBaseURL = ARjs.Context.baseURL + 'examples/multi-markers/examples/learner.html'
			ARjs.MarkersAreaUtils.navigateToLearnerPage(learnerBaseURL, trackingBackend)
		})	
	}

	//////////////////////////////////////////////////////////////////////////////
	//		Reset-marker-area
	//////////////////////////////////////////////////////////////////////////////

	if( arAnchor.parameters.markersAreaEnabled ){
		var domElement = document.createElement('button')
		domElement.style.display = 'block'
		this.domElement.appendChild(domElement)

		domElement.id = 'buttonMarkersAreaReset'
		domElement.innerHTML = 'Reset-marker-area'
		domElement.href ='javascript:void(0)'

		domElement.addEventListener('click', function(){
			ARjs.MarkersAreaUtils.storeDefaultMultiMarkerFile(trackingBackend)
			location.reload()
		})
	}
}
// @namespace
var ARjs = ARjs || {}

/**
 * Create an anchor in the real world
 * 
 * @param {ARjs.Session} arSession - the session on which we create the anchor
 * @param {Object} markerParameters - parameter of this anchor
 */
ARjs.HitTesting = function(arSession){
	var _this = this
	var arContext = arSession.arContext
	var trackingBackend = arContext.parameters.trackingBackend

	this.enabled = true
	this._arSession = arSession
	this._hitTestingPlane = null
	this._hitTestingTango = null

	if( trackingBackend === 'tango' ){
		_this._hitTestingTango = new THREEx.HitTestingTango(arContext)
	}else{
		_this._hitTestingPlane = new THREEx.HitTestingPlane(arSession.arSource.domElement)
	}
}

//////////////////////////////////////////////////////////////////////////////
//		update function
//////////////////////////////////////////////////////////////////////////////
/**
 * update
 * 
 * @param {THREE.Camera} camera   - the camera to use
 * @param {THREE.Object3D} object3d - 
 */
ARjs.HitTesting.prototype.update = function (camera, pickingRoot, changeMatrixMode) {
	// if it isnt enabled, do nothing
	if( this.enabled === false )	return

	if( this._hitTestingTango !== null ){
		this._hitTestingTango.update()
	}else if( this._hitTestingPlane !== null ){
		this._hitTestingPlane.update(camera, pickingRoot, changeMatrixMode)
	}else console.assert(false)
}

//////////////////////////////////////////////////////////////////////////////
//		actual hit testing
//////////////////////////////////////////////////////////////////////////////

/**
 * Test the real world for intersections directly from a DomEvent
 * 
 * @param {Number} mouseX - position X of the hit [-1, +1]
 * @param {Number} mouseY - position Y of the hit [-1, +1]
 * @return {[ARjs.HitTesting.Result]} - array of result
 */
ARjs.HitTesting.prototype.testDomEvent = function(domEvent){
	var trackingBackend = this._arSession.arContext.parameters.trackingBackend
	var arSource = this._arSession.arSource

	// if it isnt enabled, do nothing
	if( this.enabled === false )	return []
	
	if( trackingBackend === 'tango' ){
        	var mouseX = domEvent.pageX / window.innerWidth
        	var mouseY = domEvent.pageY / window.innerHeight
	}else{		
		// FIXME should not use css!!!
		var mouseX = domEvent.layerX / parseInt(arSource.domElement.style.width)
		var mouseY = domEvent.layerY / parseInt(arSource.domElement.style.height)
	}

	return this.test(mouseX, mouseY)
}

/**
 * Test the real world for intersections.
 * 
 * @param {Number} mouseX - position X of the hit [0, +1]
 * @param {Number} mouseY - position Y of the hit [0, +1]
 * @return {[ARjs.HitTesting.Result]} - array of result
 */
ARjs.HitTesting.prototype.test = function(mouseX, mouseY){
	var arContext = this._arSession.arContext
	var trackingBackend = arContext.parameters.trackingBackend
	var hitTestResults = []

	// if it isnt enabled, do nothing
	if( this.enabled === false )	return []

	var result = null
	if( trackingBackend === 'tango' ){
		var result = this._hitTestingTango.test(mouseX, mouseY)
	}else{
		var result = this._hitTestingPlane.test(mouseX, mouseY)
	}
			
	// if no result is found, return now
	if( result === null )	return hitTestResults

	// build a ARjs.HitTesting.Result
	var hitTestResult = new ARjs.HitTesting.Result(result.position, result.quaternion, result.scale)
	hitTestResults.push(hitTestResult)
	
	return hitTestResults
}

//////////////////////////////////////////////////////////////////////////////
//		ARjs.HitTesting.Result
//////////////////////////////////////////////////////////////////////////////
/**
 * Contains the result of ARjs.HitTesting.test()
 * 
 * @param {THREE.Vector3} position - position to use
 * @param {THREE.Quaternion} quaternion - quaternion to use
 * @param {THREE.Vector3} scale - scale
 */
ARjs.HitTesting.Result = function(position, quaternion, scale){
	this.position = position
	this.quaternion = quaternion
	this.scale = scale
}

/**
 * Apply to a controlled object3d
 * 
 * @param {THREE.Object3D} object3d - the result to apply
 */
ARjs.HitTesting.Result.prototype.apply = function(object3d){
	object3d.position.copy(this.position)
	object3d.quaternion.copy(this.quaternion)
	object3d.scale.copy(this.scale)

	object3d.updateMatrix()
}

/**
 * Apply to a controlled object3d
 * 
 * @param {THREE.Object3D} object3d - the result to apply
 */
ARjs.HitTesting.Result.prototype.applyPosition = function(object3d){
	object3d.position.copy(this.position)

	object3d.updateMatrix()

	return this
}

/**
 * Apply to a controlled object3d
 * 
 * @param {THREE.Object3D} object3d - the result to apply
 */
ARjs.HitTesting.Result.prototype.applyQuaternion = function(object3d){
	object3d.quaternion.copy(this.quaternion)

	object3d.updateMatrix()

	return this
}
var ARjs = ARjs || {}

/**
 * define a ARjs.Session
 * 
 * @param {Object} parameters - parameters for this session
 */
ARjs.Session = function(parameters){
	var _this = this
	// handle default parameters
	this.parameters = {
		renderer: null,
		camera: null,
		scene: null,
		sourceParameters: {},
		contextParameters: {},
	}

	//////////////////////////////////////////////////////////////////////////////
	//		setParameters
	//////////////////////////////////////////////////////////////////////////////
	setParameters(parameters)
	function setParameters(parameters){
		if( parameters === undefined )	return
		for( var key in parameters ){
			var newValue = parameters[ key ]

			if( newValue === undefined ){
				console.warn( "THREEx.Session: '" + key + "' parameter is undefined." )
				continue
			}

			var currentValue = _this.parameters[ key ]

			if( currentValue === undefined ){
				console.warn( "THREEx.Session: '" + key + "' is not a property of this material." )
				continue
			}

			_this.parameters[ key ] = newValue
		}
	}
	// sanity check
	console.assert(this.parameters.renderer instanceof THREE.WebGLRenderer)
	console.assert(this.parameters.camera instanceof THREE.Camera)
	console.assert(this.parameters.scene instanceof THREE.Scene)
	

	// backward emulation
	Object.defineProperty(this, 'renderer', {get: function(){
		console.warn('use .parameters.renderer renderer')
		return this.parameters.renderer;
	}});
	Object.defineProperty(this, 'camera', {get: function(){
		console.warn('use .parameters.camera instead')
		return this.parameters.camera;
	}});
	Object.defineProperty(this, 'scene', {get: function(){
		console.warn('use .parameters.scene instead')
		return this.parameters.scene;
	}});

	
	// log the version
	console.log('AR.js', ARjs.Context.REVISION, '- trackingBackend:', parameters.contextParameters.trackingBackend)

	//////////////////////////////////////////////////////////////////////////////
	//		init arSource
	//////////////////////////////////////////////////////////////////////////////
	var arSource = _this.arSource = new ARjs.Source(parameters.sourceParameters)

	arSource.init(function onReady(){
		arSource.onResize(arContext, _this.parameters.renderer, _this.parameters.camera)
	})
	
	// handle resize
	window.addEventListener('resize', function(){
		arSource.onResize(arContext, _this.parameters.renderer, _this.parameters.camera)
	})	
	
	//////////////////////////////////////////////////////////////////////////////
	//		init arContext
	//////////////////////////////////////////////////////////////////////////////

	// create atToolkitContext
	var arContext = _this.arContext = new ARjs.Context(parameters.contextParameters)
	
	// initialize it
	_this.arContext.init()
	
	arContext.addEventListener('initialized', function(event){
		arSource.onResize(arContext, _this.parameters.renderer, _this.parameters.camera)
	})
	
	//////////////////////////////////////////////////////////////////////////////
	//		update function
	//////////////////////////////////////////////////////////////////////////////
	// update artoolkit on every frame
	this.update = function(){
		if( arSource.ready === false )	return
		
		arContext.update( arSource.domElement )
	}
}

ARjs.Session.prototype.onResize = function () {
	this.arSource.onResize(this.arContext, this.parameters.renderer, this.parameters.camera)	
};
// @namespace
var ARjs = ARjs || {}

ARjs.TangoPointCloud = function(arSession){
	var _this = this
	var arContext = arSession.arContext
	this.object3d = new THREE.Group

console.warn('Work only on cameraTransformMatrix - fix me - useless limitation')
	
	arContext.addEventListener('initialized', function(event){
	        var vrPointCloud = arContext._tangoContext.vrPointCloud
	        var geometry = vrPointCloud.getBufferGeometry()
	        var material = new THREE.PointsMaterial({
	                size: 0.01, 
        		// colorWrite: false, // good for occlusion
			depthWrite: false,
	        })
	        var pointsObject = new THREE.Points(geometry, material)
	        // Points are changing all the time so calculating the frustum culling volume is not very convenient.
	        pointsObject.frustumCulled = false;
	        pointsObject.renderDepth = 0;

		_this.object3d.add(pointsObject)
	})
}
// @namespace
var ARjs = ARjs || {}

ARjs.TangoVideoMesh = function(arSession){
	var arContext = arSession.arContext
	var renderer = arSession.renderer

	var videoMesh = null
	var vrDisplay = null

	// Create the see through camera scene and camera
	var sceneOrtho = new THREE.Scene()
	var cameraOrtho = new THREE.OrthographicCamera( -1, 1, 1, -1, 0, 100 )		
this._sceneOrtho = sceneOrtho
this._cameraOrtho = cameraOrtho

	// tango only - init cameraMesh
	arContext.addEventListener('initialized', function(event){
		// sanity check
		console.assert( arContext.parameters.trackingBackend === 'tango' )
		// variable declaration
		vrDisplay = arContext._tangoContext.vrDisplay
		console.assert(vrDisplay, 'vrDisplay MUST be defined')
		// if vrDisplay isnt for tango, do nothing. It may be another vrDisplay (e.g. webvr emulator in chrome)
		if( vrDisplay.displayName !== "Tango VR Device" )	return
		// init videoPlane
		videoMesh = THREE.WebAR.createVRSeeThroughCameraMesh(vrDisplay)
		sceneOrtho.add(videoMesh)
	})
	
	//////////////////////////////////////////////////////////////////////////////
	//		Code Separator
	//////////////////////////////////////////////////////////////////////////////
	
	this.update = function(){
		// sanity check
		console.assert( arContext.parameters.trackingBackend === 'tango' )
		// if not yet initialized, return now
		if( videoMesh === null )	return
		// Make sure that the camera is correctly displayed depending on the device and camera orientations.
		THREE.WebAR.updateCameraMeshOrientation(vrDisplay, videoMesh)                        		
	}
	
	//////////////////////////////////////////////////////////////////////////////
	//		Code Separator
	//////////////////////////////////////////////////////////////////////////////
	
	this.render = function(){
		// sanity check
		console.assert( arContext.parameters.trackingBackend === 'tango' )
		// render sceneOrtho
		renderer.render( sceneOrtho, cameraOrtho )
		// Render the perspective scene
		renderer.clearDepth()		
	}
}
var ARjs = ARjs || {}
ARjs.Utils = {}

/**
 * Create a default rendering camera for this trackingBackend. They may be modified later. to fit physical camera parameters
 * 
 * @param {string} trackingBackend - the tracking to user
 * @return {THREE.Camera} the created camera
 */
ARjs.Utils.createDefaultCamera = function(trackingMethod){
	var trackingBackend = this.parseTrackingMethod(trackingMethod).trackingBackend
	// Create a camera
	if( trackingBackend === 'artoolkit' ){
		var camera = new THREE.Camera();
	}else if( trackingBackend === 'aruco' ){
		var camera = new THREE.PerspectiveCamera(42, window.innerWidth / window.innerHeight, 0.01, 100);
	}else if( trackingBackend === 'tango' ){
		var camera = new THREE.PerspectiveCamera(42, window.innerWidth / window.innerHeight, 0.01, 100);
	}else console.assert(false, 'unknown trackingBackend: '+trackingBackend)

	return camera
}

/**
 * test if the code is running on tango
 * 
 * @return {boolean} - true if running on tango, false otherwise
 */
ARjs.Utils.isTango = function(){
	// FIXME: this test is super bad
	var isTango = navigator.userAgent.match('Chrome/57.0.2987.5') !== null ? true : false
	return isTango
}


/**
 * parse tracking method
 * 
 * @param {String} trackingMethod - the tracking method to parse
 * @return {Object} - various field of the tracking method
 */
ARjs.Utils.parseTrackingMethod = function(trackingMethod){

	if( trackingMethod === 'best' ){
		trackingMethod = ARjs.Utils.isTango() ? 'tango' : 'area-artoolkit'
	}	

	if( trackingMethod.startsWith('area-') ){
		return {
			trackingBackend : trackingMethod.replace('area-', ''),
			markersAreaEnabled : true,
		}
	}else{
		return {
			trackingBackend : trackingMethod,
			markersAreaEnabled : false,
		}
	}
}
var ARjs = ARjs || {}
var THREEx = THREEx || {}

ARjs.MarkersAreaControls = THREEx.ArMultiMarkerControls = function(arToolkitContext, object3d, parameters){
	var _this = this
	THREEx.ArBaseControls.call(this, object3d)
	
	if( arguments.length > 3 )	console.assert('wrong api for', THREEx.ArMultiMarkerControls)

// have a parameters in argument
	this.parameters = {
		// list of controls for each subMarker
		subMarkersControls: parameters.subMarkersControls,
		// list of pose for each subMarker relative to the origin
		subMarkerPoses: parameters.subMarkerPoses,
		// change matrix mode - [modelViewMatrix, cameraTransformMatrix]
		changeMatrixMode : parameters.changeMatrixMode !== undefined ? parameters.changeMatrixMode : 'modelViewMatrix',
	}
	
	this.object3d.visible = false
	// honor obsolete stuff - add a warning to use
	this.subMarkersControls = this.parameters.subMarkersControls
	this.subMarkerPoses = this.parameters.subMarkerPoses

	// listen to arToolkitContext event 'sourceProcessed'
	// - after we fully processed one image, aka when we know all detected poses in it
	arToolkitContext.addEventListener('sourceProcessed', function(){
		_this._onSourceProcessed()
	})
}

ARjs.MarkersAreaControls.prototype = Object.create( THREEx.ArBaseControls.prototype );
ARjs.MarkersAreaControls.prototype.constructor = ARjs.MarkersAreaControls;

//////////////////////////////////////////////////////////////////////////////
//		Code Separator
//////////////////////////////////////////////////////////////////////////////


/**
 * What to do when a image source is fully processed
 */
ARjs.MarkersAreaControls.prototype._onSourceProcessed = function(){
	var _this = this
	var stats = {
		count: 0,
		position : {
			sum: new THREE.Vector3(0,0,0),
			average: new THREE.Vector3(0,0,0),
		},
		quaternion : {
			sum: new THREE.Quaternion(0,0,0,0),
			average: new THREE.Quaternion(0,0,0,0),
		},
		scale : {
			sum: new THREE.Vector3(0,0,0),
			average: new THREE.Vector3(0,0,0),
		},
	}

	var firstQuaternion = _this.parameters.subMarkersControls[0].object3d.quaternion

	this.parameters.subMarkersControls.forEach(function(markerControls, markerIndex){
		
		var markerObject3d = markerControls.object3d
		// if this marker is not visible, ignore it
		if( markerObject3d.visible === false )	return

		// transformation matrix of this.object3d according to this sub-markers
		var matrix = markerObject3d.matrix.clone()
		var markerPose = _this.parameters.subMarkerPoses[markerIndex]
		matrix.multiply(new THREE.Matrix4().getInverse(markerPose))

		// decompose the matrix into .position, .quaternion, .scale
		var position = new THREE.Vector3
		var quaternion = new THREE.Quaternion()
		var scale = new THREE.Vector3
		matrix.decompose(position, quaternion, scale)

		// http://wiki.unity3d.com/index.php/Averaging_Quaternions_and_Vectors
		stats.count++

		ARjs.MarkersAreaControls.averageVector3(stats.position.sum, position, stats.count, stats.position.average)
		ARjs.MarkersAreaControls.averageQuaternion(stats.quaternion.sum, quaternion, firstQuaternion, stats.count, stats.quaternion.average)
		ARjs.MarkersAreaControls.averageVector3(stats.scale.sum, scale, stats.count, stats.scale.average)
	})

	// honor _this.object3d.visible
	if( stats.count > 0 ){
		_this.object3d.visible = true
	}else{
		_this.object3d.visible = false			
	}

	// if at least one sub-marker has been detected, make the average of all detected markers
	if( stats.count > 0 ){
		// compute modelViewMatrix
		var modelViewMatrix = new THREE.Matrix4()
		modelViewMatrix.compose(stats.position.average, stats.quaternion.average, stats.scale.average)

		// change _this.object3d.matrix based on parameters.changeMatrixMode
		if( this.parameters.changeMatrixMode === 'modelViewMatrix' ){
			_this.object3d.matrix.copy(modelViewMatrix)
		}else if( this.parameters.changeMatrixMode === 'cameraTransformMatrix' ){
			_this.object3d.matrix.getInverse( modelViewMatrix )
		}else {
			console.assert(false)
		}

		// decompose - the matrix into .position, .quaternion, .scale
		_this.object3d.matrix.decompose(_this.object3d.position, _this.object3d.quaternion, _this.object3d.scale)
	}

}

//////////////////////////////////////////////////////////////////////////////
//		Utility functions
//////////////////////////////////////////////////////////////////////////////

/**
 * from http://wiki.unity3d.com/index.php/Averaging_Quaternions_and_Vectors
 */
ARjs.MarkersAreaControls.averageQuaternion = function(quaternionSum, newQuaternion, firstQuaternion, count, quaternionAverage){
	quaternionAverage = quaternionAverage || new THREE.Quaternion()
	// sanity check
	console.assert(firstQuaternion instanceof THREE.Quaternion === true)
	
	// from http://wiki.unity3d.com/index.php/Averaging_Quaternions_and_Vectors
	if( newQuaternion.dot(firstQuaternion) > 0 ){
		newQuaternion = new THREE.Quaternion(-newQuaternion.x, -newQuaternion.y, -newQuaternion.z, -newQuaternion.w)
	}

	quaternionSum.x += newQuaternion.x
	quaternionSum.y += newQuaternion.y
	quaternionSum.z += newQuaternion.z
	quaternionSum.w += newQuaternion.w
	
	quaternionAverage.x = quaternionSum.x/count
	quaternionAverage.y = quaternionSum.y/count
	quaternionAverage.z = quaternionSum.z/count
	quaternionAverage.w = quaternionSum.w/count
	
	quaternionAverage.normalize()

	return quaternionAverage
}


ARjs.MarkersAreaControls.averageVector3 = function(vector3Sum, vector3, count, vector3Average){
	vector3Average = vector3Average || new THREE.Vector3()
	
	vector3Sum.x += vector3.x
	vector3Sum.y += vector3.y
	vector3Sum.z += vector3.z
	
	vector3Average.x = vector3Sum.x / count
	vector3Average.y = vector3Sum.y / count
	vector3Average.z = vector3Sum.z / count
	
	return vector3Average
}

//////////////////////////////////////////////////////////////////////////////
//		Utility function
//////////////////////////////////////////////////////////////////////////////

/**
 * compute the center of this multimarker file
 */
ARjs.MarkersAreaControls.computeCenter = function(jsonData){
	var multiMarkerFile = JSON.parse(jsonData)
	var stats = {
		count : 0,
		position : {
			sum: new THREE.Vector3(0,0,0),
			average: new THREE.Vector3(0,0,0),						
		},
		quaternion : {
			sum: new THREE.Quaternion(0,0,0,0),
			average: new THREE.Quaternion(0,0,0,0),						
		},
		scale : {
			sum: new THREE.Vector3(0,0,0),
			average: new THREE.Vector3(0,0,0),						
		},
	}
	var firstQuaternion = new THREE.Quaternion() // FIXME ???
	
	multiMarkerFile.subMarkersControls.forEach(function(item){
		var poseMatrix = new THREE.Matrix4().fromArray(item.poseMatrix)
		
		var position = new THREE.Vector3
		var quaternion = new THREE.Quaternion
		var scale = new THREE.Vector3
		poseMatrix.decompose(position, quaternion, scale)
		
		// http://wiki.unity3d.com/index.php/Averaging_Quaternions_and_Vectors
		stats.count++

		ARjs.MarkersAreaControls.averageVector3(stats.position.sum, position, stats.count, stats.position.average)
		ARjs.MarkersAreaControls.averageQuaternion(stats.quaternion.sum, quaternion, firstQuaternion, stats.count, stats.quaternion.average)
		ARjs.MarkersAreaControls.averageVector3(stats.scale.sum, scale, stats.count, stats.scale.average)
	})
	
	var averageMatrix = new THREE.Matrix4()
	averageMatrix.compose(stats.position.average, stats.quaternion.average, stats.scale.average)

	return averageMatrix
}

ARjs.MarkersAreaControls.computeBoundingBox = function(jsonData){
	var multiMarkerFile = JSON.parse(jsonData)
	var boundingBox = new THREE.Box3()

	multiMarkerFile.subMarkersControls.forEach(function(item){
		var poseMatrix = new THREE.Matrix4().fromArray(item.poseMatrix)
		
		var position = new THREE.Vector3
		var quaternion = new THREE.Quaternion
		var scale = new THREE.Vector3
		poseMatrix.decompose(position, quaternion, scale)

		boundingBox.expandByPoint(position)
	})

	return boundingBox
}
//////////////////////////////////////////////////////////////////////////////
//		updateSmoothedControls
//////////////////////////////////////////////////////////////////////////////

ARjs.MarkersAreaControls.prototype.updateSmoothedControls = function(smoothedControls, lerpsValues){
	// handle default values
	if( lerpsValues === undefined ){
		// FIXME this parameter format is uselessly cryptic
		// lerpValues = [
		// {lerpPosition: 0.5, lerpQuaternion: 0.2, lerpQuaternion: 0.7}
		// ]
		lerpsValues = [
			[0.3+.1, 0.1, 0.3],
			[0.4+.1, 0.1, 0.4],
			[0.4+.1, 0.2, 0.5],
			[0.5+.1, 0.2, 0.7],
			[0.5+.1, 0.2, 0.7],
		]
	}
	// count how many subMarkersControls are visible
	var nVisible = 0
	this.parameters.subMarkersControls.forEach(function(markerControls, markerIndex){
		var markerObject3d = markerControls.object3d
		if( markerObject3d.visible === true )	nVisible ++
	})

	// find the good lerpValues
	if( lerpsValues[nVisible-1] !== undefined ){
		var lerpValues = lerpsValues[nVisible-1]
	}else{
		var lerpValues = lerpsValues[lerpsValues.length-1]
	}

	// modify lerpValues in smoothedControls
	smoothedControls.parameters.lerpPosition = lerpValues[0]
	smoothedControls.parameters.lerpQuaternion = lerpValues[1]
	smoothedControls.parameters.lerpScale = lerpValues[2]
}


//////////////////////////////////////////////////////////////////////////////
//		Create THREEx.ArMultiMarkerControls from JSON
//////////////////////////////////////////////////////////////////////////////

ARjs.MarkersAreaControls.fromJSON = function(arToolkitContext, parent3D, markerRoot, jsonData, parameters){
	var multiMarkerFile = JSON.parse(jsonData)
	// declare variables
	var subMarkersControls = []
	var subMarkerPoses = []
	// handle default arguments
	parameters = parameters || {}

	// prepare the parameters
	multiMarkerFile.subMarkersControls.forEach(function(item){
		// create a markerRoot
		var markerRoot = new THREE.Object3D()
		parent3D.add(markerRoot)

		// create markerControls for our markerRoot
		var subMarkerControls = new THREEx.ArMarkerControls(arToolkitContext, markerRoot, item.parameters)

// if( true ){
		// store it in the parameters
		subMarkersControls.push(subMarkerControls)
		subMarkerPoses.push(new THREE.Matrix4().fromArray(item.poseMatrix))	
// }else{
// 		// build a smoothedControls
// 		var smoothedRoot = new THREE.Group()
// 		parent3D.add(smoothedRoot)
// 		var smoothedControls = new THREEx.ArSmoothedControls(smoothedRoot, {
// 			lerpPosition : 0.1,
// 			lerpQuaternion : 0.1, 
// 			lerpScale : 0.1,
// 			minVisibleDelay: 0,
// 			minUnvisibleDelay: 0,
// 		})
// 		onRenderFcts.push(function(delta){
// 			smoothedControls.update(markerRoot)	// TODO this is a global
// 		})
// 	
// 
// 		// store it in the parameters
// 		subMarkersControls.push(smoothedControls)
// 		subMarkerPoses.push(new THREE.Matrix4().fromArray(item.poseMatrix))
// }
	})
	
	parameters.subMarkersControls = subMarkersControls
	parameters.subMarkerPoses = subMarkerPoses
	// create a new THREEx.ArMultiMarkerControls
	var multiMarkerControls = new THREEx.ArMultiMarkerControls(arToolkitContext, markerRoot, parameters)

	// return it
	return multiMarkerControls	
}
var ARjs = ARjs || {}
var THREEx = THREEx || {}

ARjs.MarkersAreaLearning = THREEx.ArMultiMakersLearning = function(arToolkitContext, subMarkersControls){
	var _this = this
	this._arToolkitContext = arToolkitContext

	// Init variables
	this.subMarkersControls = subMarkersControls
	this.enabled = true
		
	// listen to arToolkitContext event 'sourceProcessed'
	// - after we fully processed one image, aka when we know all detected poses in it
	arToolkitContext.addEventListener('sourceProcessed', function(){
		_this._onSourceProcessed()
	})
}


//////////////////////////////////////////////////////////////////////////////
//		statistic collection
//////////////////////////////////////////////////////////////////////////////

/**
 * What to do when a image source is fully processed
 */
ARjs.MarkersAreaLearning.prototype._onSourceProcessed = function(){
	var originQuaternion = this.subMarkersControls[0].object3d.quaternion
	// here collect the statistic on relative positioning 
	
	// honor this.enabled
	if( this.enabled === false )	return

	// keep only the visible markers
	var visibleMarkerControls = this.subMarkersControls.filter(function(markerControls){
		return markerControls.object3d.visible === true
	})

	var count = Object.keys(visibleMarkerControls).length

	var positionDelta = new THREE.Vector3()
	var quaternionDelta = new THREE.Quaternion()
	var scaleDelta = new THREE.Vector3()
	var tmpMatrix = new THREE.Matrix4()
	
	// go thru all the visibleMarkerControls
	for(var i = 0; i < count; i++){
		var markerControls1 = visibleMarkerControls[i]
		for(var j = 0; j < count; j++){
			var markerControls2 = visibleMarkerControls[j]

			// if markerControls1 is markerControls2, then skip it
			if( i === j )	continue


			//////////////////////////////////////////////////////////////////////////////
			//		create data in markerControls1.object3d.userData if needed
			//////////////////////////////////////////////////////////////////////////////
			// create seenCouples for markerControls1 if needed
			if( markerControls1.object3d.userData.seenCouples === undefined ){
				markerControls1.object3d.userData.seenCouples = {}
			}
			var seenCouples = markerControls1.object3d.userData.seenCouples
			// create the multiMarkerPosition average if needed`
			if( seenCouples[markerControls2.id] === undefined ){
				// console.log('create seenCouples between', markerControls1.id, 'and', markerControls2.id)
				seenCouples[markerControls2.id] = {
					count : 0,
					position : {
						sum: new THREE.Vector3(0,0,0),
						average: new THREE.Vector3(0,0,0),						
					},
					quaternion : {
						sum: new THREE.Quaternion(0,0,0,0),
						average: new THREE.Quaternion(0,0,0,0),						
					},
					scale : {
						sum: new THREE.Vector3(0,0,0),
						average: new THREE.Vector3(0,0,0),						
					},
				}
			}

			
			//////////////////////////////////////////////////////////////////////////////
			//		Compute markerControls2 position relative to markerControls1
			//////////////////////////////////////////////////////////////////////////////
			
			// compute markerControls2 position/quaternion/scale in relation with markerControls1
			tmpMatrix.getInverse(markerControls1.object3d.matrix)
			tmpMatrix.multiply(markerControls2.object3d.matrix)
			tmpMatrix.decompose(positionDelta, quaternionDelta, scaleDelta)
			
			//////////////////////////////////////////////////////////////////////////////
			//		update statistics
			//////////////////////////////////////////////////////////////////////////////
			var stats = seenCouples[markerControls2.id]
			// update the count
			stats.count++

			// update the average of position/rotation/scale
			THREEx.ArMultiMarkerControls.averageVector3(stats.position.sum, positionDelta, stats.count, stats.position.average)
			THREEx.ArMultiMarkerControls.averageQuaternion(stats.quaternion.sum, quaternionDelta, originQuaternion, stats.count, stats.quaternion.average)
			THREEx.ArMultiMarkerControls.averageVector3(stats.scale.sum, scaleDelta, stats.count, stats.scale.average)
		}
	}
}

//////////////////////////////////////////////////////////////////////////////
//		Compute markers transformation matrix from current stats
//////////////////////////////////////////////////////////////////////////////

ARjs.MarkersAreaLearning.prototype.computeResult = function(){
	var _this = this
	var originSubControls = this.subMarkersControls[0]

	this.deleteResult()

	// special case of originSubControls averageMatrix
	originSubControls.object3d.userData.result = {
		averageMatrix : new THREE.Matrix4(),
		confidenceFactor: 1,
	}
	// TODO here check if the originSubControls has been seen at least once!!
	
	
	/**
	 * ALGO in pseudo code
	 *
	 * - Set confidenceFactor of origin sub markers as 1
	 *
	 * Start Looping
	 * - For a given sub marker, skip it if it already has a result.
	 * - if no result, check all seen couple and find n ones which has a progress of 1 or more.
	 * - So the other seen sub markers, got a valid transformation matrix. 
	 * - So take local averages position/orientation/scale, compose a transformation matrix. 
	 *   - aka transformation matrix from parent matrix * transf matrix pos/orientation/scale
	 * - Multiple it by the other seen marker matrix. 
	 * - Loop on the array until one pass could not compute any new sub marker
	 */
	
	do{
		var resultChanged = false
		// loop over each subMarkerControls
		this.subMarkersControls.forEach(function(subMarkerControls){

			// if subMarkerControls already has a result, do nothing
			var result = subMarkerControls.object3d.userData.result
			var isLearned = (result !== undefined && result.confidenceFactor >= 1) ? true : false
			if( isLearned === true )	return
			
			// console.log('compute subMarkerControls', subMarkerControls.name())
			var otherSubControlsID = _this._getLearnedCoupleStats(subMarkerControls)
			if( otherSubControlsID === null ){
				// console.log('no learnedCoupleStats')
				return
			}
			
			var otherSubControls = _this._getSubMarkerControlsByID(otherSubControlsID)

			var seenCoupleStats = subMarkerControls.object3d.userData.seenCouples[otherSubControlsID]
			
			var averageMatrix = new THREE.Matrix4()
			averageMatrix.compose(seenCoupleStats.position.average, seenCoupleStats.quaternion.average, seenCoupleStats.scale.average)
				
			var otherAverageMatrix = otherSubControls.object3d.userData.result.averageMatrix

			var matrix = new THREE.Matrix4().getInverse(otherAverageMatrix).multiply(averageMatrix)
			matrix = new THREE.Matrix4().getInverse(matrix)

			console.assert( subMarkerControls.object3d.userData.result === undefined )
			subMarkerControls.object3d.userData.result = {
				averageMatrix: matrix,
				confidenceFactor: 1
			}
			
			resultChanged = true
		})
		// console.log('loop')
	}while(resultChanged === true)
	
	// debugger
	// console.log('json:', this.toJSON())
	// this.subMarkersControls.forEach(function(subMarkerControls){
	// 	var hasResult = subMarkerControls.object3d.userData.result !== undefined
	// 	console.log('marker', subMarkerControls.name(), hasResult ? 'has' : 'has NO', 'result')
	// })
}

//////////////////////////////////////////////////////////////////////////////
//		Utility function
//////////////////////////////////////////////////////////////////////////////

/** 
 * get a _this.subMarkersControls id based on markerControls.id
 */
ARjs.MarkersAreaLearning.prototype._getLearnedCoupleStats	= function(subMarkerControls){

	// if this subMarkerControls has never been seen with another subMarkerControls
	if( subMarkerControls.object3d.userData.seenCouples === undefined )	return null
	
	var seenCouples = subMarkerControls.object3d.userData.seenCouples
	var coupleControlsIDs = Object.keys(seenCouples).map(Number)

	for(var i = 0; i < coupleControlsIDs.length; i++){
		var otherSubControlsID = coupleControlsIDs[i]
		// get otherSubControls
		var otherSubControls = this._getSubMarkerControlsByID(otherSubControlsID)
			
		// if otherSubControls isnt learned, skip it
		var result = otherSubControls.object3d.userData.result
		var isLearned = (result !== undefined && result.confidenceFactor >= 1) ? true : false
		if( isLearned === false )	continue

		// return this seenCouplesStats
		return otherSubControlsID
	}
	
	// if none is found, return null
	return null
}

/** 
 * get a _this.subMarkersControls based on markerControls.id
 */
ARjs.MarkersAreaLearning.prototype._getSubMarkerControlsByID	= function(controlsID){

	for(var i = 0; i < this.subMarkersControls.length; i++){
		var subMarkerControls = this.subMarkersControls[i]
		if( subMarkerControls.id === controlsID ){
			return subMarkerControls
		}
	}

	return null
}
 //////////////////////////////////////////////////////////////////////////////
//		JSON file building
//////////////////////////////////////////////////////////////////////////////

ARjs.MarkersAreaLearning.prototype.toJSON = function(){

	// compute the average matrix before generating the file
	this.computeResult()

	//////////////////////////////////////////////////////////////////////////////
	//		actually build the json
	//////////////////////////////////////////////////////////////////////////////
	var data = {
		meta : {
			createdBy : "Area Learning - AR.js "+THREEx.ArToolkitContext.REVISION,
			createdAt : new Date().toJSON(),
			
		},
		trackingBackend: this._arToolkitContext.parameters.trackingBackend,
		subMarkersControls : [],
	}

	var originSubControls = this.subMarkersControls[0]
	var originMatrixInverse = new THREE.Matrix4().getInverse(originSubControls.object3d.matrix)
	this.subMarkersControls.forEach(function(subMarkerControls, index){
		
		// if a subMarkerControls has no result, ignore it
		if( subMarkerControls.object3d.userData.result === undefined )	return

		var poseMatrix = subMarkerControls.object3d.userData.result.averageMatrix
		console.assert(poseMatrix instanceof THREE.Matrix4)
		

		// build the info
		var info = {
			parameters : {
				// to fill ...
			},
			poseMatrix : poseMatrix.toArray(),
		}
		if( subMarkerControls.parameters.type === 'pattern' ){
			info.parameters.type = subMarkerControls.parameters.type
			info.parameters.patternUrl = subMarkerControls.parameters.patternUrl
		}else if( subMarkerControls.parameters.type === 'barcode' ){
			info.parameters.type = subMarkerControls.parameters.type
			info.parameters.barcodeValue = subMarkerControls.parameters.barcodeValue
		}else console.assert(false)

		data.subMarkersControls.push(info)
	})

	var strJSON = JSON.stringify(data, null, '\t');
	
	
	//////////////////////////////////////////////////////////////////////////////
	//		round matrix elements to ease readability - for debug
	//////////////////////////////////////////////////////////////////////////////
	var humanReadable = false
	if( humanReadable === true ){
		var tmp = JSON.parse(strJSON)
		tmp.subMarkersControls.forEach(function(markerControls){
			markerControls.poseMatrix = markerControls.poseMatrix.map(function(value){
				var roundingFactor = 100
				return Math.round(value*roundingFactor)/roundingFactor
			})
		})
		strJSON = JSON.stringify(tmp, null, '\t');
	}
	
	return strJSON;	
}

//////////////////////////////////////////////////////////////////////////////
//		utility function
//////////////////////////////////////////////////////////////////////////////

/**
 * reset all collected statistics
 */
ARjs.MarkersAreaLearning.prototype.resetStats = function(){
	this.deleteResult()
	
	this.subMarkersControls.forEach(function(markerControls){
		delete markerControls.object3d.userData.seenCouples
	})
}
/**
 * reset all collected statistics
 */
ARjs.MarkersAreaLearning.prototype.deleteResult = function(){
	this.subMarkersControls.forEach(function(markerControls){
		delete markerControls.object3d.userData.result
	})
}
var THREEx = THREEx || {}

var ARjs = ARjs || {}
var THREEx = THREEx || {}

ARjs.MarkersAreaUtils = THREEx.ArMultiMarkerUtils = {}

//////////////////////////////////////////////////////////////////////////////
//		navigateToLearnerPage
//////////////////////////////////////////////////////////////////////////////

/**
 * Navigate to the multi-marker learner page
 * 
 * @param {String} learnerBaseURL  - the base url for the learner
 * @param {String} trackingBackend - the tracking backend to use
 */
ARjs.MarkersAreaUtils.navigateToLearnerPage = function(learnerBaseURL, trackingBackend){
	var learnerParameters = {
		backURL : location.href,
		trackingBackend: trackingBackend,
		markersControlsParameters: ARjs.MarkersAreaUtils.createDefaultMarkersControlsParameters(trackingBackend),
	}
	location.href = learnerBaseURL + '#' + encodeURIComponent(JSON.stringify(learnerParameters))
}

//////////////////////////////////////////////////////////////////////////////
//		DefaultMultiMarkerFile
//////////////////////////////////////////////////////////////////////////////

/**
 * Create and store a default multi-marker file
 * 
 * @param {String} trackingBackend - the tracking backend to use
 */
ARjs.MarkersAreaUtils.storeDefaultMultiMarkerFile = function(trackingBackend){
	var file = ARjs.MarkersAreaUtils.createDefaultMultiMarkerFile(trackingBackend)
	// json.strinfy the value and store it in localStorage
	localStorage.setItem('ARjsMultiMarkerFile', JSON.stringify(file))
}



/**
 * Create a default multi-marker file
 * @param {String} trackingBackend - the tracking backend to use
 * @return {Object} - json object of the multi-marker file
 */
ARjs.MarkersAreaUtils.createDefaultMultiMarkerFile = function(trackingBackend){
	console.assert(trackingBackend)
	if( trackingBackend === undefined )	debugger
	
	// create absoluteBaseURL
	var link = document.createElement('a')
	link.href = ARjs.Context.baseURL
	var absoluteBaseURL = link.href

	// create the base file
	var file = {
		meta : {
			createdBy : "AR.js Default Marker "+ARjs.Context.REVISION,
			createdAt : new Date().toJSON(),
		},
		trackingBackend : trackingBackend,
		subMarkersControls : [
			// empty for now... being filled 
		]
	}
	// add a subMarkersControls
	file.subMarkersControls[0] = {
		parameters: {},
		poseMatrix: new THREE.Matrix4().makeTranslation(0,0, 0).toArray(),
	}
	if( trackingBackend === 'artoolkit' ){
		file.subMarkersControls[0].parameters.type = 'pattern'
		file.subMarkersControls[0].parameters.patternUrl = absoluteBaseURL + 'examples/marker-training/examples/pattern-files/pattern-hiro.patt'
	}else if( trackingBackend === 'aruco' ){
		file.subMarkersControls[0].parameters.type = 'barcode'
		file.subMarkersControls[0].parameters.barcodeValue = 1001
	}else console.assert(false)
	
	// json.strinfy the value and store it in localStorage
	return file
}

//////////////////////////////////////////////////////////////////////////////
//		createDefaultMarkersControlsParameters
//////////////////////////////////////////////////////////////////////////////

/**
 * Create a default controls parameters for the multi-marker learner
 * 
 * @param {String} trackingBackend - the tracking backend to use
 * @return {Object} - json object containing the controls parameters
 */
ARjs.MarkersAreaUtils.createDefaultMarkersControlsParameters = function(trackingBackend){
	// create absoluteBaseURL
	var link = document.createElement('a')
	link.href = ARjs.Context.baseURL
	var absoluteBaseURL = link.href


	if( trackingBackend === 'artoolkit' ){
		// pattern hiro/kanji/a/b/c/f
		var markersControlsParameters = [
			{
				type : 'pattern',
				patternUrl : absoluteBaseURL + 'examples/marker-training/examples/pattern-files/pattern-hiro.patt',
			},
			{
				type : 'pattern',
				patternUrl : absoluteBaseURL + 'examples/marker-training/examples/pattern-files/pattern-kanji.patt',
			},
			{
				type : 'pattern',
				patternUrl : absoluteBaseURL + 'examples/marker-training/examples/pattern-files/pattern-letterA.patt',
			},
			{
				type : 'pattern',
				patternUrl : absoluteBaseURL + 'examples/marker-training/examples/pattern-files/pattern-letterB.patt',
			},
			{
				type : 'pattern',
				patternUrl : absoluteBaseURL + 'examples/marker-training/examples/pattern-files/pattern-letterC.patt',
			},
			{
				type : 'pattern',
				patternUrl : absoluteBaseURL + 'examples/marker-training/examples/pattern-files/pattern-letterF.patt',
			},
		]		
	}else if( trackingBackend === 'aruco' ){
		var markersControlsParameters = [
			{
				type : 'barcode',
				barcodeValue: 1001,
			},
			{
				type : 'barcode',
				barcodeValue: 1002,
			},
			{
				type : 'barcode',
				barcodeValue: 1003,
			},
			{
				type : 'barcode',
				barcodeValue: 1004,
			},
			{
				type : 'barcode',
				barcodeValue: 1005,
			},
			{
				type : 'barcode',
				barcodeValue: 1006,
			},
		]
	}else console.assert(false)
	return markersControlsParameters
}
//////////////////////////////////////////////////////////////////////////////
//		arjs-anchor
//////////////////////////////////////////////////////////////////////////////
AFRAME.registerComponent('arjs-anchor', {
	dependencies: ['arjs', 'artoolkit'],
	schema: {
		preset: {
			type: 'string',
		},
		markerhelpers : {	// IIF preset === 'area'
			type: 'boolean',
			default: false,
		},

		// controls parameters
		size: {
			type: 'number',
			default: 1
		},
		type: {
			type: 'string',
		},
		patternUrl: {
			type: 'string',
		},
		barcodeValue: {
			type: 'number'
		},
		changeMatrixMode: {
			type: 'string',
			default : 'modelViewMatrix',
		},
		minConfidence: {
			type: 'number',
			default: 0.6,
		},
	},
	init: function () {
		var _this = this

		// get arjsSystem
		var arjsSystem = this.el.sceneEl.systems.arjs || this.el.sceneEl.systems.artoolkit

		//////////////////////////////////////////////////////////////////////////////
		//		Code Separator
		//////////////////////////////////////////////////////////////////////////////

		_this.isReady = false
		_this._arAnchor = null

		// honor object visibility
		if( _this.data.changeMatrixMode === 'modelViewMatrix' ){
			_this.el.object3D.visible = false
		}else if( _this.data.changeMatrixMode === 'cameraTransformMatrix' ){
 			_this.el.sceneEl.object3D.visible = false
		}else console.assert(false)

		// trick to wait until arjsSystem is isReady
		var startedAt = Date.now()
		var timerId = setInterval(function(){
			// wait until the system is isReady
			if( arjsSystem.isReady === false )	return

			clearInterval(timerId)

			//////////////////////////////////////////////////////////////////////////////
			//		update arProfile
			//////////////////////////////////////////////////////////////////////////////
			var arProfile = arjsSystem._arProfile
			
			// arProfile.changeMatrixMode('modelViewMatrix')
			arProfile.changeMatrixMode(_this.data.changeMatrixMode)

			// honor this.data.preset
			if( _this.data.preset === 'hiro' ){
				arProfile.defaultMarkerParameters.type = 'pattern'
				arProfile.defaultMarkerParameters.patternUrl = THREEx.ArToolkitContext.baseURL+'examples/marker-training/examples/pattern-files/pattern-hiro.patt'
				arProfile.defaultMarkerParameters.markersAreaEnabled = false
			}else if( _this.data.preset === 'kanji' ){
				arProfile.defaultMarkerParameters.type = 'pattern'
				arProfile.defaultMarkerParameters.patternUrl = THREEx.ArToolkitContext.baseURL+'examples/marker-training/examples/pattern-files/pattern-kanji.patt'
				arProfile.defaultMarkerParameters.markersAreaEnabled = false
			}else if( _this.data.preset === 'area' ){
				arProfile.defaultMarkerParameters.type = 'barcode'
				arProfile.defaultMarkerParameters.barcodeValue = 1001	
				arProfile.defaultMarkerParameters.markersAreaEnabled = true
			}else {
				arProfile.defaultMarkerParameters.type = _this.data.type;
				arProfile.defaultMarkerParameters.patternUrl = _this.data.patternUrl;
				arProfile.defaultMarkerParameters.markersAreaEnabled = false
			}		

			//////////////////////////////////////////////////////////////////////////////
			//		create arAnchor
			//////////////////////////////////////////////////////////////////////////////
			
			var arSession = arjsSystem._arSession
			var arAnchor = _this._arAnchor = new ARjs.Anchor(arSession, arProfile.defaultMarkerParameters)

			// it is now considered isReady
			_this.isReady = true

			//////////////////////////////////////////////////////////////////////////////
			//		honor .debugUIEnabled
			//////////////////////////////////////////////////////////////////////////////
			if( arjsSystem.data.debugUIEnabled ){
				// get or create containerElement
				var containerElement = document.querySelector('#arjsDebugUIContainer')
				if( containerElement === null ){
					containerElement = document.createElement('div')
					containerElement.id = 'arjsDebugUIContainer'
					containerElement.setAttribute('style', 'position: fixed; bottom: 10px; width:100%; text-align: center; z-index: 1; color: grey;')
					document.body.appendChild(containerElement)
				}
				// create anchorDebugUI
				var anchorDebugUI = new ARjs.AnchorDebugUI(arAnchor)
				containerElement.appendChild(anchorDebugUI.domElement)		
			}
		}, 1000/60)
	},
	remove : function(){
	},
	update: function () {
	},
	tick: function(){
		var _this = this
		// if not yet isReady, do nothing
		if( this.isReady === false )	return

		//////////////////////////////////////////////////////////////////////////////
		//		update arAnchor
		//////////////////////////////////////////////////////////////////////////////
		var arjsSystem = this.el.sceneEl.systems.arjs || this.el.sceneEl.systems.artoolkit
		this._arAnchor.update()

		//////////////////////////////////////////////////////////////////////////////
		//		honor pose
		//////////////////////////////////////////////////////////////////////////////
		var arWorldRoot = this._arAnchor.object3d
		arWorldRoot.updateMatrixWorld(true)		
		arWorldRoot.matrixWorld.decompose(this.el.object3D.position, this.el.object3D.quaternion, this.el.object3D.scale)

		//////////////////////////////////////////////////////////////////////////////
		//		honor visibility
		//////////////////////////////////////////////////////////////////////////////
		if( _this._arAnchor.parameters.changeMatrixMode === 'modelViewMatrix' ){
			_this.el.object3D.visible = this._arAnchor.object3d.visible
		}else if( _this._arAnchor.parameters.changeMatrixMode === 'cameraTransformMatrix' ){
			_this.el.sceneEl.object3D.visible = this._arAnchor.object3d.visible
		}else console.assert(false)
	}
})

//////////////////////////////////////////////////////////////////////////////
//                define some primitives shortcuts
//////////////////////////////////////////////////////////////////////////////

AFRAME.registerPrimitive('a-anchor', AFRAME.utils.extendDeep({}, AFRAME.primitives.getMeshMixin(), {
	defaultComponents: {
		'arjs-anchor': {},
		'arjs-hit-testing': {},
	},
	mappings: {
		'type': 'arjs-anchor.type',
		'size': 'arjs-anchor.size',
		'url': 'arjs-anchor.patternUrl',
		'value': 'arjs-anchor.barcodeValue',
		'preset': 'arjs-anchor.preset',
		'minConfidence': 'arjs-anchor.minConfidence',
		'markerhelpers': 'arjs-anchor.markerhelpers',

		'hit-testing-renderDebug': 'arjs-hit-testing.renderDebug',
		'hit-testing-enabled': 'arjs-hit-testing.enabled',
	}
}))



AFRAME.registerPrimitive('a-camera-static', AFRAME.utils.extendDeep({}, AFRAME.primitives.getMeshMixin(), {
	defaultComponents: {
		'camera': {},
	},
	mappings: {
	}
}))

//////////////////////////////////////////////////////////////////////////////
//		backward compatibility
//////////////////////////////////////////////////////////////////////////////
// FIXME 
AFRAME.registerPrimitive('a-marker', AFRAME.utils.extendDeep({}, AFRAME.primitives.getMeshMixin(), {
	defaultComponents: {
		'arjs-anchor': {},
		'arjs-hit-testing': {},
	},
	mappings: {
		'type': 'arjs-anchor.type',
		'size': 'arjs-anchor.size',
		'url': 'arjs-anchor.patternUrl',
		'value': 'arjs-anchor.barcodeValue',
		'preset': 'arjs-anchor.preset',
		'minConfidence': 'arjs-anchor.minConfidence',
		'markerhelpers': 'arjs-anchor.markerhelpers',

		'hit-testing-renderDebug': 'arjs-hit-testing.renderDebug',
		'hit-testing-enabled': 'arjs-hit-testing.enabled',
	}
}))

AFRAME.registerPrimitive('a-marker-camera', AFRAME.utils.extendDeep({}, AFRAME.primitives.getMeshMixin(), {
	defaultComponents: {
		'arjs-anchor': {
			changeMatrixMode: 'cameraTransformMatrix'
		},
		'camera': {},
	},
	mappings: {
		'type': 'arjs-anchor.type',
		'size': 'arjs-anchor.size',
		'url': 'arjs-anchor.patternUrl',
		'value': 'arjs-anchor.barcodeValue',
		'preset': 'arjs-anchor.preset',
		'minConfidence': 'arjs-anchor.minConfidence',
		'markerhelpers': 'arjs-anchor.markerhelpers',
	}
}))
//////////////////////////////////////////////////////////////////////////////
//		arjs-hit-testing
//////////////////////////////////////////////////////////////////////////////
AFRAME.registerComponent('arjs-hit-testing', {
	dependencies: ['arjs', 'artoolkit'],
	schema: {
		enabled : {
			type: 'boolean',
			default: false,
		},
		renderDebug : {
			type: 'boolean',
			default: false,
		},
	},
	init: function () {
		var _this = this
		var arjsSystem = this.el.sceneEl.systems.arjs || this.el.sceneEl.systems.artoolkit

// TODO make it work on cameraTransformMatrix too
// 
		_this.isReady = false
		_this._arAnchor = null
		_this._arHitTesting = null

		// trick to wait until arjsSystem is isReady
		var startedAt = Date.now()
		var timerId = setInterval(function(){
			var anchorEl = _this.el
			var anchorComponent = anchorEl.components['arjs-anchor']
			// wait until anchorComponent is isReady
			if( anchorComponent === undefined || anchorComponent.isReady === false )	return

			clearInterval(timerId)

			//////////////////////////////////////////////////////////////////////////////
			//		create arAnchor
			//////////////////////////////////////////////////////////////////////////////
			var arAnchor = anchorComponent._arAnchor
			var arSession = arjsSystem._arSession
			var renderer = arSession.parameters.renderer

			var hitTesting = _this._arHitTesting = new ARjs.HitTesting(arSession)
			hitTesting.enabled = _this.data.enabled
			
			// tango only - picking to set object position
			renderer.domElement.addEventListener("click", function(domEvent){
				var hitTestResults = hitTesting.testDomEvent(domEvent)
				if( hitTestResults.length === 0 )	return

				var hitTestResult = hitTestResults[0]
				hitTestResult.apply(arAnchor.object3d)
			})
			
			_this.isReady = true
		}, 1000/60)
	},
	remove : function(){
	},
	update: function () {
	},
	tick: function(){
		var _this = this
		// if not yet isReady, do nothing
		if( this.isReady === false )	return

		var arjsSystem = this.el.sceneEl.systems.arjs || this.el.sceneEl.systems.artoolkit
		var arSession = arjsSystem._arSession

		var anchorEl = _this.el
		var anchorComponent = anchorEl.components['arjs-anchor']
		var arAnchor = anchorComponent._arAnchor
		

		var hitTesting = this._arHitTesting
		var camera = arSession.parameters.camera
// console.log(camera.position)
		hitTesting.update(camera, arAnchor.object3d, arAnchor.parameters.changeMatrixMode)
	}
});
AFRAME.registerSystem('arjs', {
	schema: {
		trackingMethod : {
			type: 'string',	
			default: 'best',			
		},
		debugUIEnabled :{
			type: 'boolean',	
			default: true,			
		},
		areaLearningButton : {
			type: 'boolean',	
			default: true,
		},
		performanceProfile : {
			type: 'string',	
			default: 'default',
		},
		
		tangoPointCloudEnabled : {
			type: 'boolean',
			default: false,			
		},

		// old parameters
		debug : {
			type: 'boolean',
			default: false
		},
		detectionMode : {
			type: 'string',
			default: '',
		},
		matrixCodeType : {
			type: 'string',
			default: '',
		},
		cameraParametersUrl : {
			type: 'string',
			default: '',
		},
		maxDetectionRate : {
			type: 'number',
			default: -1
		},
		sourceType : {
			type: 'string',
			default: '',
		},
		sourceUrl : {
			type: 'string',
			default: '',
		},
		sourceWidth : {
			type: 'number',
			default: -1
		},
		sourceHeight : {
			type: 'number',
			default: -1
		},
		displayWidth : {
			type: 'number',
			default: -1
		},
		displayHeight : {
			type: 'number',
			default: -1
		},
		canvasWidth : {
			type: 'number',
			default: -1
		},
		canvasHeight : {
			type: 'number',
			default: -1
		},
	},
	
	//////////////////////////////////////////////////////////////////////////////
	//		Code Separator
	//////////////////////////////////////////////////////////////////////////////
	
	
	init: function () {
		var _this = this
		
		
		//////////////////////////////////////////////////////////////////////////////
		//		setup arProfile
		//////////////////////////////////////////////////////////////////////////////

		var arProfile = this._arProfile = new ARjs.Profile()
			.trackingMethod(this.data.trackingMethod)
			.performance(this.data.performanceProfile)
			.defaultMarker()



		//////////////////////////////////////////////////////////////////////////////
		//		honor this.data and setup arProfile with it
		//////////////////////////////////////////////////////////////////////////////

		// honor this.data and push what has been modified into arProfile
		if( this.data.debug !== false )			arProfile.contextParameters.debug = this.data.debug
		if( this.data.detectionMode !== '' )		arProfile.contextParameters.detectionMode = this.data.detectionMode
		if( this.data.matrixCodeType !== '' )		arProfile.contextParameters.matrixCodeType = this.data.matrixCodeType
		if( this.data.cameraParametersUrl !== '' )	arProfile.contextParameters.cameraParametersUrl = this.data.cameraParametersUrl
		if( this.data.maxDetectionRate !== -1 )		arProfile.contextParameters.maxDetectionRate = this.data.maxDetectionRate
		if( this.data.canvasWidth !== -1 )		arProfile.contextParameters.canvasWidth = this.data.canvasWidth
		if( this.data.canvasHeight !== -1 )		arProfile.contextParameters.canvasHeight = this.data.canvasHeight

		if( this.data.sourceType !== '' )		arProfile.sourceParameters.sourceType = this.data.sourceType
		if( this.data.sourceUrl !== '' )		arProfile.sourceParameters.sourceUrl = this.data.sourceUrl
		if( this.data.sourceWidth !== -1 )		arProfile.sourceParameters.sourceWidth = this.data.sourceWidth
		if( this.data.sourceHeight !== -1 )		arProfile.sourceParameters.sourceHeight = this.data.sourceHeight
		if( this.data.displayWidth !== -1 )		arProfile.sourceParameters.displayWidth = this.data.displayWidth
		if( this.data.displayHeight !== -1 )		arProfile.sourceParameters.displayHeight = this.data.displayHeight

		arProfile.checkIfValid()

		//////////////////////////////////////////////////////////////////////////////
		//		Code Separator
		//////////////////////////////////////////////////////////////////////////////

		this._arSession = null

		_this.isReady = false
		_this.needsOverride = true

		// wait until the renderer is isReady
		this.el.sceneEl.addEventListener('renderstart', function(){
			var scene = _this.el.sceneEl.object3D
			var camera = _this.el.sceneEl.camera
			var renderer = _this.el.sceneEl.renderer

			//////////////////////////////////////////////////////////////////////////////
			//		build ARjs.Session
			//////////////////////////////////////////////////////////////////////////////
			var arSession = _this._arSession = new ARjs.Session({
				scene: scene,
				renderer: renderer,
				camera: camera,
				sourceParameters: arProfile.sourceParameters,
				contextParameters: arProfile.contextParameters		
			})

			//////////////////////////////////////////////////////////////////////////////
			//		tango specifics - _tangoPointCloud
			//////////////////////////////////////////////////////////////////////////////

			_this._tangoPointCloud = null
			if( arProfile.contextParameters.trackingBackend === 'tango' && _this.data.tangoPointCloudEnabled ){
				// init tangoPointCloud
				var tangoPointCloud = _this._tangoPointCloud = new ARjs.TangoPointCloud(arSession)
				scene.add(tangoPointCloud.object3d)
			}

			//////////////////////////////////////////////////////////////////////////////
			//		tango specifics - _tangoVideoMesh
			//////////////////////////////////////////////////////////////////////////////

			_this._tangoVideoMesh = null
			if( arProfile.contextParameters.trackingBackend === 'tango' ){
				// init tangoVideoMesh
				var tangoVideoMesh = _this._tangoVideoMesh = new ARjs.TangoVideoMesh(arSession)
				
				// override renderer.render to render tangoVideoMesh
				var rendererRenderFct = renderer.render;
				renderer.render = function customRender(scene, camera, renderTarget, forceClear) {
					renderer.autoClear = false;
					// clear it all
					renderer.clear()
					// render tangoVideoMesh
					if( arProfile.contextParameters.trackingBackend === 'tango' ){
						// FIXME fails on three.js r84
						// render sceneOrtho
						rendererRenderFct.call(renderer, tangoVideoMesh._sceneOrtho, tangoVideoMesh._cameraOrtho, renderTarget, forceClear)
						// Render the perspective scene
						renderer.clearDepth()		
					}
					// render 3d scene
					rendererRenderFct.call(renderer, scene, camera, renderTarget, forceClear);
				}
			}
			
			//////////////////////////////////////////////////////////////////////////////
			//		Code Separator
			//////////////////////////////////////////////////////////////////////////////

			_this.isReady = true

			//////////////////////////////////////////////////////////////////////////////
			//		awefull resize trick
			//////////////////////////////////////////////////////////////////////////////
			// KLUDGE
			window.addEventListener('resize', onResize)
			function onResize(){
				var arSource = _this._arSession.arSource

				// ugly kludge to get resize on aframe... not even sure it works				
				if( arProfile.contextParameters.trackingBackend !== 'tango' ){
					arSource.copyElementSizeTo(document.body)
				}
				
				// fixing a-frame css
				var buttonElement = document.querySelector('.a-enter-vr')
				if( buttonElement ){
					buttonElement.style.position = 'fixed'
				}
			}


			//////////////////////////////////////////////////////////////////////////////
			//		honor .debugUIEnabled
			//////////////////////////////////////////////////////////////////////////////
			if( _this.data.debugUIEnabled )	initDebugUI()
			function initDebugUI(){
				// get or create containerElement
				var containerElement = document.querySelector('#arjsDebugUIContainer')
				if( containerElement === null ){
					containerElement = document.createElement('div')
					containerElement.id = 'arjsDebugUIContainer'
					containerElement.setAttribute('style', 'position: fixed; bottom: 10px; width:100%; text-align: center; z-index: 1;color: grey;')
					document.body.appendChild(containerElement)
				}

				// create sessionDebugUI
				var sessionDebugUI = new ARjs.SessionDebugUI(arSession)
				containerElement.appendChild(sessionDebugUI.domElement)
			}
		})

		//////////////////////////////////////////////////////////////////////////////
		//		Code Separator
		//////////////////////////////////////////////////////////////////////////////
// TODO this is crappy - code an exponential backoff - max 1 seconds
		// KLUDGE: kludge to write a 'resize' event
		var startedAt = Date.now()
		var timerId = setInterval(function(){
			if( Date.now() - startedAt > 10000*1000 ){
				clearInterval(timerId)
				return 					
			}
			// onResize()
			window.dispatchEvent(new Event('resize'));
		}, 1000/30)
	},
	
	tick : function(now, delta){
		var _this = this

		// skip it if not yet isInitialised
		if( this.isReady === false )	return

		var arSession = this._arSession

		// update arSession
		this._arSession.update()
		
		if( _this._tangoVideoMesh !== null )	_this._tangoVideoMesh.update()

		// copy projection matrix to camera
		this._arSession.onResize()
	},
})
